/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit.testspecs;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author edegenetais
 */
public class AntWildCardSpecTest {

    @Test
    public void shouldHonorClassicWildCardAtLeaf(){
        final AntWildCardSpec testee = new AntWildCardSpec("first/second/*");
        Assert.assertTrue(testee.accept("first/second/first.ta"));
        Assert.assertTrue(testee.accept("first/second/other.ta"));
    }
    
    @Test
    public void shouldHonorClassicWildCardAtLeafWithTerm(){
        final AntWildCardSpec testee = new AntWildCardSpec("first/second/*.ta");
        Assert.assertTrue(testee.accept("first/second/first.ta"));
        Assert.assertTrue(testee.accept("first/second/other.ta"));
    }
    
    @Test
    public void shouldHonorTreeWildCard(){
        final AntWildCardSpec testee = new AntWildCardSpec("first/*/first.ta");
        Assert.assertTrue(testee.accept("first/second/first.ta"));
        Assert.assertFalse(testee.accept("first/second/other.ta"));
        Assert.assertFalse(testee.accept("first/one/andAnother/first.ta"));
        Assert.assertTrue(testee.accept("first/wtfThisIsDifferent/first.ta"));
    }
    
    @Test
    public void shouldHonorInnerAntWildCard(){
        final AntWildCardSpec testee = new AntWildCardSpec("first/**/first.ta");
        Assert.assertTrue(testee.accept("first/second/first.ta"));
        Assert.assertFalse(testee.accept("first/second/other.ta"));
        Assert.assertTrue(testee.accept("first/one/andAnother/first.ta"));
        Assert.assertTrue(testee.accept("first/wtfThisIsDifferent/first.ta"));
    }
    
    @Test
    public void shouldHonorInitialAntWildCard(){
        final AntWildCardSpec testee = new AntWildCardSpec("**/first.ta");
        Assert.assertTrue(testee.accept("first/second/first.ta"));
        Assert.assertFalse(testee.accept("first/second/other.ta"));
        Assert.assertTrue(testee.accept("first/one/andAnother/first.ta"));
        Assert.assertTrue(testee.accept("first/wtfThisIsDifferent/first.ta"));
    }
}
