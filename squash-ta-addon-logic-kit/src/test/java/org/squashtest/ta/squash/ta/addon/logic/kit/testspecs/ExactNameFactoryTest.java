/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit.testspecs;

import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.ExactNameFactory;
import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.SpecSyntaxException;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author edegenetais
 */
public class ExactNameFactoryTest {
    ExactNameFactory testee=new ExactNameFactory();
    
    @Test
    public void acceptSimpleName(){
        Assert.assertTrue("Failed to accept simple name", testee.accept("oneTest.ta"));
    }
    
    @Test
    public void acceptExactPath(){
        Assert.assertTrue("Failed to accept exact path", testee.accept("ecosystem/test.ta"));
    }
    
    @Test
    public void refuseWildcardTestSpec(){
        Assert.assertFalse("Wrongly accepted wildcard",testee.accept("*.ta"));
    }
    
    @Test
    public void refuseAntWildcardTestSpec(){
        Assert.assertFalse("Wrongly accepted wildcard",testee.accept("**/toto.ta"));
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void throwsIfUnmatchedSpecForConstruction() throws SpecSyntaxException{
        testee.getInstance("*.ta");
    }
    
    @Test
    public void createFromExactName() throws SpecSyntaxException{
        Assert.assertNotNull(testee.getInstance("group/individual.ta"));
    }
}
