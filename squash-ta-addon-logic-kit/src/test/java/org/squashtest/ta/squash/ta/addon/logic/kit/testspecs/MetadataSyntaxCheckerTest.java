/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit.testspecs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.factories.TestSyntaxException;
import org.squashtest.ta.commons.metadata.MetadataCheckingResult;
import org.squashtest.ta.squash.ta.addon.logic.kit.MetadataSyntaxChecker;

/**
 *
 * @author dclaerhout
 */
public class MetadataSyntaxCheckerTest {

    Map<String, List<Map<String, List<String>>>> ecosystemGroups;
    List<Map<String, List<String>>> ecosystem;
    List<Map<String, List<String>>> anotherEcosystem;
    Map<String, List<String>> metadataList;
    Map<String, List<String>> anotherMetadataList;
    List<String> metadataValues;
    List<String> anotherMetadataValues;
    Set<String> givenKeySet;
    
    
    @Before
    public void setup() {
        ecosystemGroups = new HashMap<>();
        ecosystem = new ArrayList<>();
        anotherEcosystem = new ArrayList<>();
        metadataList = new HashMap<>();
        anotherMetadataList = new HashMap<>();
        metadataValues = new ArrayList<>();
        anotherMetadataValues = new ArrayList<>();
    }

    @Test
    public void metadataHasGoodSyntaxAndHasUniqueValues() {

        metadataValues.add("123456");
        metadataList.put("metadataKey", metadataValues);
        ecosystem.add(metadataList);
        ecosystemGroups.put("ecosystem", ecosystem);

        MetadataSyntaxCheckerImpl testee = new MetadataSyntaxCheckerImpl(ecosystemGroups,true, true, true);

        testee.checkMetadataSyntaxByDefault();
    }

    @Test(expected = TestSyntaxException.class)
    public void metadataHasSpecialCharacterInKey() {

        metadataValues.add("123456");
        metadataList.put("met@d@t@&Key", metadataValues);
        ecosystem.add(metadataList);
        ecosystemGroups.put("ecosystem", ecosystem);

        MetadataSyntaxCheckerImpl testee = new MetadataSyntaxCheckerImpl(ecosystemGroups, true, true, false);

        testee.checkMetadataSyntaxByDefault();
    }

    @Test(expected = TestSyntaxException.class)
    public void metadataHasSpecialCharacterInValue() {

        metadataValues.add("&éjnn");
        metadataList.put("metadataKey", metadataValues);
        ecosystem.add(metadataList);
        ecosystemGroups.put("ecosystem", ecosystem);

        MetadataSyntaxCheckerImpl testee = new MetadataSyntaxCheckerImpl(ecosystemGroups, true, true, false);

        testee.checkMetadataSyntaxByDefault();
    }
    

    @Test(expected = TestSyntaxException.class)
    public void metadataValuesAreNotUniqueAcrossEcosystems() {

        metadataValues.add("123456");
        metadataList.put("metadataKey", metadataValues);
        ecosystem.add(metadataList);
        anotherEcosystem.add(metadataList);
        ecosystemGroups.put("ecosystem", ecosystem);
        ecosystemGroups.put("anotherEcosystem", anotherEcosystem);

        MetadataSyntaxCheckerImpl testee = new MetadataSyntaxCheckerImpl(ecosystemGroups, true, true, true);

        testee.checkMetadataSyntaxWithUnicityChecking(null);
    }

    @Test
    public void metadataValuesAreUniqueAcrossEcosystemsUsingUserInputKeyList() {

        metadataValues.add("123456");
        anotherMetadataValues.add("713705");
        metadataList.put("metadataKey", metadataValues);
        anotherMetadataList.put("metadataKey",anotherMetadataValues);
        ecosystem.add(metadataList);
        anotherEcosystem.add(anotherMetadataList);
        ecosystemGroups.put("ecosystem", ecosystem);
        ecosystemGroups.put("anotherEcosystem", anotherEcosystem);
 

        MetadataSyntaxCheckerImpl testee = new MetadataSyntaxCheckerImpl(ecosystemGroups, true, true, true);

        testee.checkMetadataSyntaxWithUnicityChecking("[metadataKey]");
    }
    
    @Test(expected = TestSyntaxException.class)
    public void metadataValuesAreNotUniqueAcrossEcosystemsUsingUserInputKeyList() {

        metadataValues.add("123456");
        metadataList.put("metadataKey", metadataValues);
        ecosystem.add(metadataList);
        anotherEcosystem.add(metadataList);
        ecosystemGroups.put("ecosystem", ecosystem);
        ecosystemGroups.put("anotherEcosystem", anotherEcosystem);
 

        MetadataSyntaxCheckerImpl testee = new MetadataSyntaxCheckerImpl(ecosystemGroups, true, true, true);

        testee.checkMetadataSyntaxWithUnicityChecking("[metadataKey]");
    }

    @Test(expected = IllegalArgumentException.class)
    public void userInputListForMetadataUnicityCheckingIsNotWrappedIntoBrackets() {

        metadataValues.add("123456");
        metadataList.put("metadataKey", metadataValues);
        ecosystem.add(metadataList);
        anotherEcosystem.add(metadataList);
        ecosystemGroups.put("ecosystem", ecosystem);
        ecosystemGroups.put("anotherEcosystem", anotherEcosystem);
 

        MetadataSyntaxCheckerImpl testee = new MetadataSyntaxCheckerImpl(ecosystemGroups, true, true, true);

        testee.checkMetadataSyntaxWithUnicityChecking("metadataKey");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void userInputListForMetadataUnicityCheckingIsEmpty() {

        metadataValues.add("123456");
        metadataList.put("metadataKey", metadataValues);
        ecosystem.add(metadataList);
        anotherEcosystem.add(metadataList);
        ecosystemGroups.put("ecosystem", ecosystem);
        ecosystemGroups.put("anotherEcosystem", anotherEcosystem);
 

        MetadataSyntaxCheckerImpl testee = new MetadataSyntaxCheckerImpl(ecosystemGroups, true, true, true);

        testee.checkMetadataSyntaxWithUnicityChecking("[]");
    }    
    

    // Basic implementation of MetadataSyntaxChecker for test purpose.  
    private class MetadataSyntaxCheckerImpl extends MetadataSyntaxChecker<Map<String, List<String>>> {

        private final Logger LOGGER = LoggerFactory.getLogger(MetadataSyntaxCheckerImpl.class);
        
        public MetadataSyntaxCheckerImpl(Map<String, List<Map<String, List<String>>>> ecosystemGroups, boolean invokeError, boolean checkValueUnicityInMultiValueMetadata, boolean withProjectValueUnicityChecking) {
            super(ecosystemGroups,invokeError,checkValueUnicityInMultiValueMetadata,withProjectValueUnicityChecking);
        }

        @Override
        public MetadataCheckingResult hasMetadataErrorInTestPointer(String groupName, Map<String, List<String>> metadataList) {

            boolean hasSyntaxError = false;
            boolean hasUnicityError = false;
            boolean hasMetadataInEcoFileError = false;

            String testCaseName = "test";

            for (Map.Entry<String, List<String>> metadata : metadataList.entrySet()) {

                MetadataCheckingResult metadataResult = hasMetadataErrorInEachMetadata(groupName, testCaseName, metadata);
                hasSyntaxError = metadataResult.hasSyntaxError() || hasSyntaxError;
                hasUnicityError = metadataResult.hasUnicityError() || hasUnicityError;
                hasMetadataInEcoFileError = metadataResult.hasMetadataInEcoFileError() || hasMetadataInEcoFileError;
            }
            return new MetadataCheckingResult(hasSyntaxError, hasUnicityError, hasMetadataInEcoFileError);
        }

        @Override
        protected void invokerMetadataError(String groupName, String testCaseName, String currentKey, String errorMsg) {
            if (isInvokeError()) {
                LOGGER.error("[Test suite: {} - Test case: {} {} {} \'.", groupName, testCaseName, errorMsg, currentKey);
            } else {
                LOGGER.warn("[Test suite: {} - Test case: {} {} {} \'.", groupName, testCaseName, errorMsg, currentKey);
            }
        }

        @Override
        protected void invokeMultiValueDuplicate(String key, String currentValue, String groupName, String testCaseName) {
            if (isCheckValueUnicityInMultiValueMetadata()) {
                LOGGER.warn("[{}{}] Warning while parsing Metadata VALUE: \'{}\' - a same VALUE is already assigned to the current Metadata KEY: \'{}\'.",groupName, testCaseName, currentValue, key); 
            }
        }
    }
}
