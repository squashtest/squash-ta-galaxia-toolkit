/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit.testspecs;

import java.util.ArrayList;
import java.util.List;

/**
 * This class translate string test specs into test filter specification factories.
 * @author edegenetais
 */
public class TestSpecFactory {
    private List<TestSpec.SpecFactory> factories=new ArrayList<>();

    public TestSpecFactory() {
        factories.add(new ExactNameFactory());
        factories.add(new AntWildcardFactory());
    }
    
    public TestSpec getSpec(String specString) throws SpecSyntaxException{
        for(TestSpec.SpecFactory factory:factories){
            if(factory.accept(specString)){
                return factory.getInstance(specString);
            }
        }
        throw new SpecSyntaxException(specString+" is not recognized as a valid test specification");
    }
}
