/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TestResult;

/**
 * Json Test-list ecosystem exporter used to export UFT test lists to the 
 * test-tree format for Squash TA execution server .
 * TODO : look for possible mutualization between this and the parallel code developed for the junit runner / mutualized for the robotframework runner.
 * 
 * @author edegenetais
 */
public class JsonTestTreeExporter implements ResultExporter{
    
    private static final String CONTENTS_FIELD = "contents";
    private static final String NAME_FIELD = "name";
    private static final String TESTS_FIELD = "tests";
    
    private String outputDirectoryName="test-tree";
    
    @Override
    public void write(File destDir, SuiteResult sr) throws IOException {
        final File treeFile = new File(destDir,"testTree.json");
        try (JsonGenerator generator = new JsonFactory().createGenerator(treeFile, JsonEncoding.UTF8)) {
            generator.writeStartObject();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            TimeZone tz= TimeZone.getTimeZone("UTC");
            dateFormat.setTimeZone(tz);
            generator.writeStringField("timestamp", dateFormat.format(new Date()));
            generator.writeStringField(NAME_FIELD, TESTS_FIELD);
            generator.writeArrayFieldStart(CONTENTS_FIELD);
            for(EcosystemResult eco:sr.getSubpartResults()){
                writeEcosystemContent(generator, eco);
            }
            generator.writeEndArray();
            generator.writeEndObject();
        }
    }

    private void writeEcosystemContent(JsonGenerator generator, EcosystemResult eco) throws IOException{
        generator.writeStartObject();
        generator.writeStringField(NAME_FIELD, eco.getName());
        generator.writeArrayFieldStart(CONTENTS_FIELD);
        for(TestResult test:eco.getSubpartResults()){
            generator.writeStartObject();
            generator.writeStringField(NAME_FIELD, test.getName());
            generator.writeNullField(CONTENTS_FIELD);
            generator.writeEndObject();
        }
        generator.writeEndArray();
        generator.writeEndObject();
    }
    
    @Override
    public String getOutputDirectoryName() {
        return outputDirectoryName;
    }

    @Override
    public void setOutputDirectoryName(String customOutputDirname) {
        this.outputDirectoryName=customOutputDirname;
    }

    @Override
    public void setReportType(ReportType rt) {
        if(rt.isExecution()){
            LoggerFactory.getLogger(JsonTestTreeExporter.class).warn("Json result export is NOT supported.");
        }
    }
    
}
