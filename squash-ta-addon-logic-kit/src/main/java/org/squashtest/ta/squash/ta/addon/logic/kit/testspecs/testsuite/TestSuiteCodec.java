/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.testsuite;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author edegenetais
 */
public class TestSuiteCodec {

    protected JsonFactory jsonFactory;

    public TestSuiteCodec() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        jsonFactory = new JsonFactory(objectMapper);
    }

    public TestSuite loadTestSuite(File testSuite) throws IOException {
        try(
            JsonParser parser = getJsonFactory().createParser(testSuite);
            ){
            return  parser.readValueAs(TestSuite.class);
        }
    }
 
    /**
     * @return the jsonFactory
     */
    protected JsonFactory getJsonFactory() {
        return jsonFactory;
    }

    public void writeTestSuite(final File filteredTestSuiteFile, TestSuite filteredSuite) throws IOException {
        try(
            final JsonGenerator generator = getJsonFactory().createGenerator(filteredTestSuiteFile, JsonEncoding.UTF8);
                ){
            generator.writeObject(filteredSuite);
        }
    }
}
