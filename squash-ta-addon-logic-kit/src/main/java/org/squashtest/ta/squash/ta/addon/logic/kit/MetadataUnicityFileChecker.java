/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.squashtest.ta.commons.metadata.MetadataUnicityFileTool;

/**
 *
 * @author qtran
 */
public class MetadataUnicityFileChecker {
    
    private String groupName;
    
    private String testCaseName;
    
    private String key;
    
    private List<String> values;
    
    private Map<String, Set<String>> unicityControlMap = new HashMap<>();
    
    public MetadataUnicityFileChecker(String groupName, String testCaseName, String key, List<String> values, Map<String, Set<String>> projectUnicityCheckingMap) {
        this.groupName =  groupName;
        this.testCaseName = testCaseName;
        this.key = key;
        this.values = values;
        this.unicityControlMap = projectUnicityCheckingMap;
    }

    /**
     * This method is to check the value unicity of all metadata keys in the working project.
     * 
     * @param givenKeySet the user given key list
     * @return an object that control if the file has metadata syntax error and at the same time
     * a map that stores all the metadata "key:value" with the the list of files where it exists in
     */
    public MetadataUnicityFileCheckingResult getValueUnicityCheckingResult(Set<String> givenKeySet) {
        MetadataUnicityFileCheckingResult result = new MetadataUnicityFileCheckingResult();       
        boolean needToCheck = needToCheckThisKeyOrNot(givenKeySet);
        result.setNeedToCheck(needToCheck);
        
        if (needToCheck){
            boolean hasError = false;
        
            Set<String> metadataValueSet = new HashSet<>(values);

            for (String value : metadataValueSet) {
                String resultKey = key+":"+value;
                hasError = insertMetadataMethodNamePairListIntoUnicityControlMap(resultKey,groupName, testCaseName);
            }
            result.setHasError(hasError);
            result.setUnicityControlMap(unicityControlMap);           
        }
        
        return result;
    }
    
    /**
     * In order for a key to be checked, either user doesnt use the "-Dtf.metadata.check.keys" param,
     * or his inserted key list contains the current examinating key
     * @param givenKeySet the user given key list
     * @return true if a condition is matched
     */
    private boolean needToCheckThisKeyOrNot(Set<String> givenKeySet) {
        if (!givenKeySet.isEmpty()){
            for (String givenKey : givenKeySet) {
                if (givenKey.equalsIgnoreCase(key)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    /**
     * This method is to store every "(Metadata KEY : Metadata VALUE), method name" pair into a Map
     * if a Metadata KEY is assigned to a same value in different methods --> error
     * 
     * @param resultKey the current "Metadata KEY:Metadata VALUE"
     * @param methodName the current method (display) name
     */
    private boolean insertMetadataMethodNamePairListIntoUnicityControlMap(String resultKey,String groupName, String testCaseName) {
        boolean hasDuplicate = false;
        Set<String> methodNameSet = new HashSet<>();
        
        String foundKey = new MetadataUnicityFileTool(resultKey, unicityControlMap).getDuplicateKeyValueInUnicityControlMap(resultKey);
        
        if (foundKey != null){
            hasDuplicate = true;
            methodNameSet = unicityControlMap.get(foundKey);            
            methodNameSet.add("[Test group: "+groupName+" - Case: "+testCaseName+"]");
            unicityControlMap.put(foundKey, methodNameSet);
        } else {
            methodNameSet.add("[Test group: "+groupName+" - Case: "+testCaseName+"]");
            unicityControlMap.put(resultKey, methodNameSet);
        }
        
        return hasDuplicate;
    }

}
