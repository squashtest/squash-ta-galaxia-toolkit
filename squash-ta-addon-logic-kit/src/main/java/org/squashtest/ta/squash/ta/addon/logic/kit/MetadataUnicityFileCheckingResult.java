/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author qtran
 */
public class MetadataUnicityFileCheckingResult {
    private boolean needToCheck;
    
    private boolean hasError;
    
    private Map<String, Set<String>> unicityControlMap = new HashMap<>();

    public MetadataUnicityFileCheckingResult() {
    }

    public MetadataUnicityFileCheckingResult(boolean hasError, Map<String, Set<String>> unicityControlMap) {
        this.hasError = hasError;
        this.unicityControlMap = unicityControlMap;    
    }

    public boolean neededToCheck() {
        return needToCheck;
    }

    public void setNeedToCheck(boolean needToCheck) {
        this.needToCheck = needToCheck;
    }

    public boolean hasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public Map<String, Set<String>> getUnicityControlMap() {
        return unicityControlMap;
    }

    public void setUnicityControlMap(Map<String, Set<String>> unicityControlMap) {
        this.unicityControlMap = unicityControlMap;
    }
    
}
