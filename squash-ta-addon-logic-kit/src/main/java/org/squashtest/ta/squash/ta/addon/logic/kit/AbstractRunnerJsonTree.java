/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.json.listing.JsonTestTreeException;

/**
 * This class defines the base logic to write test pointers, grouped by ecosystem, as a json test list.
 * Defining test references is a job for the each runner implementation.
 * TODO : this comes from the mutualization of a severe case of square wheel reinventing during metadata implementation. Please note that it should be mutualized with the core logic of the {@link JsonTestTreeExporter} class (only keeping the exporter wiring in it).
 * 
 * @param <TestRefType> the test reference type specific to the runner implementation.
 * @author edegenetais
 */
public abstract class AbstractRunnerJsonTree<TestRefType> {
    
    private static final Logger LOGGER=LoggerFactory.getLogger(AbstractRunnerJsonTree.class);
    
    private static final String CONTENTS = "contents";
    /** Sub directory of the default output directory wherein the json file should be created */
    private static final String LISTING_OUTPUT_DIRECTORY = "test-tree";
    /** The name of the json file to create */
    private static final String FILE_NAME = "testTree.json";
    /** The json file to write */
    private final File jsonFile;

    protected static interface DateFactory{
        Date getCurrentDate();
    }
    
    protected DateFactory dateFactory=new DateFactory() {
        @Override
        public Date getCurrentDate() {
            return new Date();
        }
    };
    
    public AbstractRunnerJsonTree(File destinationDir) {
        File outputDirectory = new File(destinationDir, LISTING_OUTPUT_DIRECTORY);
        outputDirectory.mkdirs();
        jsonFile = new File(outputDirectory, FILE_NAME);
    }

    public void write(Map<String, List<TestRefType>> testGroupMap, boolean withMetadata) {
        try (JsonGenerator generator = new JsonFactory().createGenerator(jsonFile, JsonEncoding.UTF8)) {
            generator.setCodec(new ObjectMapper());
            
            generator.writeStartObject();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            TimeZone tz= TimeZone.getTimeZone("UTC");
            dateFormat.setTimeZone(tz);
            generator.writeStringField("timestamp", dateFormat.format(dateFactory.getCurrentDate()));
            generator.writeStringField("name", "tests");
            generator.writeArrayFieldStart(CONTENTS);
            
            for(Map.Entry<String, List<TestRefType>> testGroup : testGroupMap.entrySet()){                
                writeTestPointerGroup(generator, testGroup, withMetadata);
            }
            
            generator.writeEndArray();
            generator.writeEndObject();
        } catch (IOException e) {
            String message = "An error occurs while creating the test tree at json format.";
            LOGGER.error(message,e);
            throw new JsonTestTreeException(message, e);
        }
    }
    
    private void writeTestPointerGroup(JsonGenerator generator, Map.Entry<String, List<TestRefType>> testGroup, boolean withMetadata) throws IOException {
        String currentClassName = testGroup.getKey();
        generator.writeStartObject();
        generator.writeStringField("name", currentClassName);
        List<TestRefType> testPointers = testGroup.getValue();
        
        generator.writeArrayFieldStart(CONTENTS);
        for (TestRefType testPointer: testPointers){
            writeTestPointer(testPointer, generator, withMetadata);
        }
        generator.writeEndArray();
        
        generator.writeEndObject();
    }
    
    protected void writeMethodMetadataAnnotationValues(Map<String, List<String>> metadataMap, JsonGenerator generator) throws IOException {
         
        if (metadataMap.isEmpty()) {
            generator.writeObjectField("metadata", new LinkedHashMap<>());
        } else {
            chargeMetadataList(metadataMap, generator);
        }
    }
    
    private void chargeMetadataList(Map<String, List<String>> metadataList, JsonGenerator generator) throws IOException {
        generator.writeFieldName("metadata");
        generator.writeStartObject();
        
        for (Map.Entry<String, List<String>> entry : metadataList.entrySet()){
            writeMetadata(entry, generator);
        }
        
        generator.writeEndObject();
    }
    
    private void writeMetadata(Map.Entry<String, List<String>> entry, JsonGenerator generator) throws IOException {
        String key = entry.getKey().trim();
        List<String> values = entry.getValue();
        if(values.isEmpty()){
            generator.writeNullField(key);
        }else{
            generator.writeArrayFieldStart(key);
            //This should fulfill the specificatrion and be tesatable owing to reproducible element orders.
            Set<String> valueSet = new TreeSet<>(values);
            for (String value : valueSet) {
                generator.writeString(value.trim());
            }
            generator.writeEndArray();
        }        
    }
    
    protected void writeTestPointer(TestRefType testPointer, JsonGenerator generator, boolean withMetadata) throws IOException {
        
        String testDisplayName = getTestPointerTestName(testPointer);
        
        generator.writeStartObject();
        generator.writeStringField("name", testDisplayName);
        
        if (withMetadata) {
            Map<String,List<String>> metadataMap=getMetadataForTestPointer(testPointer);
            writeMethodMetadataAnnotationValues(metadataMap, generator);            
        }
        
        generator.writeNullField(CONTENTS);
        generator.writeEndObject();
    }
    
    protected abstract String getTestPointerTestName(TestRefType testPointer);

    /**
     * This method uses the test coordinates described by the test pointer to query the
     * project model and retrieve the metadata list for the matching test.
     * @param testPointer the test pointer to retrieve for.
     * @return The metadata list.
     */
    protected abstract Map<String,List<String>> getMetadataForTestPointer(TestRefType testPointer);
}
