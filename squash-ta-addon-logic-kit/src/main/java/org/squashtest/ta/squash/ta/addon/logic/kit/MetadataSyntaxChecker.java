/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.factories.TestSyntaxException;
import org.squashtest.ta.commons.metadata.MetadataCheckingResult;

/**
 *
 * @author qtran
 */
public abstract class MetadataSyntaxChecker<TestPointerType> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(MetadataSyntaxChecker.class);
    
    private static final String DOCUMENTATION_REFERENCE = "\nPlease refer to Squash TF documentation for further details.";
    
    private static final String UNICITY_ERROR_MSG = "Squash Test Metadata unicity checking failed." + DOCUMENTATION_REFERENCE;
    
    private static final String SYNTAX_AND_UNICITY_ERROR_MSG = "Squash Test Metadata syntax and unicity checking failed." + DOCUMENTATION_REFERENCE;
   
    private static final String INVALID_METADATA_CHECK_KEY_PARAM = "Invalid value for \'tf.metadata.check.keys\' parameter";
    
    private static final String SYNTAX_ERROR_MSG = "Squash Test Metadata checking failed." + DOCUMENTATION_REFERENCE;

    private static final String METADATA_KEY = "[\\w-.]+";
    private static final String METADATA_VALUE = "[\\w-./]+";     

    
    private Map<String, Set<String>> projectUnicityCheckingMap = new HashMap<>();
    
    private Set<String> projectFoundKeySet = new HashSet<>();
    
    private  Map<String, List<TestPointerType>> ecosystemGroups;
    
    private boolean invokeError;
    
    private boolean checkValueUnicityInMultiValueMetadata;
    
    private boolean withProjectValueUnicityChecking;
    
    private Set<String> givenKeySet = new HashSet<>();
    
    public MetadataSyntaxChecker(Map<String, List<TestPointerType>> ecosystemGroups,
                                 boolean invokeError, 
                                 boolean checkValueUnicityInMultiValueMetadata,
                                 boolean withProjectValueUnicityChecking) {
        
        this.ecosystemGroups = ecosystemGroups;
        this.invokeError = invokeError;
        this.checkValueUnicityInMultiValueMetadata = checkValueUnicityInMultiValueMetadata;
        this.withProjectValueUnicityChecking = withProjectValueUnicityChecking;
    }
    
    /**
     * This method is to check the metadata syntax.
     * It will invoke a test syntax exception if a/some Squash Metadata syntax rules is/are violated,
     * or try to catch a runtime exception during folder reading process.
     */
    public void checkMetadataSyntaxByDefault() {
        MetadataCheckingResult result = hasMetadataErrorInTestProject();
        if (result.hasError() && invokeError){
                throw new TestSyntaxException(SYNTAX_ERROR_MSG);
        }
    }
    
     /**
     * This method is to parse all the metadata in the test project and return "true" if syntax error(s) is found.
     * 
     * @return true if at least an error is obtained
     */
    private MetadataCheckingResult hasMetadataErrorInTestProject() {
        boolean hasSyntaxError = false;
        boolean hasUnicityError = false;
        boolean hasMetadataInEcoFileError = false;
        
        for (Map.Entry<String, List<TestPointerType>> testGroup : ecosystemGroups.entrySet()) {
            MetadataCheckingResult testGroupResult = hasMetadataErrorInTestPointerList(testGroup.getKey(), testGroup.getValue());
            hasSyntaxError = testGroupResult.hasSyntaxError() || hasSyntaxError;
            hasUnicityError = testGroupResult.hasUnicityError() || hasUnicityError;
            hasMetadataInEcoFileError = testGroupResult.hasMetadataInEcoFileError() || hasMetadataInEcoFileError;
        }
        
        return new MetadataCheckingResult(hasSyntaxError, hasUnicityError, hasMetadataInEcoFileError);
    }
    
    /**
     * This method is to parse all the metadata in each test pointer group and return "true" if syntax error(s) is found.
     * 
     * @param groupName the current test pointer group name
     * @param testPointerGroup the current test pointer group
     * 
     * @return true if at least an error is obtained
     */
    private MetadataCheckingResult hasMetadataErrorInTestPointerList(String groupName, List<TestPointerType> testPointerGroup) {
        boolean hasSyntaxError = false;
        boolean hasUnicityError = false;
        boolean hasMetadataInEcoFileError = false;
        
        for (TestPointerType testPointer : testPointerGroup) {
            MetadataCheckingResult testPointerResult = hasMetadataErrorInTestPointer(groupName, testPointer);
            hasSyntaxError = testPointerResult.hasSyntaxError() || hasSyntaxError;
            hasUnicityError = testPointerResult.hasUnicityError() || hasUnicityError;
            hasMetadataInEcoFileError = testPointerResult.hasMetadataInEcoFileError() || hasMetadataInEcoFileError;
        }
        return new MetadataCheckingResult(hasSyntaxError, hasUnicityError, hasMetadataInEcoFileError);
    }
    
    protected abstract MetadataCheckingResult hasMetadataErrorInTestPointer(String groupName, TestPointerType testPointer);
  
    protected MetadataCheckingResult hasMetadataErrorInEachMetadata(String groupName, String testCase, Map.Entry<String, List<String>> metadata){
        String key = metadata.getKey().trim();
        List<String> values = metadata.getValue();
        boolean hasMetadataSyntaxErrorInKey = getMetadataSyntaxErrorInKey(groupName, testCase, key);
        boolean hasMetadataSyntaxErrorInValues = getMetadataSyntaxErrorInValues(key, groupName, testCase, values);
        boolean hasSyntaxError = hasMetadataSyntaxErrorInKey || hasMetadataSyntaxErrorInValues;
        
        boolean hasUnicityError = false;        
        if (withProjectValueUnicityChecking) {
            //check if each of the given metadata keys is value-unique in all tests
            MetadataUnicityFileCheckingResult unicityCheckingResult = new MetadataUnicityFileChecker(groupName, testCase, key, values, projectUnicityCheckingMap).getValueUnicityCheckingResult(givenKeySet);
            
            projectFoundKeySet.add(key);
            if (unicityCheckingResult.neededToCheck()) {
                projectUnicityCheckingMap = unicityCheckingResult.getUnicityControlMap();
                hasUnicityError = unicityCheckingResult.hasError();
            }            
        }
        return new MetadataCheckingResult(hasSyntaxError, hasUnicityError, false);
    }

    
    private boolean getMetadataSyntaxErrorInKey(String groupName, String testCase, String key) {
        if (key.matches(METADATA_KEY)){
            return false;
        } else {
            invokerMetadataError(groupName, testCase, key, "] Metadata KEY syntax error: \'");
            return true;
        }
    }
    
    private boolean getMetadataSyntaxErrorInValues(String key, String groupName, String testCase, List<String> values) {
        boolean hasError = false;
        List<String> valueList = new ArrayList<>();
        for (String value : values){
            String newValue = value.trim();
            if (!valueList.contains(newValue)){
                valueList.add(newValue);
                if (hasMetadataSyntaxErrorInValue(groupName, testCase, newValue)){
                    hasError = true;
                }
            } else {
                invokeMultiValueDuplicate(key, newValue,groupName, testCase);
            }            
        }
        return hasError;
    }
    
    private boolean hasMetadataSyntaxErrorInValue(String groupName, String testCase, String value) {
        if (value.matches(METADATA_VALUE)){
            return false;
        } else {
            invokerMetadataError(groupName, testCase, value, "] Metadata VALUE syntax error: \'");
            return true;
        }
    }
   

    protected abstract void invokerMetadataError(String groupName, String testCase, String currentKey, String errorMsg);

    protected abstract void invokeMultiValueDuplicate(String key, String currentValue,String groupName, String testCase);
    
    //******************************************************
    /**
     * This method is to check the TF metadata syntax of all JUnit methods in all test pointer groups.
     * It will invoke a test syntax exception if a/some Squash Metadata syntax rules is/are violated,
     * or catch a runtime exception (if any) during the browsing process.
     * In addtion, the project scale value unicity checking is now activated.
     * 
     * @param keyList the user given key list
     */
    public void checkMetadataSyntaxWithUnicityChecking(String keyList) {        
        //no specific value for checking, that means a throughout checking...
        if (keyList == null){                
            checkMetadataValueUnicityByDefault();
        }        
        //when user inserts some values to check
        else {
            LOGGER.debug("User input for tf.metadata.check.keys: \'"+keyList+"\'.");
            String userInputForMetadataCheckKeyParam = keyList.trim();

            if (isWrappedByBrackets(userInputForMetadataCheckKeyParam)){
                treatUserInputListForMetadataCheckParam(userInputForMetadataCheckKeyParam);
            } else {
                invokeNonCoveredByBracketsArgExcptForMetadataCheckParam(userInputForMetadataCheckKeyParam);
            }
        }
    }
    
     private void checkMetadataValueUnicityByDefault() {
        MetadataCheckingResult result = hasMetadataErrorInTestProject();
        if (result.hasError()){
            if (result.hasUnicityError()){
                displayUnicityErrorInCheckingMap();
                if (result.hasSyntaxError()){
                    throw new TestSyntaxException(SYNTAX_AND_UNICITY_ERROR_MSG);
                } else {
                    throw new TestSyntaxException(UNICITY_ERROR_MSG);
                }
            } else {
                throw new TestSyntaxException(SYNTAX_ERROR_MSG);
            }
        }
    }

    private void displayUnicityErrorInCheckingMap() {
        //sort unicity checking map in A-Z order
        Map<String, Set<String>> treeMap = new TreeMap<>(projectUnicityCheckingMap);
        for (Map.Entry<String, Set<String>> entry: treeMap.entrySet()){
            String keyValue = entry.getKey();
            int separateurIndex = keyValue.indexOf(":");
            String key = keyValue.substring(0,separateurIndex).trim();
            String value = keyValue.substring(separateurIndex+1).trim();
            String startingMsg = "For Metadata KEY: {"+key+"}, VALUE: {"+value+"} has been found in :";
            
            StringBuilder builder = new StringBuilder(startingMsg);
            Set<String> values = entry.getValue();
            if (!values.isEmpty() && values.size()>1){
                for (String fileName : entry.getValue()){
                    builder.append("\n").append("\t").append(fileName);
                }
                LOGGER.error(builder.toString());
            }            
        }        
    }

    private boolean isWrappedByBrackets(String userInputForMetadataCheckKeyParam) {
        String regex = "\\[.*\\]";
        Pattern pat = Pattern.compile(regex);
        return pat.matcher(userInputForMetadataCheckKeyParam).matches();
    }   
    
    private void treatUserInputListForMetadataCheckParam(String userInputForMetadataCheckKeyParam) {
        String removedBracketsInput = removeBracketsOfInputString(userInputForMetadataCheckKeyParam);
        String trimmedRemovedBracketsInput = removedBracketsInput.trim();
        if ("".equals(trimmedRemovedBracketsInput)){
            invokeEmptyUserInputListForMetadataCheckParamException(userInputForMetadataCheckKeyParam);
        } else {
            String[] inputList = removedBracketsInput.split(",");
            if (inputList.length == 0) {
                invokeEmptyUserInputListForMetadataCheckParamException(userInputForMetadataCheckKeyParam);
            } else {
                treatNotEmptyUserInputListForMetadataCheckParam(inputList);
            }
        }
    }
    
    private String removeBracketsOfInputString(String userInputForMetadataCheckParam) {
        return userInputForMetadataCheckParam.substring(1, userInputForMetadataCheckParam.length()-1);
    }
    
    private void invokeEmptyUserInputListForMetadataCheckParamException(String userInputForMetadataCheckKeyParam) {
        LOGGER.error("\'"+userInputForMetadataCheckKeyParam+"\': "+INVALID_METADATA_CHECK_KEY_PARAM
                +". Parameter input list MUST NOT be empty.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_KEY_PARAM);
    }
    
    private void treatNotEmptyUserInputListForMetadataCheckParam(String[] inputList) {
        for (String input :  inputList){
            String trimmedInput =  input.trim();
            if ("".equals(trimmedInput)){
                invokeEmptyUserInputForMetadataCheckParamException(trimmedInput);
            } else {
                givenKeySet.add(trimmedInput);                
            }
        }
        checkMetadataValueUnicityWithGivenKeyList(givenKeySet);
    }
    
    private void invokeEmptyUserInputForMetadataCheckParamException(String trimmedInput) {
        LOGGER.error("\'"+trimmedInput+"\': "+INVALID_METADATA_CHECK_KEY_PARAM
                +". Parameter input MUST NOT be empty.");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_KEY_PARAM);
    }
    
    private void checkMetadataValueUnicityWithGivenKeyList(Set<String> userSet) {        
        String errorMessage = hasMetadataSyntaxOrUnicityError(userSet);
        if (errorMessage != null) {
            displayUnfoundKeyError(userSet);
            throw new TestSyntaxException(errorMessage);
        } else {
            displayUnfoundKeyError(userSet);
        }
    }

    private void displayValueUnicityCheckingFailed(Set<String> userSet) {
        StringBuilder strBuilder = createStringBulderFromStringSet(userSet);
        LOGGER.error("Metadata value unicity analysis for keys: {"+strBuilder.delete(0, 2).toString()+"} in test project failed.");
    }

    private void displayValueUnicityCheckingSuccess(Set<String> userSet) {
        StringBuilder strBuilder = createStringBulderFromStringSet(userSet);
        LOGGER.info("Metadata value unicity analysis for keys: {"+strBuilder.delete(0, 2).toString()+"} in test project has been successfully completed.");
    }
    
    private String hasMetadataSyntaxOrUnicityError(Set<String> userSet) {
        MetadataCheckingResult result = hasMetadataErrorInTestProject();
        if (result.hasError()){
            if (result.hasUnicityError()){
                displayUnicityErrorInCheckingMap();
                displayValueUnicityCheckingFailed(userSet);
                if (result.hasSyntaxError()){
                    return SYNTAX_AND_UNICITY_ERROR_MSG;
                } else {
                    return UNICITY_ERROR_MSG;
                }
            } else {
                displayValueUnicityCheckingSuccess(userSet);
                return SYNTAX_ERROR_MSG;
            }
        } else {
            displayValueUnicityCheckingSuccess(userSet);
            return null;
        }
    }
    
    private void displayUnfoundKeyError(Set<String> userSet) {
        //warning unfound KEYS
        Set<String> missingKeySet = new HashSet<>();
        
        if (userSet != null){
            addGivenKeyIntoMissingKeylist(userSet, missingKeySet);
        }
        
        if (!missingKeySet.isEmpty()){
            StringBuilder strBuilder = createStringBulderFromStringSet(missingKeySet);
            LOGGER.warn("Some metadata keys not found in any test: {"+strBuilder.delete(0, 2).toString()+"}.");
        }
    }

    private void addGivenKeyIntoMissingKeylist(Set<String> userSet, Set<String> missingKeySet) {
        for (String key : userSet){
            if(isMissingKey(key)){
                missingKeySet.add(key);
            }
        }
    }
    
    private boolean isMissingKey(String key) {
        for (String givenKey : projectFoundKeySet) {
            if (givenKey.equalsIgnoreCase(key)) {
                return false;
            }
        }
        return true;
    }
    
    private StringBuilder createStringBulderFromStringSet(Set<String> stringSet) {
        StringBuilder strBuilder = new StringBuilder();
        TreeSet<String> strTreeSet = new TreeSet<>(stringSet);
        
        for (String key : strTreeSet) {
            strBuilder.append(", ").append(key);
        }
        return strBuilder;
    }

    private void invokeNonCoveredByBracketsArgExcptForMetadataCheckParam(String userInputForMetadataCheckKeyParam) {
        LOGGER.error("\'"+userInputForMetadataCheckKeyParam+"\': "+INVALID_METADATA_CHECK_KEY_PARAM
                +". Parameter input(s) must be wrapped in brackets, ex: \"[key]\" or \"[key1, KEY2]\".");
        throw new IllegalArgumentException(INVALID_METADATA_CHECK_KEY_PARAM);
    }

    public boolean isInvokeError() {
        return invokeError;
    }

    public boolean isCheckValueUnicityInMultiValueMetadata() {
        return checkValueUnicityInMultiValueMetadata;
    }

}
