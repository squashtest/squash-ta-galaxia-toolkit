/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.testsuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Model of a test suite execution order.
 * @author edegenetais
 */
public class TestSuite {
    private List<Test> test;
    
    /**
     * This public default constructor is used by jackson for deserialization.
     * @deprecated for use by frameworks only. Please note that for Spring
     * it is best to use the full initialization constructor as a factory method.
     */
    @Deprecated
    public TestSuite(){
        test=new ArrayList<Test>();
    }

    /**
     * This is the preferred constructor in client code as it directly creates a usable model.
     * @param tests the tests to be executed.
     */
    public TestSuite(List<Test> tests) {
        this.test=tests;
    }

    public List<Test> getTest() {
        return test;
    }
}
