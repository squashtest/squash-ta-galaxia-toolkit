/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit.testspecs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class creates a specification that recognizes test names ant-style,
 * with the * AND ** wildcards.
 * @author edegenetais
 */
class AntWildCardSpec implements TestSpec{
    
    private static final Pattern WILD_CARD_PATTERN = Pattern.compile("(^\\*\\*/|/\\*\\*/|[^*]\\*[^*]|[^*]\\*$)");
    
    private final Pattern targetPattern;
    
    public AntWildCardSpec(String spec){
        StringBuilder patternBuilder=new StringBuilder();
        Pattern wildCardFinder=WILD_CARD_PATTERN;
        Matcher wildCardMatcher=wildCardFinder.matcher(spec);
        int previousBegin=0;
        while(wildCardMatcher.find()){
            final String wildCardOccurence = wildCardMatcher.group();
            
            patternBuilder.append(Pattern.quote(spec.substring(previousBegin, wildCardMatcher.start())));
            previousBegin=wildCardMatcher.end();
            
            if("**/".equals(wildCardOccurence)){
                patternBuilder.append(".*/");
            }else if("/**/".equals(wildCardOccurence)){
                patternBuilder.append("/.*/");
            }else{
                patternBuilder.append(Pattern.quote(String.valueOf(wildCardOccurence.charAt(0))));
                patternBuilder.append("[^/]*");
                if(wildCardOccurence.length()>2){
                    patternBuilder.append(Pattern.quote(String.valueOf(wildCardOccurence.charAt(2))));
                }
            }
        }
        
        patternBuilder.append(Pattern.quote(spec.substring(previousBegin)));
        
        targetPattern=Pattern.compile(patternBuilder.toString());
    }
    
    @Override
    public boolean accept(String name) {
        return targetPattern.matcher(name).matches();
    }
}
