/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.commons.factories.specification.TestSpecification;
import org.squashtest.ta.maven.param.json.JsonTestSuite;

/**
 * Root logic of test list spec parsing.
 *
 * @author edegenetais
 * @param <TestRefType> type de référence de test utilisée.
 */
public abstract class TestSpecParserBase<TestRefType> {

    private static Logger LOGGER = LoggerFactory.getLogger(TestSpecParserBase.class);
    private static final Pattern SUITE_AS_FILE_PATTERN = Pattern.compile("^\\{file:(.*)\\}$");
    private static final String ALL_TEST_WILDCARD = "**/*";

    private String testList;

    public TestSpecParserBase(String testList) {
        this.testList = testList;
    }

    public List<TestRefType> parseTestList() throws MojoFailureException {
        List<TestRefType> testSpecs = new ArrayList<>();
        final Matcher fileSpecMatcher = SUITE_AS_FILE_PATTERN.matcher(getTestList().trim());
        if (fileSpecMatcher.matches()) {
            LOGGER.debug("Test suite passed as Json");
            File jsonFile = new File(fileSpecMatcher.group(1));
            JsonTestSuite tree = createJsonTestSuite();
            SuiteSpecification suiteSpec = tree.parse(jsonFile);
            for (TestSpecification testSpec : suiteSpec.getTests()) {
                addTestPointerSpec(testSpec.getScript() + "{" + testSpec.getId() + "}", testSpecs);
            }
        } else {
            LOGGER.debug("Test suite passed as SCSV string");
            String[] testSpecTable = getTestList().split(";");
            for (String testSpec : testSpecTable) {
                addTestPointerSpec(testSpec, testSpecs);
            }
        }
        return testSpecs;
    }

   //This method is very close to the previous one but we needed to return a SuiteSpecification for TFParamService in order to get all the test parameters and global parameters
   public SuiteSpecification parseTestSuite() {
        SuiteSpecification suiteSpec = new SuiteSpecification();
        final Matcher fileSpecMatcher = SUITE_AS_FILE_PATTERN.matcher(getTestList().trim());
        
        if (fileSpecMatcher.matches()) {
            LOGGER.debug("Test suite passed as Json");
            File jsonFile = new File(fileSpecMatcher.group(1));
            JsonTestSuite tree = createJsonTestSuite();
            suiteSpec = tree.parse(jsonFile);
            
            return suiteSpec;
            
        } else {

            LOGGER.debug("Test suite wasn't passed as a Json File");
            return null;        
        }       
    }

    protected void addTestPointerSpec(String testSpec, List<TestRefType> testSpecs) throws MojoFailureException {
        String newTestSpec = testSpec.trim();
        switch (newTestSpec) {
            case "":
                LOGGER.warn("Ignoring empty test specification in list " + getTestList() + " please check your test list syntax.");
                break;
            case ALL_TEST_WILDCARD:
                addAllTestsWildCard(testSpecs);
                break;
            default:
                addIndividualTest(testSpec, testSpecs);
        }        
    }

    /**
     * This factory method tunes the JsonTestSuite instance as needed for the runner.
     *
     * @return
     */
    protected abstract JsonTestSuite createJsonTestSuite();

    /**
     * This method implements support for the all test wildcard specification support.
     *
     * @param testSpecs the method must compute test references for all tests
     * defined by this project and add them all to this collection.
     */
    protected abstract void addAllTestsWildCard(List<TestRefType> testSpecs);

    /**
     * This method must parse the test reference string and create a
     * {@link TestRefType} instance for it, then add this reference to the test list collection.
     *
     * @param testSpec the test specification string to parse.
     * @param testSpecs the test list to add the created reference to.
     * @throws MojoFailureException if anything fails (bad reference, failure
     * during the project analysis operations necessary to create the test
     * reference, if any)
     */
    protected abstract void addIndividualTest(String testSpec, List<TestRefType> testSpecs) throws MojoFailureException;

    /**
     * @return the testList
     */
    protected String getTestList() {
        return testList;
    }

}
