/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.test.event.EcosystemStatusUpdate;
import org.squashtest.ta.framework.test.event.StatusUpdateListener;
import org.squashtest.ta.framework.test.event.TestStatusUpdate;
import org.squashtest.ta.framework.test.event.TestSuiteStatusUpdate;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.ta.galaxia.tf.param.service.TFParamService;


/**
 * StatusEventListener implementation to let the java code know about
 * each test launch so that it serves the relevant test parameter set.
 * 
 * @author dclaerhout
 */
public class TestInfoListener implements StatusUpdateListener{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(TestInfoListener.class);
    

    @Override
    public void handle(TestSuiteStatusUpdate event) {/* This listener only manages test level information. */}

    @Override
    public void handle(EcosystemStatusUpdate event) {/* This listener only manages test level information. */}

    @Override
    public void handle(TestStatusUpdate event) {
        String eventName = event.getClass().getSimpleName();
        
        if ("TestInitSynchronousEvent".equals(eventName)) {
            TestResult testResult = event.getPayload();

            LOGGER.debug("TestInfoListener :Retrieving test info and sending them to TFParamService. Current test: {} ,with id: {}, in ecosytem: {}", testResult.getName(), testResult.getTestId(), event.getEcosystemName());
            TFParamService.getInstance().updateTestInfo(
                    event.getEcosystemName(),
                    testResult.getName(),
                    testResult.getTestId());
        }
    }  
}
