/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.squash.ta.addon.logic.kit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is a base algorithm to create ecosystems
 * from test pointers.
 * @param <TestRefType> type of the test pointers used by the runner / addon.
 * 
 * @author edegenetais
 */
public abstract class TestEcosystemModel<TestRefType> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestEcosystemModel.class);
    
    private List<TestRefType> testReferences;
    
    public TestEcosystemModel(List<TestRefType> testRefs){
        testReferences=new ArrayList<>(testRefs);
    }
    
    public Map<String,List<TestRefType>> groupPointersByEcosystemDefinition(){
        Map<String,List<TestRefType>> result=new HashMap<>();
        for(TestRefType testRef:testReferences){
            String ecosystemDiscriminant=buildEcosystemDiscriminant(testRef);
            if(!result.containsKey(ecosystemDiscriminant)){
                LOGGER.debug("Creating ecosystem group {} for test {}",ecosystemDiscriminant,testRef);
                result.put(ecosystemDiscriminant, new ArrayList<TestRefType>());
            }else{
                LOGGER.debug("Adding test {} to ecosystem group {}",testRef,ecosystemDiscriminant);
            }
            result.get(ecosystemDiscriminant).add(testRef);
        }
        return result;
    }
    
    /**
     * Builds the ecosystem discriminant used to group tests into ecosystems from 
     * {@link TestRefType} data.
     * @param testRef the test to sort into an ecosystem group.
     * @return the discriminant String.
     */
    protected abstract String buildEcosystemDiscriminant(TestRefType testRef);
}
