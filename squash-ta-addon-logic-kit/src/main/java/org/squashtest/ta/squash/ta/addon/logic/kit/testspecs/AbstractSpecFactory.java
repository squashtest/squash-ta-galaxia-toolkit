/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.squashtest.ta.squash.ta.addon.logic.kit.testspecs;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;
import org.slf4j.LoggerFactory;

/**
 *
 * @author edegenetais
 */
public abstract class AbstractSpecFactory<S extends TestSpec> implements TestSpec.SpecFactory {
    private static final Class<?>[] CONSTRUCTOR_PARMTYPES={String.class};
    private Constructor<S> constructor;

    protected AbstractSpecFactory(Class<S> specClass) {
        try {
            constructor=specClass.getConstructor(CONSTRUCTOR_PARMTYPES);
        } catch (NoSuchMethodException ex) {
            throw new LinkageError("Class "+specClass.getName()+" misses <init>(String spec).", ex);
        } catch (SecurityException ex) {
            throw new LinkageError("Cannot access <init>(String spec) in class "+specClass.getName(),ex);
        }
    }
    
    
    
 
    @Override
    public boolean accept(String spec) {
        return getSpecValidator().matcher(spec).matches();
    }
    
    protected abstract Pattern getSpecValidator();

    @Override
    public S getInstance(String spec) throws SpecSyntaxException{
        if(accept(spec)){
            return preformInstanciation(spec);
        }else{
            throw new IllegalArgumentException(spec+" is not a valid "+constructor.getDeclaringClass().getName()+" test specification.");
        }
    }

    private S preformInstanciation(String spec) throws SpecSyntaxException {
        try {
            return constructor.newInstance(spec);
        } catch (InstantiationException   |
                IllegalAccessException   |
                IllegalArgumentException ex) {
            throw new LinkageError("Failed to instanciate class "+constructor.getDeclaringClass().getName()+" : unexpected error.",ex);
        } catch(InvocationTargetException ite) {
            LoggerFactory.getLogger(AbstractSpecFactory.class).debug("InvocationTargetException thrown, unwrapping",ite);
            Throwable t=ite.getTargetException();
            if(t instanceof SpecSyntaxException){
                throw (SpecSyntaxException)t;
            }else{
                throw new RuntimeException("Unexpected runtime exception.",t);
            }
        }
    }

}
