<?xml version="1.0" encoding="UTF-8"?>
<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2011 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<xsl:stylesheet
    xpath-default-namespace="http://maven.apache.org/POM/4.0.0"
    targetNamespace="http://maven.apache.org/POM/4.0.0"
    xmlns:pom="http://maven.apache.org/POM/4.0.0"
    xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    exclude-result-prefixes="pom"
    version="2.0"
>
    <xsl:output method="xml" />
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="pom:build/pom:plugins/pom:plugin[./pom:groupId='org.squashtest.ta' and ./pom:artifactId='squash-ta-maven-plugin']/pom:dependencies">
        <xsl:copy>
            <xsl:apply-templates />
            <xsl:call-template name="galaxia-tools" />
        </xsl:copy>
    </xsl:template>
    <xsl:template name="galaxia-tools">
            <xsl:comment> Injected Galaxia tools </xsl:comment>
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>${project.artifactId}</artifactId>
                <version>${project.version}</version>
                <!-- 
                    Under here, we exclude Squash TA framework
                    dependencies to avoid linkage errors.
                    These will be provided at runtime by the Squash TA framework
                    in the version of the test project.
                -->
                <exclusions>
                    <xsl:comment> Excluding all dependencies provided by the Squash TA framework </xsl:comment>
                    <exclusion>
                        <groupId>org.squashtest.ta</groupId>
                        <artifactId>squash-ta-commons-exploitation</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.squashtest.ta</groupId>
                        <artifactId>squash-ta-core-utils</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.squashtest.ta.plugin</groupId>
                        <artifactId>squash-ta-plugin-db</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
    </xsl:template>
    <xsl:template match="pom:build/pom:plugins/pom:plugin[./pom:groupId='org.squashtest.ta' and ./pom:artifactId='squash-ta-maven-plugin']">
        <xsl:choose>
            <xsl:when test="count(./*[name()='dependencies'])=0">
                <xsl:copy>
                    <groupId><xsl:value-of select="pom:groupId"/></groupId>
                    <artifactId><xsl:value-of select="pom:artifactId"/></artifactId>
                    <xsl:for-each select="./pom:version">
                        <version><xsl:value-of select="."/></version>
                    </xsl:for-each>
                    <dependencies>
                        <xsl:call-template name="galaxia-tools"/>
                    </dependencies>
                    <xsl:apply-templates select="./*[name()!='groupId' and name()!='artifactId' and name()!='version'] | comment()"/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates />
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="pom:build/pom:plugins/pom:plugin[./pom:groupId='org.squashtest.ta' and ./pom:artifactId='squash-ta-maven-plugin']/*[name()='configuration']/*[name()='exporters']">
        <xsl:copy>
            <aggregatedReport>
                <resultStorage>../squashTA.galaxia/previous</resultStorage>
                <exporters>
                    <xsl:apply-templates />
                </exporters>
            </aggregatedReport>
            <xsl:comment> Injected Galaxia exporters </xsl:comment>
            <engineClasspath>
                <outputDirectoryName>../squashTA.galaxia</outputDirectoryName>
            </engineClasspath>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
