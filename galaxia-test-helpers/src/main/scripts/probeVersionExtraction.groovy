/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
// Find the probe dependency and inject its version in the build properties to make sure the right version is selected.
boolean probeDependencyFound=false;
String probeGroupId='org.squashtest.ta.galaxia'
String probeArtifactId='galaxia-component-probe'
String probeVersion=null

for(def dependency:project.dependencies){
    if(dependency.groupId==probeGroupId && dependency.artifactId==probeArtifactId){
        probeVersion=dependency.version
        project.properties.setProperty("injected.probe.version",probeVersion)
        probeDependencyFound=true;
    }
}
if(probeDependencyFound){
    println("Found probe dependency injected version will be "+probeVersion)
}else{
    String message="Failed to find the probe dependency ("+probeGroupId+":"+probeArtifactId+") Please check project dependencies."
    println(message)
    throw new org.apache.maven.plugin.MojoFailureException(message);
}