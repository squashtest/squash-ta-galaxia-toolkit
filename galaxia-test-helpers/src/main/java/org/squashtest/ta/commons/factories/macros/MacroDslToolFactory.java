/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.commons.factories.macros;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.squashtest.ta.galaxia.dsltools.MacroDslToolsFactory;
import org.squashtest.ta.galaxia.dsltools.MacroValidator;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacro;

/**
 * This class is the base for factories to produce {@link MacroValidator} instances from
 * {@link SquashDSLMacro} instances.
 * This class is not part of the official API, which is why it is abstract.
 * 
 * @author edegenetais
 */
public abstract class MacroDslToolFactory implements MacroDslToolsFactory,Comparator<MacroValidator>{
    
    public MacroValidator getValidator(SquashDSLMacro macro){
        return new MacroMatcherSource(macro);
    }
    
    @Override
    public List<MacroValidator> getValidators(Collection<SquashDSLMacro> macros){
        final ArrayList<MacroValidator> validatorList = new ArrayList<>(macros.size());
        for(SquashDSLMacro m:macros){
            validatorList.add(getValidator(m));
        }
        Collections.sort(validatorList, this);
        return validatorList;
    }

    @Override
    public int compare(MacroValidator t, MacroValidator t1) {
        return -Float.compare(t.getMatchSpecificity(), t1.getMatchSpecificity());
    }
    
}
