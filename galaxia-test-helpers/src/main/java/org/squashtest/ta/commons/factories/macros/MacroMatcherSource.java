/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.commons.factories.macros;

import org.squashtest.ta.galaxia.dsltools.MacroValidator;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacro;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignature;

/**
 * A macro-like object that does nothing but try to match candidate macro
 * invocations.
 *
 * @author edegenetais
 */
class MacroMatcherSource extends Matcher implements MacroValidator {

    private SquashDSLMacro initial;
    
    public MacroMatcherSource(SquashDSLMacro macroDefinition) {
        super(apiMacroToSignature(macroDefinition));
        this.initial = macroDefinition;
    }

    private static String apiMacroToSignature(SquashDSLMacro definition) {
        StringBuilder signature = new StringBuilder();
        for (SquashDSLMacroSignature s : definition.getSignatures()) {
            signature.append(s.definitionPart());
        }
        return signature.toString();
    }

    @Override
    public SquashDSLMacro originalMacroDefinition() {
        return initial;
    }

    @Override
    public float getMatchSpecificity() {
        return this.getSpecificityFactor();
    }

    @Override
    public boolean isValidInstanceOfMe(String invocationCode) {
        return matches(invocationCode);
    }
}
