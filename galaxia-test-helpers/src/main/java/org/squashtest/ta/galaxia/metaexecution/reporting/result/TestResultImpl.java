/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.PhaseResult;
import org.squashtest.ta.framework.test.result.TestResult;

/**
 * Implementation of the test result object for json.
 * @author edegenetais
 */
@JsonTypeName("default")
public class TestResultImpl extends AbstractInstructionSequence implements TestResult{
    private String testId;
    
    private PhaseResult teardownPhaseResult;
    private PhaseResult testPhaseResult;
    private PhaseResult setupPhaseResult;
    protected ExecutionDetails firstFailure;
    
    @Override
    public String getTestId() {
        return testId;
    }

    @Override
    public ExecutionDetails getFirstFailure() {
        return firstFailure;
    }

    @JsonGetter("setupPhaseResult")
    @Override
    public PhaseResult getSetupPhaseResult() {
        return setupPhaseResult;
    }

    @JsonGetter("testPhaseResult")
    @Override
    public PhaseResult getTestPhaseResult() {
        return testPhaseResult;
    }
    
    @JsonGetter("teardownPhaseResult")
    @Override
    public PhaseResult getTeardownPhaseResult() {
        return teardownPhaseResult;
    }

    @Override
    public boolean hasOnlyFailOrErrorWithContinue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
