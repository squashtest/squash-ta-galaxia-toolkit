/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

import org.squashtest.ta.galaxia.utils.MeFirstIslandClassloader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.SimpleLinesData;
import org.squashtest.ta.galaxia.utils.FileCanonicalPath;

/**
 * Honoring the test project's Squash TA plugin configuration: 
 * we create an isolated classpath to apply the test project's classpath.
 */
public class MeFirstIslandClassLoaderFactory implements ClassLoaderFactory {

    private static final Logger LOGGER=LoggerFactory.getLogger(MeFirstIslandClassLoaderFactory.class);
    
    public ClassLoader buildDbClassloader(Collection<URL> cpURLs, ClassLoader defaultCl, String bridgePackageName) {
        return new MeFirstIslandClassloader(cpURLs, defaultCl, bridgePackageName);
    }

    @Override
    public ClassLoader getTestProjectClassLoader(File projectRootDir) throws EngineLinkException {
        try {
            File classPathFile = new File(projectRootDir, "target/squashTA.galaxia/.classpath");
            if (classPathFile.exists()) {
                LOGGER.debug("Classpath data found at {}", new FileCanonicalPath(classPathFile));
                SimpleLinesData cpData = new SimpleLinesData(classPathFile.getCanonicalPath());
                ArrayList<URL> cpURLs = new ArrayList<>();
                for (String line : cpData.getLines()) {
                    cpURLs.add(new URL("file:" + line));
                    LOGGER.trace("Adding classpath URL file:{}", line);
                }
                final ClassLoader defaultCl = getClass().getClassLoader();
                return buildDbClassloader(cpURLs, defaultCl, "org.squashtest.ta.framework");
            } else {
                throw new EngineLinkException("Classpath file not found at expected " + new FileCanonicalPath(classPathFile) + ", please check for generation failure!");
            }
        } catch (IOException ex) {
            throw new EngineLinkException("Error while reading and parsing test project classpath.", ex);
        }
    }

}
