/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result.codec;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.Date;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.PhaseResult;

/**
 *
 * @author edegenetais
 */
@JsonTypeName("default")
@JsonIgnoreProperties(ignoreUnknown = true)
final class DefaultTestResultMixin {

    @JsonProperty("teardownPhaseResult")
    private PhaseResult teardownPhase;
    @JsonProperty("testPhaseResult")
    private PhaseResult testPhase;
    @JsonProperty("setupPhaseResult")
    private PhaseResult setupPhase;
    @JsonProperty("status")
    private GeneralStatus testStatus;

    @JsonGetter("endTime")
    public Date endTime() {
        return new Date();
    }

    @JsonGetter("startTime")
    public Date startTime() {
        return new Date();
    }
}
