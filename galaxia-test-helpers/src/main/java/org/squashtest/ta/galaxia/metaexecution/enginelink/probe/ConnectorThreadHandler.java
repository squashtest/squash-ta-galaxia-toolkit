/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink.probe;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.galaxia.metaexecution.enginelink.FrameworkConnector;

/**
 *
 * @author lpoma
 */
public class ConnectorThreadHandler implements Runnable{
    
    private ConnectorScheduling scheduler;
    private FrameworkConnector connector;
    private BackgroundFrameworkConnector.ErrorReporter errorSink;
    private BackgroundFrameworkConnector.SuccessListener listener;
    
    private long timeout=100;
    private TimeUnit timeoutUnit=TimeUnit.MILLISECONDS;
    private Future<SquashDSLComponentRegistry> lastFuture;
    private Future<SquashDSLComponentRegistry> registryFuture;
    
    
    /**
     * Handler for the secondary thread running in the background and trying to load the DSL component registry
     * Notify errors, exceptions and success of the process
     * 
     * @param listener 
     * @param errorSink
     * @param connector
     * @param scheduler
     */
    
    
    
    /**
     * Standard constructor
     * 
     * @param listener
     * @param errorSink
     * @param connector
     * @param scheduler 
     */
    public ConnectorThreadHandler(BackgroundFrameworkConnector.SuccessListener listener, BackgroundFrameworkConnector.ErrorReporter errorSink, FrameworkConnector connector, ConnectorScheduling scheduler) {
        this.listener = listener;
        this.errorSink = errorSink;
        this.connector = connector;
        this.scheduler = scheduler;
    }
    
    /**
     * Use this constructor if the delay before Timeout needs to be changed
     * 
     * @param listener
     * @param errorSink
     * @param connector
     * @param scheduler
     * @param timeout
     * @param timeoutUnit 
     */
    public ConnectorThreadHandler(BackgroundFrameworkConnector.SuccessListener listener, BackgroundFrameworkConnector.ErrorReporter errorSink, FrameworkConnector connector, ConnectorScheduling scheduler, long timeout, TimeUnit timeoutUnit) {
        this.listener = listener;
        this.errorSink = errorSink;
        this.connector = connector;
        this.scheduler = scheduler;
        this.timeout = timeout;
        this.timeoutUnit = timeoutUnit;
    }
    
    @Override
    public void run(){
        try {
            SquashDSLComponentRegistry registry = registryFuture.get();
            this.listener.notifySuccess(registry);
        } catch (InterruptedException | ExecutionException ex) {
            Logger.getLogger(ConnectorThreadHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    public SquashDSLComponentRegistry getSquashDSLComponentRegistry() throws TimeoutException, EngineLinkException {
        if(this.lastFuture == null || this.lastFuture.isDone()){
            Callable<SquashDSLComponentRegistry> registryLoadCallable = new DSLComponentRegistryBuildTask(errorSink,connector);
            registryFuture = scheduler.getExecutor().submit(registryLoadCallable);        
            this.lastFuture = registryFuture;
        } else {
            registryFuture = this.lastFuture;
        }
        try {
            return registryFuture.get(timeout, timeoutUnit);
        
        } catch (TimeoutException ex){
            Thread thread = new Thread(this);
            thread.setDaemon(true);
            thread.start();
            throw ex;            
        } catch (InterruptedException ex) {            
            final TimeoutException timeoutException = new TimeoutException("Failed to retrieve component registry because the task was interrupted");
            timeoutException.addSuppressed(ex);
            throw timeoutException;
        } catch (ExecutionException ex) {
                LoggerFactory.getLogger(ConnectorThreadHandler.class).debug("Registry extraction failed while in background", ex);
            try{
                throw ex.getCause();
            }catch(TimeoutException | EngineLinkException | Error e){
                throw e;
            }catch(Throwable e){
                /* 
                * Whatever tools say, the fact that we extract cause makes us catch Throwable. 
                * This little stunt allows the extraction code - which make seamless proxying possible - to compile.
                * As implied by the message, this code should only be triggered in off cases. Everything expected is already caught.
                */
                throw new RuntimeException("Got an unexpected throwable. Something's rotten in the realm of Denmark.", e);
            }
        }
    }
}
