/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.Date;

/**
 * Base class for named result objects.
 * @author edegenetais
 */
public class AbstractNamedResultObject extends AbstractResult {

    protected String name;
    protected Date endTime;
    protected Date startTime;

    public String getName() {
        return name;
    }

    @JsonGetter("startTime")
    public Date startTime() {
        return startTime;
    }

    @JsonGetter("endTime")
    public Date endTime() {
        return endTime;
    }


    public void cleanUp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonSetter("endTime")
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @JsonSetter("startTime")
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

}
