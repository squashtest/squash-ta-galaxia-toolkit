/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink.probe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.galaxia.metaexecution.enginelink.TempStorageMalfunction;

/**
 *
 * @author edegenetais
 */
public class ProbeSetup implements Runnable {

    public static final String PROBE_TEST_NAME = "probetest.ta";
    private File base;

    public ProbeSetup() {
        LoggerFactory.getLogger(ProbeSetup.class).info("Deploying probe pseudo-ecosystem.");
        try {
            base = createTempDirectoryTree();
            final Thread shutdownHook = new Thread(this);
            shutdownHook.setName("ProbeEcosystem shutdown hook");
            Runtime.getRuntime().addShutdownHook(shutdownHook);
            File testDirectory = getTestDirectory();
            testDirectory.mkdirs();
            InputStream is = getClass().getResourceAsStream(PROBE_TEST_NAME);
            FileOutputStream os = new FileOutputStream(new File(testDirectory, PROBE_TEST_NAME));
            byte[] buffer = new byte[4096];
            int read = is.read(buffer);
            while (read >= 0) {
                os.write(buffer, 0, read);
                read = is.read(buffer);
            }
            os.close();
            is.close();
        } catch (IOException ex) {
            throw new TempStorageMalfunction("Failed to deploy probe's virtual test directory. No project analysis will be available.", ex);
        }
    }

    /**
     * Atomically creates a temporary directory which will be guaranteed not to already exist.
     * @return
     * @throws IOException
     */
    public File createTempDirectoryTree() throws IOException {
        File f = new File(System.getProperty("java.io.tmpdir"));
        Path tmpBase = Paths.get(f.toURI());
        return Files.createTempDirectory(tmpBase, null).toFile();
    }

    public final File getTestDirectory() {
        return new File(base, "tests");
    }

    @Override
    public void run() {
        final org.slf4j.Logger logger = LoggerFactory.getLogger(ProbeSetup.class);
        logger.info("Cleaning probe pseudo-ecosystem.");
        if (base != null && base.exists()) {
            try {
                FileTree.FILE_TREE.clean(base);
            } catch (IOException ex) {
                logger.warn("Failed to clean up galaxia probe's pseudo-ecosystem at {}.", base.getAbsolutePath(), ex);
            }
        }
    }

}
