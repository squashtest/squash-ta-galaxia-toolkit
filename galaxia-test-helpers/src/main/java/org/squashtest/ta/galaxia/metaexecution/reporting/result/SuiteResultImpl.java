/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TargetInitialisationResult;

/**
 *
 * @author edegenetais
 */
public class SuiteResultImpl extends AbstractTestGroup implements SuiteResult{
    private String projectGroupId;
    private String projectArtifactId;
    private String projectVersion;
    
    protected List<? extends EcosystemResult> subpartResults=new ArrayList<>();
    private List<TargetInitialisationResult> targetInitialisationResults=new ArrayList<>();

    @Override
    public String getProjectGroupId() {
        return projectGroupId;
    }

    @Override
    public String getProjectArtifactId() {
        return projectArtifactId;
    }

    @Override
    public String getProjectVersion() {
        return projectVersion;
    }

    @Override
    public List<TargetInitialisationResult> getTargetInitialisationResults() {
        return targetInitialisationResults;
    }

    @Override
    public List<? extends EcosystemResult> getSubpartResults() {
        return subpartResults;
    }

    public void setProjectGroupId(String projectGroupId) {
        this.projectGroupId = projectGroupId;
    }

    public void setProjectArtifactId(String projectArtifactId) {
        this.projectArtifactId = projectArtifactId;
    }

    public void setProjectVersion(String projectVersion) {
        this.projectVersion = projectVersion;
    }

}
