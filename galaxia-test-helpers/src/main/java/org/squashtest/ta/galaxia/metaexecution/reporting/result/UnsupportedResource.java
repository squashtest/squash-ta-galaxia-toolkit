/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.Resource;

/**
 * Placeholder for all unsupported resources in json data.
 * @author edegenetais
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("unsupported")
public class UnsupportedResource implements Resource<UnsupportedResource>{
    @Override
    public UnsupportedResource copy() {
        return this;
    }

    @Override
    public void cleanUp() {
        //noop : this is a mere placeholder.
    }

    @JsonAnySetter
    public void logIgnoredProperty(String name, Object value) {
        LoggerFactory.getLogger(getClass()).debug("Ignoring value {} for property {}", value, name);
    }
}
