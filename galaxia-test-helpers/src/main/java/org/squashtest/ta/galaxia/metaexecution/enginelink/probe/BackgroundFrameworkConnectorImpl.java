/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink.probe;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.galaxia.metaexecution.enginelink.FrameworkConnector;

/**
 *
 * @author edegenetais
 */
class BackgroundFrameworkConnectorImpl implements BackgroundFrameworkConnector{
    
    private ConnectorScheduling scheduler;
    private FrameworkConnector connector;
    private ErrorReporter errorSink;
    private SuccessListener listener;
    private long timeout=100;
    private TimeUnit timeoutUnit=TimeUnit.MILLISECONDS;
    

    public BackgroundFrameworkConnectorImpl(ErrorReporter errorSink, File pomPath) throws EngineLinkException {
        this(errorSink,pomPath,new String[]{});
    }
    
    public BackgroundFrameworkConnectorImpl(ErrorReporter errorSink, File pomPath, String... additionalToolPath) throws EngineLinkException {
        this(errorSink, pomPath, getConnectorScheduling(pomPath), additionalToolPath);
    }
    
    public BackgroundFrameworkConnectorImpl(ErrorReporter errorSink, SuccessListener listener, File pomPath, String... additionalToolPath) throws EngineLinkException {
        this(errorSink, pomPath, getConnectorScheduling(pomPath), additionalToolPath);
        this.listener = listener;
    }
    
    /**
     * Use this constructor in tests and if you want to tweak the scheduling strategy.
     * If you don't get what this is about, use the default scheduling constructor .
     * @param scheduler the scheduler implementation to use.
     */
    public BackgroundFrameworkConnectorImpl(ErrorReporter errorSink,File pomPath,ConnectorScheduling scheduler, String... additionalToolPath) {
        this.errorSink=errorSink;
        this.connector=new FrameworkConnector(pomPath,additionalToolPath);
        this.scheduler = scheduler;
    }
    
    /**
     * Full initialization constructor with no default values.
     * @param pomPath the path to the target project pom.
     * @param scheduler the scheduler implementation to use.
     * @param timeout the timeout (unit is given by the timeoutUnit parameter)
     * @param timeoutUnit 
     */
    public BackgroundFrameworkConnectorImpl(ErrorReporter errorSink, File pomPath, ConnectorScheduling scheduler, long timeout, TimeUnit timeoutUnit) {
        this(errorSink,pomPath,scheduler, new String[]{});
        this.timeout = timeout;
        this.timeoutUnit = timeoutUnit;
    }
    
    /**
     * Full initialization constructor with no default values.
     * @param pomPath the path to the target project pom.
     * @param scheduler the scheduler implementation to use.
     * @param timeout the timeout (unit is given by the timeoutUnit parameter)
     * @param timeoutUnit 
     */
    public BackgroundFrameworkConnectorImpl(ErrorReporter errorSink, File pomPath, ConnectorScheduling scheduler, long timeout, TimeUnit timeoutUnit,String... additionalToolPath) {
        this(errorSink, pomPath,scheduler, additionalToolPath);
        this.timeout = timeout;
        this.timeoutUnit = timeoutUnit;
    }
    
    private static BasicConnectorScheduling getConnectorScheduling(File pomPath) throws EngineLinkException {
        try{
            return new BasicConnectorScheduling(pomPath);
        }catch(IOException e){
            throw new EngineLinkException("Failed to create framework connector", e);
        }
    }

    @Override
    public synchronized SquashDSLComponentRegistry getSquashDSLComponentRegistry() throws EngineLinkException,TimeoutException {       
        this.listener = (SuccessListener) errorSink;    
        ConnectorThreadHandler threadHandler = new ConnectorThreadHandler(listener,errorSink,connector,scheduler);

        try {
            return threadHandler.getSquashDSLComponentRegistry();            
        } catch (EngineLinkException | TimeoutException ex){
            throw ex;
        }
    }
}
