/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.conditions;

import java.util.Set;

/**
 * Common interface of all conditions.
 * @author edegenetais
 */
public interface Condition {
    /**
     * Evaluates the condition.
     * @return <code>true</code> if the condition is fulfilled, <code>false</code> otherwise.
     * @throws ConditionException if the condition is cannot be evaluated.
     * @see ConditionException for subclasses to distinguish misconfiguration from runtime failure.
     */
    boolean check() throws ConditionException;
    /**
     * To allow various meta-treatment on conditions, the {@link Condition} interfaces
     * offers access to the initial condition parameters.
     * These should allow, among other things, recreating the same condition.
     * @param name name of the parameter.
     * @return value of the parameter.
     * @see #getInitialParameterNames() 
     */
    Object getInitialParm(String name);
    
    /**
     * Method to find out the set of supported parameters of a condition.
     * @return the set of supported parameters.
     * @see #getInitialParm(java.lang.String) 
     */
    Set<String> getInitialParameterNames();
}
