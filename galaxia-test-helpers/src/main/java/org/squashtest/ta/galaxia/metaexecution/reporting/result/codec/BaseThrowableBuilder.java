/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result.codec;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.slf4j.LoggerFactory;

/**
 * Base class to manage throwable properties such as cause.
 * @author edegenetais
 * @param <EX> the builder's product.
 */
@JsonPropertyOrder(alphabetic = true)
@JsonIgnoreProperties(
        "localizedMessage"
)
public abstract class BaseThrowableBuilder<EX extends Throwable> {

    @JsonProperty("cause")
    private Throwable cause;
    @JsonProperty("stackTrace")
    private StackTraceElement[] stackTrace;
    @JsonProperty("suppressed")
    private Throwable[] suppressed;
    
    protected abstract EX createProduct();
    
    public EX build(){
        EX exception=createProduct();
        exception.setStackTrace(stackTrace);
        if(cause!=null){
            try{
                exception.initCause(cause);
            }catch(IllegalStateException ise){
                LoggerFactory.getLogger(getClass()).trace("Cause {} was already set", cause,ise);
            }
        }
        if(suppressed==null){
            suppressed=new Throwable[0];
        }
        for(Throwable oneSuppressed:suppressed){
            exception.addSuppressed(oneSuppressed);
        }
        return exception;
    }
}
