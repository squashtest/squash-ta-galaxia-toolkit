/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.galaxia.utils.FileCanonicalPath;
import org.squashtest.ta.galaxia.utils.MeFirstIslandClassloader;

/**
 * Exporter to write the test project classpath down to disk.
 *
 * @author edegenetais
 */
public class ClassPathExporter implements ResultExporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClassPathExporter.class);

    private static final String SLF4J_PROBE_CLASS_PER_MNG5787 = "Logger.class";
    
    private static final String JAR_PROTOCOL = "jar";
    private static final String FILE_PROTOCOL = "file";
    private static final String CP_PROBE_RESSOURCE_NAME = "META-INF/MANIFEST.MF";
    /* Why compute this over and over again ? */
    private static final int PROBE_RESSOURCE_NAME_LENGTH = CP_PROBE_RESSOURCE_NAME.length();
    
    private String outputDirectoryName;
    private String cpFileName = ".classpath";
    /** As a function of report type, we'll wirte classpath or go no-op */
    private ReportType mode;

    @Override
    public void write(File targetDirectory, SuiteResult result) throws IOException {
        if(mode.isList()){
            File destinationDir = new File(targetDirectory, outputDirectoryName);
            destinationDir.mkdirs();
            File cpFile = new File(destinationDir, cpFileName);
            Enumeration<URL> manifestEnumeration = getClass().getClassLoader().getResources(CP_PROBE_RESSOURCE_NAME);

            try (
                    FileWriter sink = new FileWriter(cpFile);) {
                Set<URL> urlSet=new HashSet<>();
                while (manifestEnumeration.hasMoreElements()) {
                    URL url = manifestEnumeration.nextElement();
                    addClasspathEntryFromResourceURL(url, urlSet,sink);
                }

                lookoutForForMaven3ClasspathBugMNG5787(urlSet, sink);

            }catch(Exception e){
                cpFile.delete();
                throw new InstructionRuntimeException("Failed to write classpath file "+cpFile.getAbsolutePath(),e);
            }catch(Error e){//NOSONAR : we attempt to clean our mess, then rethrow the error, not trying to recover from the unrecoverable.
                cpFile.delete();
                LOGGER.error("An error caused the classpath file write to fail",e);
                throw e;
            }
            LOGGER.info("Exported test project classpath to {}", new FileCanonicalPath(cpFile));
        }else{
            LOGGER.debug("Do nothing for mode {}, as classpath is a List mode exporter.",mode);
        }    
    }

    private void lookoutForForMaven3ClasspathBugMNG5787(Set<URL> urlSet, final FileWriter sink) throws URISyntaxException, IOException {
        try{
            MeFirstIslandClassloader checkCl=new MeFirstIslandClassloader(urlSet, new URLClassLoader(new URL[]{}), "org.squashtest.ta.galaxia");
            checkCl.loadClass(Logger.class.getName());
        }catch(ClassNotFoundException cnfe){
            LOGGER.trace("Logger interface definition {} not found, trying to FIX.",Logger.class.getName(),cnfe);
            URL interfaceJarManifestUrl=Logger.class.getResource(SLF4J_PROBE_CLASS_PER_MNG5787);
            addClasspathEntryFromResourceURL(interfaceJarManifestUrl, urlSet, sink,SLF4J_PROBE_CLASS_PER_MNG5787.length());
            LOGGER.info("Applying classpath FIX for Maven 3.x as per bug #MNG5787, and adding classpath entry for manifest {} to the classpath.",interfaceJarManifestUrl);
        }
    }
    
    private void addClasspathEntryFromResourceURL(URL url, Set<URL> cpSet, final FileWriter sink) throws IOException, URISyntaxException {
        addClasspathEntryFromResourceURL(url, cpSet, sink, PROBE_RESSOURCE_NAME_LENGTH);
    }

    private void addClasspathEntryFromResourceURL(URL url, Set<URL> cpSet, final FileWriter sink, int probeRessourceName) throws URISyntaxException, IOException {
        final String urlExternalForm = url.toExternalForm();
        final String urlProtocol = url.getProtocol();
        switch (urlProtocol) {
            case JAR_PROTOCOL:
                addJarClasspathEntry(urlExternalForm, cpSet, sink);
                break;
            case FILE_PROTOCOL:
                addFileClasspathEntry(url, probeRessourceName, sink, cpSet);
                break;
            default:
                LOGGER.warn("Ignoring classpath entry {} because protocol {} is unsupported.", urlExternalForm, urlProtocol);
        }
    }

    private void addFileClasspathEntry(URL url, int probeRessourceName, final FileWriter sink, Set<URL> cpSet) throws IOException {
        final String cpEntryDir = url.getFile().substring(0, url.getFile().length()-probeRessourceName-1);
        LOGGER.debug("Including directory {} in exported classpath.", cpEntryDir);
        sink.append(cpEntryDir).append("\n");
        cpSet.add(new URL("file:"+cpEntryDir));
    }

    private void addJarClasspathEntry(final String urlExternalForm, Set<URL> cpSet, final FileWriter sink) throws URISyntaxException, IOException {
        String[] parts = urlExternalForm.split("!");
        final String jarURLString = parts[0].substring(JAR_PROTOCOL.length()+1);
        LOGGER.debug("Including jar file {} in exported classpath.", jarURLString);
        URL jarURL=new URL(jarURLString);
        cpSet.add(jarURL);
        sink.append(new File(jarURL.toURI()).getCanonicalPath()).append("\n");
    }
    
    /**
     * @return the name of the classpath file. This defaults to
     * <code>.classpath</code> if not set.
     */
    public String getCpFileName() {
        return cpFileName;
    }

    /**
     * @param cpFileName value to override the default ".classpath" value.
     */
    public void setCpFileName(String cpFileName) {
        this.cpFileName = cpFileName;
    }

    @Override
    public String getOutputDirectoryName() {
        return outputDirectoryName;
    }

    @Override
    public void setOutputDirectoryName(String directoryName) {
        this.outputDirectoryName = directoryName;
    }

    @Override
    public void setReportType(ReportType type) {
        this.mode=type;
    }

}
