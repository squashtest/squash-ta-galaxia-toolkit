/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

import org.squashtest.ta.galaxia.metaexecution.enginelink.probe.ProbeSetup;
import org.squashtest.ta.galaxia.metaexecution.enginelink.probe.ProbeInjector;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.json.codecs.component.registry.JsonCodec;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.concurrent.TimeoutException;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.local.process.library.process.LocalProcessClient;
import org.squashtest.ta.local.process.library.shell.Platform;
import org.squashtest.ta.local.process.library.shell.ShellResult;
import org.squashtest.ta.local.process.library.shell.WindowsPlatform;

/**
 * This connector allows clients of the galaxia environement to use KeywordFramework functionalities.
 * @author edegenetais
 */
public class FrameworkConnector{
    /*
    * If you add public methods here, please extends the BackgroundFrameworkConnector interface 
    * org.squashtest.ta.galaxia.metaexecution.enginelink.probe package in the so that the BackgroundFrameworkConnector will offer these methods too.
    * If you remove public methods here, please remove the corresponding method from the BackgroundFrameworkConnector interface.
    * If you don't, you will break the BackgroundFrameworkConnectorTest unit test.
    */
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(FrameworkConnector.class);
    
    private  static final String FAILED_TO_EXTRACT_COMPONENT_REGISTRY_DATA = "Failed to extract component registry data from the Squash TA engine.";

    private String[] additionalToolPath=new String[]{};
    public static final ProbeSetup PROBE_PSEUDO_ECOSYSTEM=new ProbeSetup();
    private File projectPom;
    
     /**
      * Default configuration constructor.
      * @param pomFile path to the pom file of the target project.
      */
    public FrameworkConnector(File pomFile){
        this.projectPom=pomFile;
    }
    
    /**
     * Constructor with additioonal tool path configuration.
     * @param pomFile path to the pom file of the target project.
     * @param additionalToolPath one or more additional path elements for maven instance lookup.
     */
    public FrameworkConnector(File pomFile, String... additionalToolPath){
        this(pomFile);
        if(additionalToolPath!=null && additionalToolPath.length>0){
            this.additionalToolPath=additionalToolPath;
        }
    }
    
    /**
     * This method is to charge the JSON file that describes all built_in_framework macros
     * @return a TA Macro object list
     * @throws EngineLinkException if the engine component registry build process gets error(s)
     */
    public SquashDSLComponentRegistry getSquashDSLComponentRegistry() throws EngineLinkException {
        File injectedPom = null;
        try{
            final File registryDataFaile = new File(projectPom.getParentFile(),"target/squashTA.galaxia/componentRegistry.json");
            if(registryDataFaile.exists() && registryDataFaile.lastModified()>projectPom.lastModified()){
                LOGGER.debug("Registry file is newer than pom, reusing it.");
            }else{
                injectedPom=new ProbeInjector().inject(projectPom);
                injectedPom.deleteOnExit();
                performEngineProbeRun(injectedPom);
            }
            return new JsonCodec().unmarshall(registryDataFaile);
        }catch(IOException e){
            throw new EngineLinkException("Failed to build component registry.",e);
        }finally{
            if(injectedPom!=null){
                //boolean for C-like brain-dead error management...
                boolean deleted=injectedPom.delete();
                
                if(deleted){
                    LOGGER.debug("Successfully deleted injected pom file {}",injectedPom.getAbsoluteFile());
                }else if(injectedPom.exists()){
                    LOGGER.warn("Failed to clean up file {}. Hopefully deleteOnExit will succeede.",injectedPom);
                }
            }
        }
    }

    private void performEngineProbeRun(File injectedPom) throws EngineLinkException, IOException {
        LocalProcessClient client=new LocalProcessClient();
        File tmpDir=null;
        String systemPath=System.getenv("PATH");
        LOGGER.debug("System path is PATH={}",systemPath);
        try {
            
            tmpDir=PROBE_PSEUDO_ECOSYSTEM.createTempDirectoryTree();
            final String pomPath = injectedPom.getCanonicalPath();
            final String probeTestDirPath = PROBE_PSEUDO_ECOSYSTEM.getTestDirectory().getCanonicalPath();

            String probeCommandLine = buildMavenCommand(pomPath, tmpDir, probeTestDirPath);

            ShellResult engineCallResult = client.runLocalProcessCommand(probeCommandLine, injectedPom.getParentFile(), 60000, 200000);
            LOGGER.info("Here is the Squash TA engine output during component data extraction:\n{}",engineCallResult.getStdout());
            if(LOGGER.isWarnEnabled() && engineCallResult.getStderr().length()>0){
                LOGGER.warn("The Squash TA engine has issued non-fatal error messages during commonent registry data extraction.");
            }
            if(engineCallResult.getExitValue()!=0){
                throw new EngineLinkException(FAILED_TO_EXTRACT_COMPONENT_REGISTRY_DATA+
                        "\ncmdLine="+engineCallResult.getCommand()+
                        "\nstderr="+engineCallResult.getStderr()+
                        "\nstdout="+engineCallResult.getStdout()
                );
            }
        } catch (
                InterruptedException    |
                        TimeoutException        |
                        ParseException
                        ex) {
            throw new EngineLinkException(FAILED_TO_EXTRACT_COMPONENT_REGISTRY_DATA,ex);
        }finally{
            if(tmpDir!=null && tmpDir.exists()){
                try{
                    FileTree.FILE_TREE.clean(tmpDir);
                }catch(IOException e){
                    LOGGER.warn("Failed to clean temporary directory {}",tmpDir.getAbsolutePath(),e);
                }
            }
        }
    }

    private String buildMavenCommand(final String pomPath, File tmpDir, final String probeTestDirPath) throws IOException {
        
        String probeCommandLine;
        if(Platform.getLocalPlatform() instanceof WindowsPlatform){
            String mvnPath=null;
            String systemPath=System.getenv("PATH");
            final String[] toolPath = systemPath.split(File.pathSeparator);
            mvnPath = searchMavenInToolPath(toolPath);
            if(mvnPath==null){
                mvnPath= searchMavenInToolPath(additionalToolPath);
            }
            if(mvnPath==null){
                LOGGER.warn("Maven not found by looking up the path. Testing automatic search as a fallback. Failure hazard.");
                mvnPath="mvn";
            }
            probeCommandLine = "cmd /C \"\""+mvnPath+"\" "+
                "-f \""+pomPath+"\" "+
                "org.squashtest.ta:squash-ta-maven-plugin::run "+
                "-Djava.io.tmpdir=\""+tmpDir.getCanonicalPath()+"\" "+
                "-Dta.src.tests=\""+probeTestDirPath+"\" -Dta.test.suite="+PROBE_PSEUDO_ECOSYSTEM.PROBE_TEST_NAME+"\"";
        }else{
            String mvnPath=null;
            String systemPath=System.getenv("PATH");
            final String[] toolPath = systemPath.split(File.pathSeparator);
            mvnPath = searchMavenInToolPath(toolPath);
            if(mvnPath==null){
                mvnPath= searchMavenInToolPath(additionalToolPath);
            }
            if(mvnPath==null){
                LOGGER.warn("Maven not found by looking up the path. Testing automatic search as a fallback. Failure hazard.");
                mvnPath="mvn";
            }
            probeCommandLine = mvnPath+" -f \""+pomPath+"\" "+
                "org.squashtest.ta:squash-ta-maven-plugin::run "+
                "-Djava.io.tmpdir=\""+tmpDir.getCanonicalPath()+"\" "+
                "-Dta.src.tests=\""+probeTestDirPath+"\" -Dta.test.suite="+PROBE_PSEUDO_ECOSYSTEM.PROBE_TEST_NAME;
        }
        return probeCommandLine;
    }

    private String searchMavenInToolPath(final String[] toolPath) {
        String mvnPath=null;
        final String mvnExecName;
        if(Platform.getLocalPlatform() instanceof WindowsPlatform){
            mvnExecName = "mvn.cmd";
        }else{
            mvnExecName = "mvn";
        }
        for(String pathEl:toolPath){
            final File mvnFile = new File(new File(pathEl), mvnExecName);
            if(mvnFile.exists()){
                final String absolutePath = mvnFile.getAbsolutePath();
                //Somme embedded maven instances (among others) may have lost execute permissions...
                if(Platform.getLocalPlatform() instanceof WindowsPlatform || mvnFile.canExecute()){
                    LOGGER.debug("Execution permitted for {}, or the current platform doesn't care");
                }else if(mvnFile.setExecutable(true)){
                    LOGGER.info("Made {} executable.", absolutePath);
                }else{
                    LOGGER.warn("Failed to make {} executable, file extraction will most probably fail.", absolutePath);
                }
                mvnPath=absolutePath;
                break;
            } 
        }
        return mvnPath;
    }
}
