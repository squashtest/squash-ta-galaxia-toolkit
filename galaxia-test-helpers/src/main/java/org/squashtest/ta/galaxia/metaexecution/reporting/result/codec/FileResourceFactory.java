/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result.codec;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.galaxia.utils.FileCanonicalPath;

/**
 * Deserialization of file resources from file resource data and file storage.
 * @author ericdegenetais
 */
@JsonDeserialize(builder=FileResourceFactory.class)
class FileResourceFactory{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FileResourceFactory.class);
    
    private String file;
    
    private File baseDir;
    
    /**
     * This method is used to configure base file repository for deserialzation.
     * @param baseDir
     * @return 
     */
    @JacksonInject("baseDir")
    public FileResourceFactory withBaseDir(File baseDir){
        LOGGER.debug("Using FileResource base dir {}",new FileCanonicalPath(baseDir));
        this.baseDir=baseDir;
        return this;
    }
    
    @JsonProperty("file")
    public FileResourceFactory withFile(String file){
        LOGGER.debug("This factory is called for FileResource {}.",file);
        this.file=file;
        return this;
    }
    
    public FileResource build(){
        final File attachmentContent = new File(baseDir,file);
        if(!attachmentContent.exists()){
            LOGGER.warn("File resource content {} was apparently lost since serialization occurred.",new FileCanonicalPath(attachmentContent));
        }
        return new FileResource(attachmentContent);
    }
}
