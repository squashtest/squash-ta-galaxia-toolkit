/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.Properties;
import org.squashtest.ta.framework.test.result.ResourceGenerator;

/**
 * Implementation of the resource generator object suitable for json deserialization.
 * @author edegenetais
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,defaultImpl = ResourceGeneratorImpl.class)
@JsonSubTypes(@JsonSubTypes.Type(ResourceGeneratorImpl.class))
@JsonTypeName("default")
public class ResourceGeneratorImpl implements ResourceGenerator{
    
    private String name;
    private String generatorType;
    private Properties configuration;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public ResourceGeneratorImpl(
            @JsonProperty("name")           String name, 
            @JsonProperty("generatorType")  String generatorType, 
            @JsonProperty("configuration")  Properties configuration
    ) {
        this.name = name;
        this.generatorType = generatorType;
        this.configuration = configuration;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getGeneratorType() {
        return generatorType;
    }

    @Override
    public Properties getConfiguration() {
        return configuration;
    }

}
