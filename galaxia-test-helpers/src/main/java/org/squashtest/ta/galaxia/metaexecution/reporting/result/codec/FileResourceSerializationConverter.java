/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result.codec;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.galaxia.utils.FileCanonicalPath;

/**
 * This serializer transforms the {@link FileResource} object
 * into conveniently laid out file storage and a reference to that storage
 * to keep the json file small.
 * In addition, this makes the resulting tree (json+file storage directory) portable
 * from one location to anohter, and from one platform to another.
 * 
 * @author edegenetais
 */
@JsonSerialize(converter = FileResourceSerializationConverter.class)
@JsonDeserialize(builder=FileResourceFactory.class)
@JsonTypeName("file")
class FileResourceSerializationConverter implements Converter<FileResource, FileResourceData>{
    private static final Logger LOGGER = LoggerFactory.getLogger(FileResourceSerializationConverter.class);

    static ThreadLocal<File> baseDir=new ThreadLocal<>();

    
    
    @Override
    public FileResourceData convert(FileResource in) {
        try {
            File attachedStorage;
            if(in.getFile().isDirectory()){
                attachedStorage = createDirectoryStorage();
                FileUtils.copyDirectory(in.getFile(), attachedStorage);
            }else{
                attachedStorage=createFileStorage();
                FileUtils.copyFile(in.getFile(), attachedStorage);
            }
            return new FileResourceData(attachedStorage.getName());
        } catch (IOException ex) {
            LoggerFactory.getLogger(FileResourceSerializationConverter.class).warn("Lost attached resource in I/O error",ex);
            
            createNofileTxtPlaceholderIfNeedBe();
            
            return new FileResourceData("nofile.txt");
        }
    }

    private void createNofileTxtPlaceholderIfNeedBe() {
        final File file = new File(getBaseDirectory(),"nofile.txt");
        if(!file.exists()){
            try(OutputStreamWriter destination=new OutputStreamWriter(new FileOutputStream(file), "utf-8");){
                destination.write("Sorry, this attached resource content was not found, or was lost, by the result serializer.\n");
            } catch (IOException ex1) {
                LOGGER.debug("Failed to create placeholder file nofile.txt. Dangling file reference ahead.",ex1);
            }
        }
    }

    private File createFileStorage() throws IOException {
        if(testAlwaysUseSameAttachmentName()){
            logTestModeWarning();
            return new File(getBaseDirectory(),"attach.ser");
        }else{
            return File.createTempFile("attach", ".ser", getBaseDirectory());
        }
    }

    private void logTestModeWarning() {
        LOGGER.warn("Warning, unique attached file name shunted for test purpose. If not doing tests, attachment contents will be lost!");
    }

    private static boolean testAlwaysUseSameAttachmentName() {
        return "true".equals(System.getProperty("org.squashtest.ta.galaxia.metaexecution.reporting.result.codec.always.same.file"));
    }
    
    private File createDirectoryStorage() throws IOException {
        if(testAlwaysUseSameAttachmentName()){
            logTestModeWarning();
            final File dir = new File(getBaseDirectory(),"attach.dir.ser");
            dir.mkdirs();
            return dir;
        }else{
            final Path baseDirectoryPath = Paths.get(getBaseDirectory().toURI());
            Path newDir=java.nio.file.Files.createTempDirectory(baseDirectoryPath, "resource");
            return newDir.toFile();
        }
    }

    private File getBaseDirectory() {
        final File baseDirectory = baseDir.get();
        if(baseDirectory.mkdirs()){
            LOGGER.debug("Successfully created attachment directory {}",new FileCanonicalPath(baseDirectory));
        }else if(LOGGER.isDebugEnabled()){
            LOGGER.debug("Attachment directory {} {}.",new FileCanonicalPath(baseDirectory),baseDirectory.canWrite()?"existed":"could not be created.");
        }else{
            LOGGER.debug("");
        }
        return baseDirectory;
    }

    @Override
    public JavaType getInputType(TypeFactory tf) {
        return tf.constructType(FileResource.class);
    }

    @Override
    public JavaType getOutputType(TypeFactory tf) {
        return tf.constructType(FileResourceData.class);
    }

}
