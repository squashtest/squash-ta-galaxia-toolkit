/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink.probe;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Persisting connector resources - including its executor pool.
 * @author edegenetais
 */
public class BasicConnectorScheduling implements ConnectorScheduling{
    //The given map will be used 
    private static Map<String,ExecutorService> executorMap=new ConcurrentHashMap<>();

    private ExecutorService executor;
    
    public BasicConnectorScheduling(File testProjectPom) throws IOException {
        //To be reviewed when more tools are added : given their long duration, we should use a queue 
        synchronized(executorMap){            
                this.executor=executorMap.get(testProjectPom.getCanonicalPath());
                if(this.executor==null){
                    executor=Executors.newSingleThreadExecutor();
                    executorMap.put(testProjectPom.getCanonicalPath(), executor);
                }
        }
    }

    @Override
    public ExecutorService getExecutor() {
        return executor;
    }
}
