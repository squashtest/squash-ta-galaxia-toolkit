/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result.codec;

import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.File;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;
import org.squashtest.ta.framework.exception.BrokenTestException;
import org.squashtest.ta.framework.exception.InstructionRuntimeException;
import org.squashtest.ta.framework.test.instructions.ResourceName;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.PhaseResult;
import org.squashtest.ta.framework.test.result.ResourceGenerator;
import org.squashtest.ta.framework.test.result.ResourceMetadata;
import org.squashtest.ta.framework.test.result.TargetInitialisationResult;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.ResourceGeneratorImpl;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.ResourceMetaDataImpl;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.TargetInitialisationResultAdaptorDaughter;

/**
 * This class handles the life-cycle of the codec, making sure configuration
 * is properly applied, and that the operation-related state is properly bound and cleand-up
 * at appropriate time.
 * 
 * @author edegenetais
 */
class TestResultCodecSession implements AutoCloseable{

    private static final Logger LOGGER = LoggerFactory.getLogger(TestResultCodecSession.class);
    
    private ObjectMapper objectMapper;

    public TestResultCodecSession() {
        objectMapper = new ObjectMapper();
        setCodecUp(objectMapper);
    }

    public ObjectMapper buildJsonCodec() {
        return objectMapper;
    }

    public void bindInjectableValues(Map<String,Object> injectableValues){
         objectMapper.setInjectableValues(new InjectableValues.Std(injectableValues));
         FileResourceSerializationConverter.baseDir.set((File)injectableValues.get("baseDir"));
    }
    
    private void setCodecUp(final ObjectMapper objectMapper) {
        
        objectMapper.addMixIn(TargetInitialisationResult.class, TargetInitialisationResultAdaptorDaughter.class);
        
        injectMixinIfClassExists("org.squashtest.ta.backbone.test.DefaultSuiteResult", objectMapper, DefaultSuiteResultMixin.class);
        objectMapper.addMixIn(EcosystemResult.class, EcosystemResultMixin.class);
        injectMixinIfClassExists("org.squashtest.ta.backbone.test.DefaultEcosystemResult", objectMapper, DefaultEcosystemResultMixin.class);
        
        objectMapper.addMixIn(TestResult.class, TestResultMixin.class);
        injectMixinIfClassExists("org.squashtest.ta.backbone.test.DefaultTestResult", objectMapper, DefaultTestResultMixin.class);
        injectMixinIfClassExists("org.squashtest.ta.backbone.test.DefaultPhaseResult", objectMapper, DefaultPhaseResultMixin.class);
        objectMapper.addMixIn(PhaseResult.class, PhaseResultMixin.class);
        
        objectMapper.addMixIn(ExecutionDetails.class, ExecutionDetailMixin.class);
        injectMixinIfClassExists("org.squashtest.ta.backbone.test.CompositeExecutionDetails", objectMapper, CompositeExecutionDetailsMixin.class);
        injectMixinIfClassExists("org.squashtest.ta.backbone.test.DefaultExecutionDetails", objectMapper, DefaultExecutionDetailsMixin.class);
        injectMixinIfClassExists("org.squashtest.ta.maven.list.ListTestResult$ListExecutionDetails", objectMapper, ListExecutionDetailsMixin.class);
        
        objectMapper.addMixIn(ResourceMetadata.class, ResourceMetaDataImpl.class);
        objectMapper.addMixIn(ResourceGenerator.class, ResourceGeneratorImpl.class);
        objectMapper.addMixIn(ResourceName.class, ResourceNameFactory.class);
        
        objectMapper.addMixIn(Resource.class, ResourceMixin.class);
        objectMapper.addMixIn(FileResource.class, FileResourceSerializationConverter.class);
        
        objectMapper.addMixIn(Exception.class, ExceptionMixin.class);
        objectMapper.addMixIn(AssertionFailedException.class, AssertionFailedBuilder.class);
        objectMapper.addMixIn(BinaryAssertionFailedException.class, BinaryAssertionFailedBuilder.class);
        objectMapper.addMixIn(BrokenTestException.class, BrokenTestBuilder.class);
        objectMapper.addMixIn(InstructionRuntimeException.class, BrokenTestBuilder.class);
        
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        //This under there was added to tolerate some beans in the DatabaseResource
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }

    /**
     * This allows conditional setup of the object mapper for a class through the mixin system IF it is in the classpath.
     * @param targetClassName name of the target class
     * @param objectMapper mapper instance
     * @param mixinClass the mixin class to register if the target class is indeed available in the classpath.
     */
    private void injectMixinIfClassExists(final String targetClassName, final ObjectMapper objectMapper, final Class<?> mixinClass) {
        try {
            Class<?> targetClass = Class.forName(targetClassName);
            objectMapper.addMixIn(targetClass, mixinClass);
            LOGGER.debug("Class {} was found, setting up its json mapper tuning.",targetClassName);
        } catch (ClassNotFoundException ex) {
            LOGGER.debug("Class {} was not found, NOT setting json mapper tuning for it.", targetClassName, ex);
        } 
    }

    @Override
    public void close() {
        FileResourceSerializationConverter.baseDir.remove();
    }

}
