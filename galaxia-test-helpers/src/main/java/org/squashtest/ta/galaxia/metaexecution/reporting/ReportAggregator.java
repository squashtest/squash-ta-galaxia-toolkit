/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.codec.TestResultCodec;
import org.squashtest.ta.galaxia.utils.FileCanonicalPath;

/**
 * This is a reporting wrapper that aggregates test results with previous phase 
 * test results for aggregated report in multi-stage builds.
 * 
 * @author edegenetais
 */
public class ReportAggregator implements ResultExporter{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportAggregator.class);
    
    private String resultStorage="previous-results";
    
    private String outputDirectoryName=".";
    
    private List<ResultExporter> exporters;
    
    private ReportType type;
    
    private TestResultCodec codec=new TestResultCodec();
    
    public void setExporters(List<ResultExporter> exporters) {
        this.exporters = exporters;
    }
    
    public void setResultStorage(String storageLocation){
        this.resultStorage=storageLocation;
    }
    
    @Override
    public void write(File targetDirectory, final SuiteResult result) throws IOException {
        File resultStorageFile=new File(targetDirectory,this.resultStorage+"/"+type.name()+".json");
         
        LOGGER.debug("Storage will be located in {}",new FileCanonicalPath(resultStorageFile));
        
        SuiteResult globalResult = computeAndPersistAggregateResult(resultStorageFile, result);
        
        LOGGER.debug("Aggregate execution result written to {}, exporting.",new FileCanonicalPath(resultStorageFile));
        
        exportAggregateResult(targetDirectory, globalResult);
        
        LOGGER.info("Aggregate result written to {} and exported.",new FileCanonicalPath(resultStorageFile));
    }

    private void exportAggregateResult(File targetDirectory, SuiteResult globalResult) {
        for(ResultExporter exporter:exporters){
            try{
                final File exporterTargetDirectory = new File(targetDirectory,exporter.getOutputDirectoryName());
                exporterTargetDirectory.mkdirs();
                exporter.write(exporterTargetDirectory, globalResult);
            }catch(Exception e){
                LOGGER.warn("Filed export of aggregate report for exporter {}",exporter,e);
            }
        }
    }

    private SuiteResult computeAndPersistAggregateResult(File resultStorageFile, final SuiteResult result) throws IOException {
        SuiteResult globalResult;
        if(resultStorageFile.exists()){
            LOGGER.debug("Previous report found, loading and merging.");
            
            final SuiteResult previousResults=codec.load(resultStorageFile);
            globalResult=new AggregateSuiteResult(previousResults, result);
            
        }else{
            globalResult=result;
            
            if(resultStorageFile.getParentFile().getCanonicalFile().exists() || resultStorageFile.getParentFile().getCanonicalFile().mkdirs()){
                LOGGER.debug("No previous result found, the new results will be written to the storage location.");
            }else{
                LOGGER.error("Storage location cannot be created at {} Aggregation will fail.",new FileCanonicalPath(resultStorageFile.getParent()));
            }
        }
        codec.write(globalResult, resultStorageFile);
        return globalResult;
    }

    
    @Override
    public String getOutputDirectoryName() {
        return outputDirectoryName;
    }

    @Override
    public void setOutputDirectoryName(String directoryName) {
        this.outputDirectoryName=directoryName;
    }

    @Override
    public void setReportType(ReportType type) {
        //propagate to delegated reporters
        for(ResultExporter exporter:exporters){
            exporter.setReportType(type);
        }
        this.type=type;
    }

}
