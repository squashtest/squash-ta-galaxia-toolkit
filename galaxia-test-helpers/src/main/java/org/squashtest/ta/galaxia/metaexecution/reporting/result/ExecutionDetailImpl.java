/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.Collection;
import java.util.List;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.test.result.ResourceAndContext;

/**
 *
 * @author edegenetais
 */
@JsonTypeName("default")
public class ExecutionDetailImpl extends AbstractResult implements ExecutionDetails{
    private List<ExecutionDetails> childrens;
    private List<ExecutionDetails> errorOrFailedChildrens;
    private int errorOrFailedWithContinue;
    private List<ResourceAndContext> resourcesAndContext;
    protected Exception caughtException;
    protected String instructionAsText;
    private int instructionNumber;
    private int instructionNumberInPhase;
    @JsonProperty("instructionType")
    protected InstructionType instructionType;
    
    @JsonGetter("instructionType")
    @Override
    public InstructionType instructionType() {
        return instructionType;
    }

    @Override
    public int instructionNumberInPhase() {
        return instructionNumberInPhase;
    }

    
    @Override
    public int instructionNumber() {
        return instructionNumber;
    }

    @JsonGetter("instructionAsText")
    @Override
    public String instructionAsText() {
        return instructionAsText;
    }
    
    @JsonGetter("caughtException")
    @Override
    public Exception caughtException() {
        return caughtException;
    }

    @Override
    public Collection<ResourceAndContext> getResourcesAndContext() {
        return resourcesAndContext;
    }

    @Override
    public void addResourceAndContext(ResourceAndContext contextResource) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ExecutionDetails> getChildrens() {
        return childrens;
    }

    @Override
    public List<ExecutionDetails> getErrorOrFailedChildrens() {
        return errorOrFailedChildrens;
    }

    @Override
    public int getErrorOrFailedWithContinue() {
        return errorOrFailedWithContinue;
    }
    
}
