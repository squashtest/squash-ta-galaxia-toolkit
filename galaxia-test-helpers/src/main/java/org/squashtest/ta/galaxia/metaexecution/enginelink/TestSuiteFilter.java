/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.testsuite.TestSuiteCodec;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.testsuite.Test;
import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.testsuite.TestSuite;
import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.SpecSyntaxException;
import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.TestSpec;
import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.TestSpecFactory;

/**
 * This class is a filter to write partial TestSuites json files retaining 
 * caller data such as callback coordinates.
 * This can be used to split the initial test suite between stages in a multi-stage pipeline.
 * 
 * @author edegenetais
 */
public class TestSuiteFilter extends TestSuiteCodec {
    
    private Set<TestSpec> specs;
    
    /**
     * Build a filter based on one or more specs.
     * @param specSet one or more specs of test cases to keep, in the maven/ant format.
     * @throws org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.SpecSyntaxException if one or more specification is invalid.
     */
    public TestSuiteFilter(String... specSet) throws SpecSyntaxException {
        if(specSet.length==0){
            LoggerFactory.getLogger(TestSuiteFilter.class).warn("You have given no spec, this test will never keep any test!");
        }
        specs=new HashSet<>(specSet.length);
        TestSpecFactory specFactory=new TestSpecFactory();
        for(String spec:specSet){
            specs.add(specFactory.getSpec(spec));
        }
        
    }
    
    public File filter(File testSuite) throws IOException{
        TestSuite suite = loadTestSuite(testSuite);
        List<Test> filteredTestList=new ArrayList<>();
        for(Test t:suite.getTest()){
            for(TestSpec spec:specs){
                if(spec.accept(t.getScript())){
                    filteredTestList.add(t);
                    break;
                }
            }
        }
        TestSuite filteredSuite=new TestSuite(filteredTestList);
        final File filteredTestSuiteFile = File.createTempFile("testsuite", ".json");
        writeTestSuite(filteredTestSuiteFile, filteredSuite);
        return filteredTestSuiteFile;
    }
}