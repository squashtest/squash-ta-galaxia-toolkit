/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.conditions;

import org.squashtest.ta.galaxia.metaexecution.enginelink.MeFirstIslandClassLoaderFactory;
import org.squashtest.ta.galaxia.metaexecution.enginelink.ClassLoaderFactory;
import org.squashtest.ta.galaxia.metaexecution.enginelink.TargetFinder;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.galaxia.utils.FileCanonicalPath;

/**
 * SQL query restart condition class.
 * @author edegenetais
 */
public class SQLCondition implements Condition {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SQLCondition.class);
    
    private static final String INCOMPATIBLE_DB_PLUGIN_MSG = "The squash-ta-plugin-db version from the test project is not compatible with this galaxia test helper version.";

    private static final String[] PARAMETER_NAMES = {"projectRoot","targetName","sqlQuery","expectedResult"};
    private static final Set<String> PARAMETER_NAMES_SET = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(PARAMETER_NAMES)));
    
    protected static ClassLoaderFactory FACTORY = new MeFirstIslandClassLoaderFactory();
    
    /*
    * End of testability code.
     */
    
    private String sqlQuery;
    private Method connectionFactory;
    private Target target;
    private String targetName;
    private String projectRoot;
    private final String expectedResult;
    
    public SQLCondition(String projectRoot, String targetName, String sqlQuery, String expectedResult) 
            throws InvalidConditionSetupException {
        
        LOGGER.debug("Created SQLCondition '{}=>{}' on target {} for project {}",sqlQuery,expectedResult,targetName,projectRoot);
        
        this.expectedResult = expectedResult;
        this.sqlQuery=sqlQuery;
        this.targetName=targetName;
        this.projectRoot=projectRoot;
        
        LOGGER.debug("Project root is {}",new FileCanonicalPath(projectRoot));
        
        final File projectRootDir = new File(projectRoot);    
        
         try {
                
                ClassLoader dbLoader = FACTORY.getTestProjectClassLoader(projectRootDir);
                Thread.currentThread().setContextClassLoader(dbLoader);
                Class<TargetCreator> creatorClass=(Class<TargetCreator>)dbLoader.loadClass("org.squashtest.ta.plugin.db.targets.DatabaseTargetCreator");
                TargetCreator creator=creatorClass.newInstance();
                
                target=new TargetFinder(projectRootDir).findTarget(targetName, creator);
                
                connectionFactory=target.getClass().getMethod("getConnection", new Class<?>[]{});
        
            } catch (ClassNotFoundException ex) {
                throw new InvalidConditionSetupException("The test project classpath information is not valid, or not complete.",ex);
            }catch (EngineLinkException ex) {
                throw new InvalidConditionSetupException("Failed to open or parse ",ex);
            }catch (InstantiationException | IllegalAccessException ex) {
                throw new InvalidConditionSetupException("The Squash TA framework version from test project is not binary comptible with this galaxia test helper version", ex);
            } catch (NoSuchMethodException ex) {
                throw new InvalidConditionSetupException(INCOMPATIBLE_DB_PLUGIN_MSG, ex);
            }
    }
    
    
    @Override
    public boolean check() throws ConditionException{
        target.init();

        try(Connection dbConn=(Connection)connectionFactory.invoke(target);){
            Statement s=dbConn.createStatement();
            ResultSet rs=s.executeQuery(sqlQuery);
            boolean result;
            if(rs.next()){
                final String actualResult = rs.getString(1);
                final boolean conditionValue = expectedResult.equals(actualResult);
                LOGGER.debug("Query '{}' extracted value '{}', while expecting '{}', conndition is '{}'",sqlQuery,actualResult,expectedResult,conditionValue);
                result=conditionValue;
            }else{
                result=false;
                LOGGER.debug("Query {} selected 0 lines, condition evaluiates to false.",sqlQuery);
            }
            if(rs.next()){
                LOGGER.warn("Your condition query {} should have a single result but yields multiple lines.",sqlQuery);
            }
            return result;
        }catch(SQLException sqle){
            throw new ConditionEvaluationFailure("Execution of the SQLCondition query '"+sqlQuery+"' failed.", sqle);
        }catch (IllegalAccessException ex) {
            throw new InvalidConditionSetupException(INCOMPATIBLE_DB_PLUGIN_MSG, ex);
        }  catch (InvocationTargetException ex) {
            throw new ConditionEvaluationFailure(sqlQuery, ex);
        }
        finally{
            target.cleanup();
        }
    }

    @Override
    public Object getInitialParm(String name) {
        switch(name){
            case "projectRoot":
                return projectRoot;
            case "targetName":
                return targetName;
            case "sqlQuery":
                return sqlQuery;
            case "expectedResult":
                return expectedResult;
            default:
                throw new IllegalArgumentException("Unknown parameter "+name);
        }
    }

    @Override
    public Set<String> getInitialParameterNames() {
        return PARAMETER_NAMES_SET;
    }
    
}

