/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.conditions;

/**
 * Exception thrown when condition setup fails.
 * @author edegenetais
 */
public class InvalidConditionSetupException extends ConditionException{

    /**
     * Constructor with message.
     * @param message the error message.
     */
    public InvalidConditionSetupException(String message) {
        super(message);
    }

    /**
     * Constructor with message and exception cause.
     * @param message the message.
     * @param cause the initial exception that caused setup failure.
     */
    public InvalidConditionSetupException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
