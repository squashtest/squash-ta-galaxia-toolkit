/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TargetInitialisationResult;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.EcosystemResultImpl;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.TargetInitialisationResultAdaptorDaughter;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.OrdinalAware;

/**
 *
 * @author edegenetais
 */
@JsonPropertyOrder(alphabetic = true)
class AggregateSuiteResult implements SuiteResult {

    private final SuiteResult previousResults;
    private final SuiteResult result;

    public AggregateSuiteResult(SuiteResult previousResults, SuiteResult result) {
        this.previousResults = previousResults;
        this.result = result;
    }

    @Override
    public String getProjectGroupId() {
        return aggregateValue(previousResults.getProjectGroupId(), result.getProjectGroupId());
    }

    private String aggregateValue(String previousValue, String newValue) {
        if (previousValue.equals(newValue)) {
            return previousValue;
        } else {
            return computeCompositeString(previousValue, newValue);
        }
    }

    private String computeCompositeString(String previousValue, String newValue) {
        Pattern versionLookup = Pattern.compile("^(?:[^\\/]+/)*" + newValue + "(?:/[^\\/]+)*$");
        Matcher matcher = versionLookup.matcher(previousValue);
        if (matcher.matches()) {
            return previousValue;
        } else {
            return previousValue + "/" + newValue;
        }
    }

    @Override
    public String getProjectArtifactId() {
        return aggregateValue(previousResults.getProjectArtifactId(), result.getProjectArtifactId());
    }

    @Override
    public String getProjectVersion() {
        return aggregateValue(previousResults.getProjectVersion(), result.getProjectVersion());
    }

    @Override
    public List<TargetInitialisationResult> getTargetInitialisationResults() {
        
        final List<TargetInitialisationResult> previousTargetInitResults = previousResults.getTargetInitialisationResults();
        final List<TargetInitialisationResult> newTargetInitResults = result.getTargetInitialisationResults();
        List<TargetInitialisationResult> globalList=new ArrayList<>(Math.max(previousTargetInitResults.size(), newTargetInitResults.size()));
            
        wrapperMerge(globalList, previousTargetInitResults, newTargetInitResults);
        
        return globalList;
    }
    
    /**
     * To avoid getting tripped by heterogenous type comparisons, we need to rebuild all
     * target results with our wrapper type.
     * @param globalList
     * @param previousResults
     * @param results 
     */
    private void wrapperMerge(List<TargetInitialisationResult> globalList, List<TargetInitialisationResult> previousResults, List<TargetInitialisationResult> results) {
        
        for(TargetInitialisationResult in:previousResults){
            globalList.add(new TargetInitialisationResultAdaptorDaughter(in));
        }
        for(TargetInitialisationResult in:results){
            TargetInitialisationResultAdaptorDaughter wrapper=new TargetInitialisationResultAdaptorDaughter(in);
            if(!globalList.contains(wrapper)){
                globalList.add(wrapper);
            }
        }
    }
    
    /**
     * Represents an occurence of an object in a list.
     * @param <I> item type
     */
    private class Occurrence<I>{
        private I item;
        private Integer listIndex;

        public Occurrence(I item, Integer listIndex) {
            this.item = item;
            this.listIndex = listIndex;
        }

        public I item() {
            return item;
        }

        public Integer listIndex() {
            return listIndex;
        }
        
    }
    
    @Override
    public List<? extends EcosystemResult> getSubpartResults() {
        
        final List<? extends EcosystemResult> previousResultList = previousResults.getSubpartResults();
        final List<? extends EcosystemResult> newResultList = result.getSubpartResults();
        
        List<EcosystemResult> globalResult = new ArrayList<>(previousResultList.size() + newResultList.size());
                
        Map<String,Occurrence<EcosystemResult>> lastOccurrence=new HashMap<>();
        
        mergeResultList(previousResultList, lastOccurrence, globalResult);
        
        mergeResultList(newResultList, lastOccurrence, globalResult);
        
        return globalResult;
    }

    private void mergeResultList(final List<? extends EcosystemResult> resultList, Map<String, Occurrence<EcosystemResult>> lastOccurrence, List<EcosystemResult> globalResult) {
        for(EcosystemResult er:resultList){
            
            String basename = getBasename(er);
            EcosystemResult addedObject=treatEcosystemOccurence(er, lastOccurrence, basename, globalResult);
            
            lastOccurrence.put(basename, new Occurrence(addedObject,globalResult.size()));
            globalResult.add(addedObject);
        }
    }

    private EcosystemResult treatEcosystemOccurence(EcosystemResult er, Map<String, Occurrence<EcosystemResult>> lastOccurrence, String basename, List<EcosystemResult> globalResult) {
        EcosystemResult addedObject=er;
        if(lastOccurrence.containsKey(basename)){
            Occurrence<EcosystemResult> last=lastOccurrence.get(basename);
            Integer lastOrdinal;
            final EcosystemResult item = last.item();
            if(item instanceof OrdinalAware){
                final OrdinalAware ordinalAwareLastItem = (OrdinalAware)item;
                lastOrdinal=ordinalAwareLastItem.ordinal();
                if(lastOrdinal==null){
                    ordinalAwareLastItem.setOrdinal(1);
                    lastOrdinal=1;
                }
            }else{
                lastOrdinal=1;
                EcosystemResultImpl lastWithOrdinal = makeEcosystemResultImpl(item, 1);
                globalResult.set(last.listIndex, lastWithOrdinal);
            }
            addedObject=makeEcosystemResultImpl(er, lastOrdinal+1);
        }
        return addedObject;
    }

    private EcosystemResultImpl makeEcosystemResultImpl(final EcosystemResult item, int ordinal) {
        final EcosystemResultImpl lastWithOrdinal = new EcosystemResultImpl(item);
        if(item instanceof OrdinalAware){
            lastWithOrdinal.setOrdinal(ordinal);
            lastWithOrdinal.setBaseName(((OrdinalAware)item).baseName());
        }else{
            lastWithOrdinal.setOrdinal(ordinal);
            lastWithOrdinal.setBaseName(item.getName());
        }
        return lastWithOrdinal;
    }

    private String getBasename(EcosystemResult er) {
        String basename;
        if(er instanceof OrdinalAware){
            basename=((OrdinalAware)er).baseName();
        }else{
            basename=er.getName();
        }
        return basename;
    }

    @Override
    public int getTotalTests() {
        return previousResults.getTotalTests() + result.getTotalTests();
    }

    @Override
    public int getTotalErrors() {
        return previousResults.getTotalErrors() + result.getTotalErrors();
    }

    @Override
    public int getTotalFailures() {
        return previousResults.getTotalFailures() + result.getTotalFailures();
    }

    @Override
    public int getTotalNotRun() {
        return previousResults.getTotalNotRun() + result.getTotalNotRun();
    }

    @Override
    public int getTotalNotFound() {
        return previousResults.getTotalNotFound() + result.getTotalNotFound();
    }

    @Override
    public int getTotalWarning() {
        return previousResults.getTotalWarning() + result.getTotalWarning();
    }

    @Override
    public int getTotalSuccess() {
        return previousResults.getTotalSuccess() + result.getTotalSuccess();
    }

    @Override
    public int getTotalPassed() {
        return previousResults.getTotalPassed() + result.getTotalPassed();
    }

    @Override
    public int getTotalNotPassed() {
        return previousResults.getTotalNotPassed() + result.getTotalNotPassed();
    }

    @Override
    public String getName() {
        return aggregateValue(previousResults.getName(), result.getName());
    }

    @JsonGetter("startTime")
    @Override
    public Date startTime() {
        return previousResults.startTime();
    }

    @JsonGetter("endTime")
    @Override
    public Date endTime() {
        return result.endTime();
    }

    @Override
    public GeneralStatus getStatus() {
        return previousResults.getStatus().mostSevereStatus(result.getStatus());
    }

    @Override
    public void cleanUp() {
        previousResults.cleanUp();
        result.cleanUp();
    }

}
