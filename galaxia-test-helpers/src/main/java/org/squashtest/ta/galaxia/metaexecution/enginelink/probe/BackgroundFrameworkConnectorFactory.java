/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink.probe;

import java.io.File;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;

/**
 * This class wraps a synchronous FrameworkConnector to add asynchrounous management to it.
 * @author edegenetais
 */
public class BackgroundFrameworkConnectorFactory {
    
    public BackgroundFrameworkConnector getFrameworkConnector(BackgroundFrameworkConnector.ErrorReporter errorSink, File pomPath, String... additionalPath) throws EngineLinkException{
        return new BackgroundFrameworkConnectorImpl(errorSink, pomPath, additionalPath);
    }
    
    public BackgroundFrameworkConnector getFrameworkConnector(BackgroundFrameworkConnector.ErrorReporter errorSink, BackgroundFrameworkConnector.SuccessListener listener, File pomPath, String... additionalPath) throws EngineLinkException{
        return new BackgroundFrameworkConnectorImpl(errorSink, pomPath, additionalPath);
    }
    
    public BackgroundFrameworkConnector getFrameworkConnector(BackgroundFrameworkConnector.ErrorReporter errorSink, File pomPath) throws EngineLinkException{
        return new BackgroundFrameworkConnectorImpl(errorSink,pomPath);
    }
    
    public BackgroundFrameworkConnector getFrameworkConnector(BackgroundFrameworkConnector.ErrorReporter errorSink, BackgroundFrameworkConnector.SuccessListener listener, File pomPath) throws EngineLinkException{
        return new BackgroundFrameworkConnectorImpl(errorSink,pomPath);
    }
}
