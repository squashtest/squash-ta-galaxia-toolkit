/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.conditions;

import java.lang.reflect.InvocationTargetException;

/**
 * Exception thrown when condition evaluation fails.
 * @author edegenetais
 */
public class ConditionEvaluationFailure extends ConditionException{

    public ConditionEvaluationFailure(String message) {
        super(message);
    }

    /**
     * The {@link InvocationTargetException} gives us nothing, we want the real exception thrown
     * by our target method.
     * @param message
     * @param causeWrapper 
     */
    public ConditionEvaluationFailure(String message, InvocationTargetException causeWrapper){
        this(message,causeWrapper.getTargetException());
    }
    
    public ConditionEvaluationFailure(String message, Throwable cause) {
        super(message, cause);
    }
    
}
