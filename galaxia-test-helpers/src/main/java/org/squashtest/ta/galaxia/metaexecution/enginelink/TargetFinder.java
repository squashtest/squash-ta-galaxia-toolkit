/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

import java.io.File;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.init.DefaultTestProjectWorkspaceBrowser;
import org.squashtest.ta.framework.components.Target;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;
import org.squashtest.ta.framework.tools.ComponentRepresentation;

/**
 *
 * @author edegenetais
 */
public class TargetFinder {

    private static final Logger LOGGER=LoggerFactory.getLogger(TargetFinder.class);
    
    private TestWorkspaceBrowser browser;

    /**
     * This was created for testability.
     * @param browser 
     */
    TargetFinder(TestWorkspaceBrowser browser) {
        this.browser=browser;
    }

    public TargetFinder(final File projectRootDir) {
        this(new DefaultTestProjectWorkspaceBrowser(new File(projectRootDir,"src/squashTA")));
    }
    
    public Target findTarget(String targetName, TargetCreator creator) {
        Target target = null;
        for (URL targetURL : browser.getTargetsDefinitions()) {
            LOGGER.trace("Checking target URL {}", targetURL.toExternalForm());
            if (extractTargetName(targetURL).equals(targetName)) {
                if (creator.canInstantiate(targetURL)) {
                    target = creator.createTarget(targetURL);
                    LOGGER.debug("Target definition acquired from URL {}", targetURL.toExternalForm());
                } else {
                    throw new IllegalArgumentException("Target " + targetName + " cannot be instanciated by the "+ new ComponentRepresentation(creator) + " target creator.");
                }
            }
        }
        if (target == null) {
            throw new IllegalArgumentException("The " + targetName + " target was not found in the project.");
        }
        return target;
    }

    public String extractTargetName(URL targetURL) {
        final String targetName = targetURL.getPath().substring(targetURL.getPath().lastIndexOf('/')+1);
        if(targetName.endsWith(".properties")){
            return targetName.substring(0,targetName.length()-".properties".length());
        }else{
            return targetName;
        }
    }
}
