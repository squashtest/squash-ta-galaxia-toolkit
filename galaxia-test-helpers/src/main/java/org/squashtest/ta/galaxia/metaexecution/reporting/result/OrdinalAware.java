/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Defines services for result objects that can exist in several distinct 
 * and ordered instances.
 * 
 * @author edegenetais
 */
public interface OrdinalAware {

    /**
     * Separate record of the ecosystem name of the original result.
     * @return the ecosystem name.
     * @see #getName() 
     */
    @JsonGetter("basename")
    String baseName();

    /**
     * The getName method is reimplemented with a new contract making room
     * for adding the {@link #ordinal()} value to the {@link #baseName()}.
     * @return the computed result instance name.
     */
    @JsonGetter("name")
    String getName();

    @JsonSetter("basename")
    void setBaseName(String basename);
    
    /**
     * An ordinal that makes the result instance unique among others with the same {@link #baseName()}
     * and gives it its rank.
     * @return 
     */
    @JsonGetter("ordinal")
    Integer ordinal();
    
    @JsonSetter("ordinal")
    void setOrdinal(Integer ordinal);
}
