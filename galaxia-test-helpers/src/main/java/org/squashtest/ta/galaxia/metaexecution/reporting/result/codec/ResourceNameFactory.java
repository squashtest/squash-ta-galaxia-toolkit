/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result.codec;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.squashtest.ta.framework.test.instructions.ResourceName;

/**
 * Deserialzation helper for resource names.
 * @author edegenetais
 */
@JsonDeserialize(builder = ResourceNameFactory.class)
class ResourceNameFactory {
    
    private String name;
    private ResourceName.Scope scope;
    
    @JsonProperty("name")
    public ResourceNameFactory withName(String name){
        this.name=name;
        return this;
    }
    @JsonProperty("scope")
    public ResourceNameFactory whithScope(ResourceName.Scope scope){
        this.scope=scope;
        return this;
    }
    
    public ResourceName build(){
        return new ResourceName(scope, name);
    }
}
