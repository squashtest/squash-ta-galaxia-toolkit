/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.core.tools.io.BinaryData;
import org.squashtest.ta.galaxia.utils.FileCanonicalPath;
import org.squashtest.ta.xml.functions.library.Transform;

/**
 *
 * @author edegenetais
 */
public class GalaxiaInstrumentInjector {

    private static final Logger LOGGER = LoggerFactory.getLogger(GalaxiaInstrumentInjector.class);
    
    protected final File injectionStyleSheet;

    protected GalaxiaInstrumentInjector(final String transformReference) throws EngineLinkException {
        try {
            injectionStyleSheet = File.createTempFile("galaxiaInjector", ".xslt");
            injectionStyleSheet.deleteOnExit();
            new BinaryData(getClass().getResource(transformReference)).write(injectionStyleSheet);
        } catch (IOException ex) {
            throw new EngineLinkException("Failed to deploy engine injector XSLT sheet", ex);
        }
    }

    /**
     * Add galaxia configuration to the testproject pom.
     *
     * @param projectPom
     * @return
     * @throws EngineLinkException
     */
    public File inject(File projectPom) throws EngineLinkException {
        File injectedPom=null;
        try {
            if(projectPom.canRead()){
                injectedPom = File.createTempFile("galaxia", "pom.xml", projectPom.getParentFile());
                Transform t = new Transform(injectionStyleSheet, false);
                t.transform(projectPom, injectedPom);
                return injectedPom;
            }else{
                throw new EngineLinkException("Cannot "+(projectPom.exists()?"access":"find")+" target pom "+projectPom.getAbsolutePath());
            }
        } catch (RuntimeException/* Yup ! Any ! */ | IOException ex) {
            if(injectedPom!=null && injectedPom.exists()){
                if(injectedPom.delete()){
                    LOGGER.debug("Successfully deleted file {}",new FileCanonicalPath(injectedPom));
                }else{
                    LOGGER.warn("Failed to delete existing file {}",new FileCanonicalPath(injectedPom));
                    injectedPom.deleteOnExit();
                }
            }
            throw new EngineLinkException("Failed to inject galaxia instruments into test project configuration", ex);
        }
    }
    
}
