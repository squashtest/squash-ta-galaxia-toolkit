/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink.probe;

import java.util.concurrent.Callable;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.metaexecution.enginelink.FrameworkConnector;

/**
 *
 * @author edegenetais
 */
class DSLComponentRegistryBuildTask implements Callable<SquashDSLComponentRegistry> {

    private BackgroundFrameworkConnector.ErrorReporter errorSink;

    private FrameworkConnector connector;

    public DSLComponentRegistryBuildTask(BackgroundFrameworkConnector.ErrorReporter errorSink, FrameworkConnector connector) {
        this.errorSink = errorSink;
        this.connector = connector;
    }
    
    @Override
    public SquashDSLComponentRegistry call() throws Exception {
        try {
            return connector.getSquashDSLComponentRegistry();
        } catch (Error | Exception e) {
            if (errorSink == null) {
                LoggerFactory.getLogger(DSLComponentRegistryBuildTask.class).debug("Component registry extraction failure.", e);
            } else {
                errorSink.report(e);
            }
            throw e;
        }
    }

}
