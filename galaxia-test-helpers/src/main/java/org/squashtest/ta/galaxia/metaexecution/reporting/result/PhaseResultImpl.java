/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.List;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.Phase;
import org.squashtest.ta.framework.test.result.PhaseResult;

/**
 *
 * @author edegenetais
 */
@JsonTypeName("default")
public class PhaseResultImpl extends AbstractInstructionSequence implements PhaseResult{
    
    private List<ExecutionDetails> allInstructions;
    private List<ExecutionDetails> failedInstructions;
    private Phase phase;
    private int failedOrErrorWithContinue;
    
    @Override
    public List<ExecutionDetails> getAllInstructions() {
        return allInstructions;
    }

    @Override
    public List<ExecutionDetails> getFailedInstructions() {
        return failedInstructions;
    }

    @Override
    public int getFailedOrErrorWithContinue() {
        return failedOrErrorWithContinue;
    }

    @Override
    public Phase getPhase() {
        return phase;
    }

    @Override
    public void setPhaseStatus(GeneralStatus phaseStatus) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
