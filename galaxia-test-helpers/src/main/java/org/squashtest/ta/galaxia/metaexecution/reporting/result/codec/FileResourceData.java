/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result.codec;

import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Intermediate representation of file resources used for serialization.
 * @author edegenetais
 */
@JsonTypeName("file")
class FileResourceData {

    /** 
     * Referenced file name as string to avoid unwanted treatment 
     * brought by the {@link java.io} functionalities.
     */
    private String file;

    /**
     * Full initialization constructor.
     * @param file 
     */
    public FileResourceData(String file) {
        this.file = file;
    }

    /**
     * Getter fore the {@link #file} property.
     * @return 
     */
    public String getFile() {
        return file;
    }

}
