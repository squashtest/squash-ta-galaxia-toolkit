/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.TestResult;

/**
 * Implementation of the ecosystem result object that supports json deserialization
 * AND the {@link OrdinalAware} functionality.
 * Ecosystem results can exist in several ordered instances in an aggregated result.
 * @author edegenetais
 */
@JsonTypeName("default")
public class EcosystemResultImpl extends AbstractTestGroup implements EcosystemResult, OrdinalAware{
    private TestResult setupResult;
    private TestResult tearDownResult;
    protected List<? extends TestResult> subpartResults=new ArrayList<>();
    
    @JsonProperty("ordinal")
    private Integer ordinal;
    
    private String baseName;

    public EcosystemResultImpl() {
    }

    public EcosystemResultImpl(EcosystemResult original) {
        
        this.endTime=original.endTime();
        this.name=original.getName();
        this.setupResult = original.getSetupResult();
        this.startTime = original.startTime();
        this.status = original.getStatus();
        this.subpartResults = original.getSubpartResults();
        this.tearDownResult = original.getTearDownResult();
        this.totalErrors = original.getTotalErrors();
        this.totalFailures = original.getTotalFailures();
        this.totalNotFound = original.getTotalNotFound();
        this.totalNotPassed = original.getTotalNotPassed();
        this.totalNotRun = original.getTotalNotRun();
        this.totalPassed = original.getTotalPassed();
        this.totalSuccess = original.getTotalSuccess();
        this.totalTests = original.getTotalTests();
        this.totalWarning = original.getTotalWarning();
        
    }
    
    @JsonGetter("basename")
    @Override
    public String baseName(){
        if(baseName==null){
            return super.getName();
        }else{
            return baseName;
        }
    }
    
    @Override
    public void setBaseName(String basename){
        this.baseName=basename;
    }
    
    @Override
    @JsonGetter("name")
    public String getName(){
        if(ordinal==null){
            return super.getName();
        }
        if(baseName==null){
            baseName=super.getName();
        }
        return baseName+"["+ordinal+"]";
    }
    
    @Override
    public TestResult getSetupResult() {
        return setupResult;
    }

    @Override
    public TestResult getTearDownResult() {
        return tearDownResult;
    }

    @Override
    public List<? extends TestResult> getSubpartResults() {
        return subpartResults;
    }    

    @Override
    public Integer ordinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(Integer ordinal) {
        this.ordinal=ordinal;
        if(this.baseName==null){
            this.baseName=super.getName();
        }
    }
}
