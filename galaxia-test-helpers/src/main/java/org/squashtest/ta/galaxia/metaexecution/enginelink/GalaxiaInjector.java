/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

/**
 * This function injects galaxia runtime instruments into the test project pom.
 * For project analysis (eg : in IDEs), see {@link org.squashtest.ta.galaxia.metaexecution.enginelink.probe.ProbeInjector}.
 * @author edegenetais
 */
public class GalaxiaInjector extends GalaxiaInstrumentInjector {

    public GalaxiaInjector() throws EngineLinkException {
        super("galaxiaInjector-1.0.0.xslt");
    }
}
