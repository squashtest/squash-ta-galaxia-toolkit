/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result.codec;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.SuiteResultImpl;

/**
 * This class is the facade for the jackson-based codec fonctionalities for test
 * results.
 *
 * @author edegenetais
 */
public class TestResultCodec {

    public void write(SuiteResult result, File destination) throws IOException {

        try (
            TestResultCodecSession codecSession = new TestResultCodecSession();
            OutputStream out = new FileOutputStream(destination.getCanonicalFile());) {

            ObjectMapper objectCodec = codecSession.buildJsonCodec();
            JsonFactory factory = new JsonFactory(objectCodec);

            Map<String, Object> injectableValues = new HashMap<>(1);
            File baseFileTree = new File(destination.getParent(), "attachments");
            injectableValues.put("baseDir", baseFileTree);
            codecSession.bindInjectableValues(injectableValues);

            final JsonGenerator writer = factory.createGenerator(out, JsonEncoding.UTF8);
            writer.writeObject(result);
        }
    }

    public SuiteResult load(File source) throws IOException {
        try (TestResultCodecSession codecSession = new TestResultCodecSession()) {

            ObjectMapper objectCodec = codecSession.buildJsonCodec();
            JsonFactory factory = new JsonFactory(objectCodec);
            Map<String, Object> injectableValues = new HashMap<>(1);
            File baseFileTree = new File(source.getParent(), "attachments");
            injectableValues.put("baseDir", baseFileTree);
            codecSession.bindInjectableValues(injectableValues);

            final JsonParser jsonParser = factory.createParser(source);
            return jsonParser.readValueAs(SuiteResultImpl.class);
        }
    }
}
