/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.framework.test.result.TargetInitialisationResult;

/**
 * This class is used for aggregation of target initialization results.
 * It replaces {@link TargetInitialisationResult} instances in aggregate results,
 * and implements value semantics for target initialization results.
 * @author edegenetais
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        defaultImpl = TargetInitialisationResultAdaptorDaughter.class
)
@JsonTypeName("default")
public class TargetInitialisationResultAdaptorDaughter extends TargetInitialisationResult{

    private static final Logger LOGGER = LoggerFactory.getLogger(TargetInitialisationResultAdaptorDaughter.class);
    
    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public TargetInitialisationResultAdaptorDaughter(
            @JsonProperty("name")String name, 
            @JsonProperty("nature")String nature, 
            @JsonProperty("status")TargetStatus status
    ) {
        super(name, nature, status);
    }
    
    public TargetInitialisationResultAdaptorDaughter(TargetInitialisationResult toAdapt){
        super(toAdapt.getName(),toAdapt.getNature(),toAdapt.getStatus());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(getName());
        hash = 53 * hash + Objects.hashCode(getNature());
        hash = 53 * hash + Objects.hashCode(getStatus());
        return hash;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if(obj==this){
            return true;
        }else if(obj==null){
            return false;
        }else if(obj.getClass().equals(getClass())){
            final TargetInitialisationResultAdaptorDaughter asThisType = (TargetInitialisationResultAdaptorDaughter)obj;
            return asThisType.getName().equals(getName()) && 
                    asThisType.getNature().equals(getNature()) && 
                    asThisType.getStatus().equals(getStatus());
        }else if(obj.getClass().isAssignableFrom(getClass())){
            LOGGER.warn("{} is of parent class {}, returning as not equal because of symmetry.",obj,obj.getClass());
            return false;
        }else{
            return false;
        }
    }
        
}
