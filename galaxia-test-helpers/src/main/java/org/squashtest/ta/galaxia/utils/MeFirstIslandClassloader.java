/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.utils;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This classloader implements softlinking a private classloading domain through a selected interface package.
 * @author edegenetais
 */
public class MeFirstIslandClassloader extends URLClassLoader {
    
    private static final Logger LOGGER=LoggerFactory.getLogger(MeFirstIslandClassloader.class);
    
    private static final String INSTANCIATION_DEBUG_LOG = "Created "+MeFirstIslandClassloader.class.getName()+" classloader from these locations : {}";
    
    private final ClassLoader defaultCl;
    private final String bridgePackage;

    /**
     * Full initialization constructor from {@link Collection}{@literal <URL>} to make classpath building easier.
     * @param cpURLs classpath as a {@link Collection} of root URLs.
     * @param defaultCl classloader for the surrounding environment used to get brige package classes.
     * @param bridgePackageName the name of a package which classes will be taken from the bridge package.
     */
    public MeFirstIslandClassloader(Collection<URL> cpURLs, ClassLoader defaultCl, String bridgePackageName) {
        this(cpURLs.toArray(new URL[cpURLs.size()]), defaultCl, bridgePackageName);
    }

    /**
     * Full initialization constructor using the {@link URL} array format from {@link URLClassLoader}.
     * @param cpURLs classpath as an array of root URLs.
     * @param defaultCl classloader for the surrounding environment used to get brige package classes.
     * @param bridgePackageName the name of a package which classes will be taken from the bridge package.
     * @see java.net.URLClassLoader#URLClassLoader(java.net.URL[], java.lang.ClassLoader)
     */
    public MeFirstIslandClassloader(URL[] cpURLs, ClassLoader defaultCl, String bridgePackageName) {
        super(cpURLs, null);
        this.bridgePackage = bridgePackageName;
        this.defaultCl = defaultCl;
        LOGGER.debug(INSTANCIATION_DEBUG_LOG,(Object)cpURLs);
    }
    
    //Mostly overriden to offer debug / trace messages.
    @Override
    protected Class<?> findClass(String className) throws ClassNotFoundException {
        LOGGER.trace("Looking class {} up.", className);
        final Class<?> foundClass;
        try {
            foundClass = super.findClass(className);
        } catch (ClassNotFoundException e) {
            LOGGER.trace("Missed class {}", className, e);
            throw e;
        }
        return foundClass;
    }

    /**
     * This implementation of {@link ClassLoader#loadClass(java.lang.String)}
     * implements the 'me first behavior' that creates the classloading island, 
     * ie makes sure that used classes live in the classloading domain created by this classloader, 
     * so that most classes are loaded from it. 
     * This allows dynamic classpath changes to use the classpath defined by the target test project.
     * @param className name of the class to load.
     * @return the loaded class.
     * @throws ClassNotFoundException if the class was not found in the classloading island.
     */
    @Override
    public Class<?> loadClass(String className) throws ClassNotFoundException {
        LOGGER.trace("Loading class {}", className);
        Class<?> result;
        if (className.startsWith(bridgePackage)) {
            LOGGER.debug("Reuse {} class from default CL for correct linking", className);
            result = defaultCl.loadClass(className);
        }else{
            LOGGER.debug("Taking {} class from island domain", className);
            result = super.loadClass(className);
        }
        LOGGER.trace("Loaded {} class", className);
        return result;
    }
}
