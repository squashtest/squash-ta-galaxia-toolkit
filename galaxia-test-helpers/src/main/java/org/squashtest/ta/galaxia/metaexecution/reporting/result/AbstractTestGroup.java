/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

/**
 * Base class for all result objects that aggregated several test results.
 * @author edegenetais
 */
public class AbstractTestGroup extends AbstractNamedResultObject {

    protected int totalErrors;
    protected int totalFailures;
    protected int totalNotFound;
    protected int totalNotPassed;
    protected int totalNotRun;
    protected int totalPassed;
    protected int totalSuccess;
    protected int totalTests;
    protected int totalWarning;

    public int getTotalTests() {
        return totalTests;
    }

    public int getTotalErrors() {
        return totalErrors;
    }

    public int getTotalFailures() {
        return totalFailures;
    }

    public int getTotalNotRun() {
        return totalNotRun;
    }

    public int getTotalNotFound() {
        return totalNotFound;
    }

    public int getTotalWarning() {
        return totalWarning;
    }

    public int getTotalSuccess() {
        return totalSuccess;
    }

    public int getTotalPassed() {
        return totalPassed;
    }

    public int getTotalNotPassed() {
        return totalNotPassed;
    }

}
