/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.utils;

import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logging helper to make sure that the cost of file canonical path will only be paid when logging,
 * AND that the logging operation will NOT throw any exception.
 * @author edegenetais
 */
public class FileCanonicalPath {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FileCanonicalPath.class);
    
    private final File file;

    public FileCanonicalPath(String initialPath){
        this(new File(initialPath));
    }
    
    public FileCanonicalPath(File file) {
        this.file = file;
    }

    @Override
    public String toString() {
        try{
            return file.getCanonicalPath();
        }catch(IOException e){
            LOGGER.warn("Failed on IO while trying to log canonical path for file {}",file.getAbsolutePath(),e);
            return file.getAbsolutePath();
        }catch(NullPointerException e){
            LOGGER.debug("Trying to log null file's canonicalpath.",e);
            return "<null>";
        }
    }
    
}
