/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

import java.io.File;
import java.io.IOException;
import org.junit.Ignore;
import org.junit.Test;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.squash.ta.addon.logic.kit.testspecs.SpecSyntaxException;

/**
 * @author edegenetais
 */
public class TestSuiteFilterTest extends ResourceExtractorTestBase{
    
    @Test
    public void selectsByName() throws IOException, SpecSyntaxException{
        TestSuiteFilter testee=new TestSuiteFilter("multi-stage-test/sql-trigger/precond.ta");
        File initialTS=createFile("initial-test-suite.json");
        File expected=createFile("filtered-test-suite-exactname.json");
        File actual=testee.filter(initialTS);
        checkActualContentAgainstExpected(actual, expected);
        actual.delete();
    }
    
    @Test
    public void selectsAllInDirectory() throws SpecSyntaxException, IOException{
        TestSuiteFilter testee=new TestSuiteFilter("multi-stage-test/sql-trigger/*");
        File initialTS=createFile("initial-test-suite.json");
        File expected=createFile("filtered-test-suite-all-in-directory.json");
        File actual=testee.filter(initialTS);
        actual.deleteOnExit();
        checkActualContentAgainstExpected(actual, expected);
    }
    
    @Test
    public void selectsByPartOfName() throws SpecSyntaxException, IOException{
        TestSuiteFilter testee=new TestSuiteFilter("multi-stage-test/partofName/*partOfName.ta");
        File initialTS=createFile("initial-test-suite.json");
        File expected=createFile("filtered-test-suite-partOfName.json");
        File actual=testee.filter(initialTS);
        actual.deleteOnExit();
        checkActualContentAgainstExpected(actual, expected);
    }
    
    @Test
    public void selectsByPartOfNameAntWildcard() throws SpecSyntaxException, IOException{
        TestSuiteFilter testee=new TestSuiteFilter("**/partofName/*partOfName.ta");
        File initialTS=createFile("initial-test-suite.json");
        File expected=createFile("filtered-test-suite-partOfName.json");
        File actual=testee.filter(initialTS);
        actual.deleteOnExit();
        checkActualContentAgainstExpected(actual, expected);
    }
    
    @Ignore //TODO in the end, but other things must come first
    @Test
    public void selectsByPartOfNameAntAnydir() throws SpecSyntaxException, IOException{
        TestSuiteFilter testee=new TestSuiteFilter("**/*partOfName.ta");
        File initialTS=createFile("initial-test-suite.json");
        File expected=createFile("filtered-test-suite-partOfName.json");
        File actual=testee.filter(initialTS);
        actual.deleteOnExit();
        checkActualContentAgainstExpected(actual, expected);
    }
}
