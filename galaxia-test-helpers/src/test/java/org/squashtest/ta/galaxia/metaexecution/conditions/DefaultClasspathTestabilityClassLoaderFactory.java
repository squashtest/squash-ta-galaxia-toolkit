/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.conditions;

import java.io.File;
import org.squashtest.ta.galaxia.metaexecution.enginelink.ClassLoaderFactory;

/*
 * This class exists for testability :
 * the tests are run with a default classloader,
 * while real executions uses the classloading island to run with the test project classpath.
 */
class DefaultClasspathTestabilityClassLoaderFactory implements ClassLoaderFactory {

    @Override
    public ClassLoader getTestProjectClassLoader(File projectRootDir) {
        return getClass().getClassLoader();
    }

}
