/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.dsltools;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacro;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroFixedPart;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroParam;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroParamDefinition;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignature;

/**
 * Testing the MacroValidator impl, whatever it is, to make sure it honors its contract.
 * @author edegenetais
 */
public class MacroValidatorTest {
    
    MacroDslToolsFactory macroDslToolsFactory = new DslTools().getMacroDslToolsFactory();
    
    private SquashDSLMacro createMoreSpecificMacro(SquashDSLMacro squashDSLMacro) {
        final ArrayList<SquashDSLMacroSignature> moreSpecificSignature = new ArrayList<>(squashDSLMacro.getSignatures());
        moreSpecificSignature.add(new SquashDSLMacroFixedPart(" USING "));
        moreSpecificSignature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("configuration")));
        final SquashDSLMacro moreParms = new SquashDSLMacro("moreParms", moreSpecificSignature);
        return moreParms;
    }

    private SquashDSLMacro createBaseMacro() {
        final ArrayList<SquashDSLMacroSignature> signature1 = new ArrayList<>();
        signature1.add(new SquashDSLMacroFixedPart("ASSERT XML ROCKS "));
        signature1.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("data")));
        List<SquashDSLMacroSignature> signature = signature1;
        final SquashDSLMacro squashDSLMacro = new SquashDSLMacro("generic", signature);
        return squashDSLMacro;
    }
    
    @Test
    public void moreSpecificShouldHaveHigherSpecificity(){
        
        SquashDSLMacro squashDSLMacro = createBaseMacro();
        SquashDSLMacro moreParms = createMoreSpecificMacro(squashDSLMacro);
        
        MacroValidator genericValidator=macroDslToolsFactory.getValidator(squashDSLMacro);
        MacroValidator moreParmsValidator=macroDslToolsFactory.getValidator(moreParms);
        
        Assert.assertTrue("The validator fore a macro with one fixed part and one parameter more should be scored as more specific", moreParmsValidator.getMatchSpecificity()>genericValidator.getMatchSpecificity());
    }
    
    @Test
    public void shouldOrderByDecreasingSpecificity(){
        final ArrayList<SquashDSLMacro> inputInOrder = new ArrayList<>();
        inputInOrder.add(createMoreSpecificMacro(createBaseMacro()));
        inputInOrder.add(createBaseMacro());
        List<MacroValidator> validatorsFromInOrder=macroDslToolsFactory.getValidators(inputInOrder);
        
        Assert.assertTrue("Should order in decreasing levels of specificity when input is in order", decreasingSpecificity(validatorsFromInOrder));
        
        final ArrayList<SquashDSLMacro> inputInReverseOrder = new ArrayList<>();
        inputInReverseOrder.add(createBaseMacro());
        inputInReverseOrder.add(createMoreSpecificMacro(createBaseMacro()));
        List<MacroValidator> validatorsFromReverseOrder=macroDslToolsFactory.getValidators(inputInReverseOrder);
        
        Assert.assertTrue("Should order in decreasing levels of specificity when input is in order", decreasingSpecificity(validatorsFromReverseOrder));
        
    }
    
    @Test
    public void testNormalMatchingBehaviorForSpaceTerminatedFixedPart(){
        SquashDSLMacro simpleMacro=createBaseMacro();
        MacroValidator testee=macroDslToolsFactory.getValidator(simpleMacro);
        final String positiveMatch = "ASSERT XML ROCKS n'rolls";
        Assert.assertTrue(testee.originalMacroDefinition().toString()+" should match"+positiveMatch, testee.isValidInstanceOfMe(positiveMatch));
    }
    
     @Test
    public void testNormalNonMatchingBehaviorForSpaceTerminatedFixedPart(){
        SquashDSLMacro simpleMacro=createBaseMacro();
        MacroValidator testee=macroDslToolsFactory.getValidator(simpleMacro);
        final String negativeMatch = "ASSERT XML GROOVES n'cooes";
        Assert.assertFalse(testee.originalMacroDefinition().toString()+" should NOT match "+negativeMatch, testee.isValidInstanceOfMe(negativeMatch));
    }
    
    //THIS IS $({conernercase}) MACRO OF {custom} - no match
    @Test
    public void testNormalNonMatchingBehaviorForNonSpaceTerminatedFixedPart(){
        final List<SquashDSLMacroSignature> signature = new ArrayList<>();
        signature.add(new SquashDSLMacroFixedPart("THIS IS $("));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("cornercase")));
        signature.add(new SquashDSLMacroFixedPart(") MACRO OF"));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("custom")));
        
        SquashDSLMacro evilMacro=new SquashDSLMacro("cornercase", signature);
        final String nonMatch = "THIS IS LA PESTE MACRO OF Tartempion!";
        Assert.assertFalse(evilMacro+" should NOT match "+nonMatch,macroDslToolsFactory.getValidator(evilMacro).isValidInstanceOfMe(nonMatch));
    }
    
    //THIS IS $({conernercase}) MACRO OF {custom} - match
    @Test
    public void testNormalMatchingBehaviorForNonSpaceTerminatedFixedPart(){
        final List<SquashDSLMacroSignature> signature = new ArrayList<>();
        signature.add(new SquashDSLMacroFixedPart("THIS IS $("));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("cornercase")));
        signature.add(new SquashDSLMacroFixedPart(") MACRO OF"));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("custom")));
        
        SquashDSLMacro evilMacro=new SquashDSLMacro("cornercase", signature);
        final String match = "THIS IS $(LA PESTE) MACRO OF Tartempion!";
        Assert.assertTrue(evilMacro+" should match "+match,macroDslToolsFactory.getValidator(evilMacro).isValidInstanceOfMe(match));
    }
    
    private boolean decreasingSpecificity(List<MacroValidator> validatorsFromInOrder) {
        return validatorsFromInOrder.get(0).getMatchSpecificity()>
                validatorsFromInOrder.get(1).getMatchSpecificity();
    }
    
    //the devil lies in the case...
    //KeY 1 {param1} KEY 2 {param2} KEY_3
    @Test
    public void testMacroWithMixedCaseSignature(){
        final List<SquashDSLMacroSignature> signature = new ArrayList<>();
        signature.add(new SquashDSLMacroFixedPart("KeY 1 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param1")));
        signature.add(new SquashDSLMacroFixedPart(" KEY 2 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param2")));
        signature.add(new SquashDSLMacroFixedPart(" KEY_3"));
        
        SquashDSLMacro evilMacro=new SquashDSLMacro("cornercase", signature);
        final String match = "KeY 1 {param1} KEY 2 {param2} KEY_3";
        Assert.assertTrue(evilMacro+" should match "+match,macroDslToolsFactory.getValidator(evilMacro).isValidInstanceOfMe(match));
    }
    
    @Test
    public void testMacroWithMixedCaseSignatureOnLowerCaseUse(){
        final List<SquashDSLMacroSignature> signature = new ArrayList<>();
        signature.add(new SquashDSLMacroFixedPart("KeY 1 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param1")));
        signature.add(new SquashDSLMacroFixedPart(" KEY 2 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param2")));
        signature.add(new SquashDSLMacroFixedPart(" KEY_3"));
        
        SquashDSLMacro evilMacro=new SquashDSLMacro("cornercase", signature);
        final String match = "key 1 {param1} key 2 {param2} key_3";
        Assert.assertTrue(evilMacro+" should match "+match,macroDslToolsFactory.getValidator(evilMacro).isValidInstanceOfMe(match));
    }
    
    @Test
    public void testMacroWithMixedCaseSignatureOnUpperCaseUse(){
        final List<SquashDSLMacroSignature> signature = new ArrayList<>();
        signature.add(new SquashDSLMacroFixedPart("KeY 1 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param1")));
        signature.add(new SquashDSLMacroFixedPart(" KEY 2 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param2")));
        signature.add(new SquashDSLMacroFixedPart(" KEY_3"));
        
        SquashDSLMacro evilMacro=new SquashDSLMacro("cornercase", signature);
        final String match = "KEY 1 {param1} KEY 2 {param2} KEY_3";
        Assert.assertTrue(evilMacro+" should match "+match,macroDslToolsFactory.getValidator(evilMacro).isValidInstanceOfMe(match));
    }
    
    @Test
    public void testMacroWithMixedCaseSignatureOnInvertedCaseUse(){
        final List<SquashDSLMacroSignature> signature = new ArrayList<>();
        signature.add(new SquashDSLMacroFixedPart("KeY 1 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param1")));
        signature.add(new SquashDSLMacroFixedPart(" KEY 2 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param2")));
        signature.add(new SquashDSLMacroFixedPart(" KEY_3"));
        
        SquashDSLMacro evilMacro=new SquashDSLMacro("cornercase", signature);
        final String match = "kEy 1 {param1} key 2 {param2} key_3";
        Assert.assertTrue(evilMacro+" should match "+match,macroDslToolsFactory.getValidator(evilMacro).isValidInstanceOfMe(match));
    }
    
    @Test
    public void testMacroWithMultispaceSignature(){
        final List<SquashDSLMacroSignature> signature = new ArrayList<>();
        signature.add(new SquashDSLMacroFixedPart("  KeY 1 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param1")));
        signature.add(new SquashDSLMacroFixedPart(" KEY 2 "));
        signature.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("param2")));
        signature.add(new SquashDSLMacroFixedPart(" KEY_3"));
        
        SquashDSLMacro evilMacro=new SquashDSLMacro("cornercase", signature);
        final String match = " kEy 1 {param1} key 2 {param2} key_3";
        Assert.assertTrue(evilMacro+" should match "+match,macroDslToolsFactory.getValidator(evilMacro).isValidInstanceOfMe(match));
    }
}
