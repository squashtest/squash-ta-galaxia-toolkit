/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result.codec;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.test.result.EcosystemResult;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.PhaseResult;
import org.squashtest.ta.framework.test.result.ResourceAndContext;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.codec.TestResultCodec;

/**
 *
 * @author edegenetais
 */
public class TestResultCodecTest extends ResourceExtractorTestBase{
    private TestResultCodec testee=new TestResultCodec();
    private File jsonDir;
    private File json;
    private boolean hasInstructionType=false;
    
    @Before
    public void setSerializedSuiteUp() throws IOException{
        jsonDir=createNtrackDir();
        json=new File(jsonDir,"EXECUTION.json");
        json.getParentFile().mkdirs();
        extractToFile(json, "EXECUTION-unmarshal.json");
    }
    
    @Test
    public void shouldDeserializeNoError() throws IOException{
        
        new File(jsonDir,"attachments").mkdir();
        
        extractToFile(new File(jsonDir,"attachments/attach1.ser"), "attach1.ser");
        extractToFile(new File(jsonDir,"attachments/attach2.ser"), "attach2.ser");
        extractToFile(new File(jsonDir,"attachments/attach3.ser"), "attach3.ser");
        
        Map<String,File> foundFiles=loadAndAnalyze();
        
        checkFoundAndCorrect("diff1", foundFiles, "attach1.ser");
        checkFoundAndCorrect("diff2", foundFiles, "attach2.ser");
        checkFoundAndCorrect("diff3", foundFiles, "attach3.ser");
    }

    private Map<String, File> loadAndAnalyze() throws IOException {
        final Map<String,File> foundFiles=new HashMap<>(3);
        SuiteResult suite=testee.load(json);
        for(EcosystemResult eco:suite.getSubpartResults()){
            for(TestResult tr:eco.getSubpartResults()){
                
                if(tr.getFirstFailure()!=null){
                    getAllFileResources(tr.getFirstFailure(), foundFiles);
                }
                    
                checkPhase(tr.getSetupPhaseResult(), foundFiles);
                
                checkPhase(tr.getTestPhaseResult(), foundFiles);
                
                checkPhase(tr.getTeardownPhaseResult(), foundFiles);
            }
        }
        return foundFiles;
    }

    @Test
    public void loadsInstructionTypes() throws IOException{
        loadAndAnalyze();
        Assert.assertTrue("No instructionType was found", hasInstructionType);
    }
    
    private void checkPhase(final PhaseResult phaseResult, final Map<String, File> foundFiles) {
        if(phaseResult!=null){
            getAllFileResources(phaseResult.getAllInstructions(), foundFiles);
            getAllFileResources(phaseResult.getFailedInstructions(), foundFiles);
        }
    }

    private void checkFoundAndCorrect(final String rscName, final Map<String, File> foundFiles, final String contentTestResource) throws IOException {
        Assert.assertNotNull(rscName+" not found",foundFiles.get(rscName));
        checkActualContentAgainstExpected(foundFiles.get(rscName), contentTestResource);
    }

    private void getAllFileResources(Collection<ExecutionDetails> detailCollection, final Map<String, File> foundFilesByTAResourceName) {
        if(detailCollection==null){
            detailCollection=Collections.emptyList();
        }
        for(ExecutionDetails ed:detailCollection){
            getAllFileResources(ed, foundFilesByTAResourceName);
        }
    }

    private void getAllFileResources(ExecutionDetails ed, final Map<String, File> foundFilesByTAResourceName) {
        if(ed.instructionType()!=null){
            hasInstructionType=true;
        }
        if(ed.getChildrens()!=null){
            getAllFileResources(ed.getChildrens(), foundFilesByTAResourceName);
        }
        Collection<ResourceAndContext> resourcesAndContext = ed.getResourcesAndContext();
        if(resourcesAndContext==null){
            resourcesAndContext=Collections.emptyList();
        }
        for(ResourceAndContext rac1:resourcesAndContext){
            if("file".equals(rac1.getMetadata().getResourceType())){
                foundFilesByTAResourceName.put(rac1.getMetadata().getName().getName(), ((FileResource)rac1.getResource()).getFile());
            }
        }
    }
    
}
