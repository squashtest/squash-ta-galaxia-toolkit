/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.conditions;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import org.hsqldb.jdbc.JDBCDriver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;

/**
 *
 * @author edegenetais
 */
public class SQLConditionTest extends ResourceExtractorTestBase{
    
    private File testProject;
    
    @BeforeClass
    public static void deactivateClassloadingIsland(){
        SQLCondition.FACTORY=new DefaultClasspathTestabilityClassLoaderFactory();
    }
    
    @BeforeClass
    public static final void passDDL() throws SQLException{
        JDBCDriver driver=new JDBCDriver();
        Properties connProp=new Properties();
        connProp.setProperty("user", "petit-anonymous");
        connProp.setProperty("password", "");
        
        try(
                Connection ddlConn=driver.connect("jdbc:hsqldb:mem:guinea-pig",connProp);
                final Statement statement = ddlConn.createStatement();
            ){
                final int ddlResult = statement.executeUpdate("create table DUMMY(ID INTEGER PRIMARY KEY,YESNO VARCHAR(1))");
                LoggerFactory.getLogger(SQLConditionTest.class).info("DDL query passed with {} as a result.", ddlResult);
                statement.executeUpdate("INSERT INTO DUMMY(ID,YESNO) VALUES(1,'O')");
                statement.executeUpdate("INSERT INTO DUMMY(ID,YESNO) VALUES(2,'N')");
                ddlConn.commit();
        }
        
    }
    
    @Before
    public void deployDummyProject() throws IOException{
        testProject=createNtrackDir();
        File targetsDir=new File(testProject,"src/squashTA/targets");
        targetsDir.mkdirs();
        extractToFile(new File(targetsDir,"hsql-local.properties"), "hsql-local.properties");
        
        File galaxiaDir=new File(testProject,"target/galaxia");
        galaxiaDir.mkdirs();
        extractToFile(new File(galaxiaDir,".classpath"), "dummy.cp");
    }
    
    @Test
    public void constructs4Project() throws InvalidConditionSetupException{
        new SQLCondition(testProject.getAbsolutePath(), "hsql-local", "", "O");
    }
    
    @Test
    public void conditionIsTrueIfQueryExtractsExpectedSingleColumnValue() throws ConditionException{
        final SQLCondition testee = new SQLCondition(testProject.getAbsolutePath(), "hsql-local", "select YESNO from DUMMY WHERE ID=1", "O");
        Assert.assertTrue("Condition should be true", testee.check());
    }
    
    @Test
    public void conditionIsFalseIfQueryExtractsDifferentSingleColumnValue() throws ConditionException{
        final SQLCondition testee = new SQLCondition(testProject.getAbsolutePath(), "hsql-local", "select YESNO from DUMMY WHERE ID=1", "N");
        Assert.assertFalse("Condition should be true", testee.check());
    }
    
    @Test
    public void conditionIsFalseIfQueryFindsNoRosw() throws ConditionException{
        final SQLCondition testee = new SQLCondition(testProject.getAbsolutePath(), "hsql-local", "select YESNO from DUMMY WHERE ID=3", "N");
        Assert.assertFalse("Condition should be true", testee.check());
    }
}
