/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.conditions;

import java.lang.reflect.InvocationTargetException;
import org.junit.Assert;
import org.junit.Test;

/**
 * I seldom get to test <strong>Exception classes</strong>, but I want to remove the {@link InvocationTargetException},
 * which we don't care about and is only a vehicle for the true exception when a method is called by reflection.
 * 
 * @author edegenetais
 */
public class ConditionEvaluationFailureTest {
    @Test
    public void extractTarget(){
        InvocationTargetException ite=new InvocationTargetException(new IllegalAccessException());
        ConditionEvaluationFailure testee=new ConditionEvaluationFailure("I don' care", ite);
        Assert.assertSame(testee.getCause(), ite.getTargetException());
    }
}
