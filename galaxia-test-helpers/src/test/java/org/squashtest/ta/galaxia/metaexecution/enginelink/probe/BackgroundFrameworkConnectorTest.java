/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink.probe;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import org.junit.Assert;
import org.junit.Test;
import org.squashtest.ta.galaxia.metaexecution.enginelink.FrameworkConnector;

/**
 * This test checks that the {@link BackgroundFrameworkConnector} interface covers all
 * public methods of the {@link SimpleFrameworkConnector} class, so that the
 * background connector supplied by the
 * {@link BackgroundFrameworkConnectorFactory} class will be able to offer them.
 *
 * @author edegenetais
 */
public class BackgroundFrameworkConnectorTest {

    private SortedSet<Method> sortedConnectorServices = getSortedServices(FrameworkConnector.class);
    private SortedSet<Method> sortedInterfaceServices = getSortedServices(BackgroundFrameworkConnector.class);
    
    @Test
    public void theFrameworkConnectorInterfaceShouldCoverAllFrameWorkConnectorPublicMethods() {
        
        StringBuilder messages=new StringBuilder();
        
        int nbMissing=0;    
        for(Method service:sortedConnectorServices){
            Method match=null;
            Iterator<Method> interfaceServiceIt=sortedInterfaceServices.iterator();
            while(match==null && interfaceServiceIt.hasNext()){
                Method candidate=interfaceServiceIt.next();
                if(new MethodSorter().compare(service, candidate)==0){
                    match=candidate;
                }
            }
            if(match==null){
                messages.append("Uncovered method ").append(service.toString()).append("\n");
                nbMissing++;
            }else{
                sortedInterfaceServices.remove(match);
            }
        }
        
        if(nbMissing>0){
            messages.insert(0,"Missing "+nbMissing+" methods in interface to cover connector services.\n");
            Assert.fail(messages.toString());
        }
        
    }
    
    @Test
    public void thereShouldBeNoMethodFromInterfaceWithoutMatchingImplementation(){
        int nbMissing=0;    
         StringBuilder messages=new StringBuilder();
        for(Method service:sortedInterfaceServices){
            Method match=null;
            Iterator<Method> interfaceServiceIt=sortedConnectorServices.iterator();
            while(match==null && interfaceServiceIt.hasNext()){
                Method candidate=interfaceServiceIt.next();
                if(new MethodSorter().compare(service, candidate)==0){
                    match=candidate;
                }
            }
            if(match==null){
                messages.append("Unimplemented method ").append(service.toString()).append("\n");
                nbMissing++;
            }else{
                sortedInterfaceServices.remove(match);
            }
        }
        
        if(nbMissing>0){
            messages.insert(0,nbMissing+" methods in interface have no impmlementation by FrameworkConnector services.\n");
            Assert.fail(messages.toString());
        }
    }
    

    private SortedSet<Method> getSortedServices(Class<?> targetClass) throws SecurityException {
        Method[] services=targetClass.getMethods();
        SortedSet<Method> sortedServices=new TreeSet<Method>(new MethodSorter());
        for(Method m:services){
            if(!Object.class.equals(m.getDeclaringClass()) && Modifier.isPublic(m.getModifiers())){
                sortedServices.add(m);
            }
        }
        return sortedServices;
    }

    static class MethodSorter implements Comparator<Method> {

        public MethodSorter() {
        }

        @Override
        public int compare(Method t, Method t1) {
            if(t.getName().equals(t1.getName())){
                if(t.getParameterCount()==t1.getParameterCount()){
                    final Class<?>[] parameterTypes = t.getParameterTypes();
                    final Class<?>[] parameterTypes1 = t1.getParameterTypes();
                    for(int i=0;i<parameterTypes.length;i++){
                        Class<?> parmType=parameterTypes[i];
                        Class<?> parmType1=parameterTypes1[i];
                        if(!parmType.equals(parmType1)){
                            return parmType.getName().compareTo(parmType1.getName());
                        }
                    }
                    return 0;
                }else{
                    return Integer.compare(t.getParameterCount(), t1.getParameterCount());
                }
            }else{
                return t.getName().compareTo(t1.getName());
            }
        }
    }

}
