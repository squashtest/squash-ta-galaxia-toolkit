/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting.result;

import java.lang.reflect.Field;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author edegenetais
 */
public class EcosystemResultImplTest {
    private EcosystemResultImpl testee;
    
    @Before
    public void setup(){
        testee = new EcosystemResultImpl();
        testee.name="caVaRuerDansLesBrancards";
    }
    
    @Test
    public void behaveNormallyWithoutOrdinal(){
        Assert.assertEquals("caVaRuerDansLesBrancards",testee.getName());
        Assert.assertEquals("caVaRuerDansLesBrancards",testee.getName());
    }
    
    @Test
    public void behaveNromallyWithOrdinalSetNull(){
        testee.setOrdinal(null);
        Assert.assertEquals("caVaRuerDansLesBrancards",testee.getName());
        Assert.assertEquals("caVaRuerDansLesBrancards",testee.getName());
    }
    
    @Test
    public void addOrdinalPartIfOrdinalSet(){
        testee.setOrdinal(2);
        Assert.assertEquals("caVaRuerDansLesBrancards[2]",testee.getName());
        Assert.assertEquals("caVaRuerDansLesBrancards[2]",testee.getName());
    }
    
    @Test
    public void addOrdinalPartIfOrdinalInjected() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        final Field ordinalField = testee.getClass().getDeclaredField("ordinal");
        ordinalField.setAccessible(true);
        ordinalField.set(testee, 2);
        ordinalField.setAccessible(false);
        
        Assert.assertEquals("caVaRuerDansLesBrancards[2]",testee.getName());
        Assert.assertEquals("caVaRuerDansLesBrancards[2]",testee.getName());
    }
}
