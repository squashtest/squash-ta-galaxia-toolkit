/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.reporting;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.exception.AssertionFailedException;
import org.squashtest.ta.framework.test.result.ExecutionDetails;
import org.squashtest.ta.framework.test.result.GeneralStatus;
import org.squashtest.ta.framework.test.result.InstructionType;
import org.squashtest.ta.framework.test.result.TargetInitialisationResult;
import org.squashtest.ta.framework.test.result.TestResult;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.EcosystemResultImpl;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.ExecutionDetailImpl;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.SuiteResultImpl;
import org.squashtest.ta.galaxia.metaexecution.reporting.result.TestResultImpl;

/**
 *
 * @author edegenetais
 */
public class ReportAggregatorTest extends ResourceExtractorTestBase{
    
    private ReportAggregator testee;
    private File jsonDir;
    private SuiteResultImplTest currentResult;
    
    
    @Before
    public void setTesteeUp() throws IOException {
        jsonDir = createNtrackDir();
        testee = new ReportAggregator();
        testee.setExporters(Collections.EMPTY_LIST);
        testee.setReportType(ReportType.EXECUTION);
        testee.setOutputDirectoryName(jsonDir.getName());
        testee.setResultStorage(".");
    }

    @Before
    public void setCurrentResultUp() {
        currentResult = new SuiteResultImplTest();
        currentResult.setProjectGroupId("icdc.test");
        currentResult.setProjectArtifactId("candidate-integration-tests");
        currentResult.setProjectVersion("1.0-SNAPSHOT");
        currentResult.setName("tests");
    }    

    private File getFileOut(String resourceName) throws IOException {
        File jsonFile=new File(jsonDir, resourceName);
        extractToFile(jsonFile, resourceName);
        return jsonFile;
    }
    
    @Test
    public void shouldDeserializeAsExpected() throws IOException{
       
        File jsonFile=getFileOut("EXECUTION.json");
    
        testee.write(jsonDir, currentResult);
        
        File expected=createFile("expectedAggregate.json");
        
        checkActualContentAgainstExpected(jsonFile, expected);
    }

    @Test 
    public void shouldMergeValueEqualTargetInitializationResults() throws IOException{
        File jsonFile=getFileOut("EXECUTION.json");
        
        currentResult.getTargetInitialisationResults().add(new TargetInitialisationResult("mysql", "database", TargetInitialisationResult.TargetStatus.OK));
        
        testee.write(jsonDir, currentResult);
        
        File expected=createFile("expectedAggregate.json");
        
        checkActualContentAgainstExpected(jsonFile, expected);
    }
    
    @Test
    public void shouldKeepValueDistinctTargetInitializationResults() throws IOException{
        File jsonFile=getFileOut("EXECUTION.json");
        
        currentResult.getTargetInitialisationResults().add(new TargetInitialisationResult("mysql", "database", TargetInitialisationResult.TargetStatus.ERROR));
        
        testee.write(jsonDir, currentResult);
        
        File expected=createFile("expectedAggregateWithMysqlFailure.json");
        
        checkActualContentAgainstExpected(jsonFile, expected);
    }

    @Test
    public void shouldKeepBothSameNameEcosystemsWithOrdinalMark() throws IOException{
        /*
        * because these ecosystems ARE setup and teared down twice, and there is no added value to merging them in the report
        * however, we must make their names different, without loosing track of the fact that they have the same name
        */
        File jsonFile=getFileOut("EXECUTION.json");
        
        final EcosystemResultImplTest ecosystemResultImpl = new EcosystemResultImplTest();
        ecosystemResultImpl.setName("tests.multi-stage-test.sql-trigger");
        currentResult.addEcosystem(ecosystemResultImpl);

        final TestResultImpl daTest=new TestResultImpl();
        daTest.setName("yetAnotherTest.ta");
        ecosystemResultImpl.addTest(daTest);

        testee.write(jsonDir, currentResult);

        File expected=createFile("expectedAggregateWithMoreTests.json");

        checkActualContentAgainstExpected(jsonFile, expected);
        
    }
    
    @Test
    public void shouldSerializeFailedTestNoProblem() throws IOException{
        System.setProperty("org.squashtest.ta.galaxia.metaexecution.reporting.result.codec.always.same.file", "true");
        Properties testParms=new Properties();
        try(InputStream parmStream=getClass().getResourceAsStream("testparm.properties")){
            testParms.load(parmStream);
        }
        String targetPath=testParms.getProperty("baseDirForFiles");
        final File storage = new File(targetPath);
        testee.setOutputDirectoryName("target");
        
        File attachment=createFile("attachment.txt");
        
        File jsonFile=new File(storage,"EXECUTION.json");
        extractToFile(jsonFile, "EXECUTION.json");
        
        final EcosystemResultImplTest ecosystemResultImpl = new EcosystemResultImplTest();
        ecosystemResultImpl.setName("tests.multi-stage-test.postcondition");
        currentResult.addEcosystem(ecosystemResultImpl);

        final TestResultImplTest daTest=new TestResultImplTest();
        daTest.setName("yetAnotherTest.ta");
        final ExecutionDetailImplTest failureDetailsImpl = new ExecutionDetailImplTest();
        
        final AssertionFailedException assertionFailedException = new AssertionFailedException("Assertion failed !!!", new FileResource(attachment), Collections.EMPTY_LIST);
        assertionFailedException.setStackTrace(new StackTraceElement[]{
            new StackTraceElement("DummyClass4SerializationValidation", "dummyMethod4SerilizationValidation", "VirtualFile1", 12),
            new StackTraceElement("DummyClass4SerializationValidation2", "dummyMethod4SerilizationValidation2", "VirtualFile2", 154)
        });
        failureDetailsImpl.setCaughtException(assertionFailedException);
        failureDetailsImpl.setInstructionAsText("Gotcha!");
        failureDetailsImpl.setInstructionType(InstructionType.ASSERTION);
        daTest.setFirstFailure(failureDetailsImpl);
        daTest.setStatus(GeneralStatus.FAIL);
        ecosystemResultImpl.addTest(daTest);
        
        testee.write(storage, currentResult);

        File expected=createFile("expectedAggregateWithFailedTests.json");

        checkActualContentAgainstExpected(jsonFile, expected);
    }
   
    /**
     * Test class to help build results.
     */
    private static class SuiteResultImplTest extends SuiteResultImpl {
        public void addEcosystem(EcosystemResultImpl eco){((List)subpartResults).add(eco);}
    }

    /**
     * Test class to help build results.
     */
    private static class EcosystemResultImplTest extends EcosystemResultImpl {
        public void addTest(TestResult test){((List)subpartResults).add(test);}
    }

    
    /**
     * Test class to help build results.
     */
    private static class TestResultImplTest extends TestResultImpl {
        public void setFirstFailure(ExecutionDetails det){
            this.firstFailure=det;
        }
    }

    /**
     * Test class to help build results.
     */
    private class ExecutionDetailImplTest extends ExecutionDetailImpl {

        public void setCaughtException(Exception e){
            this.caughtException=e;
        }
        
        public void setInstructionAsText(String text){
            this.instructionAsText=text;
        }
        
        public void setInstructionType(InstructionType type){
            this.instructionType=type;
        }
    }
    
    @After
    public void removeFileResourceSerializationTestMode(){
        System.getProperties().remove("org.squashtest.ta.galaxia.metaexecution.reporting.result.codec.always.same.file");
    }
}
