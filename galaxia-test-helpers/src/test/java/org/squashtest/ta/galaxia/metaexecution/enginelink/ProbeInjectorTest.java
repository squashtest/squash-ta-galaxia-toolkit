/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

import org.squashtest.ta.galaxia.metaexecution.enginelink.probe.ProbeInjector;
import java.io.File;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.xml.functions.library.Transform;
import org.squashtest.ta.galaxia.utils.FileCanonicalPath;

/**
 *
 * @author edegenetais
 */
public class ProbeInjectorTest extends ResourceExtractorTestBase{
        private final Transform normalizer = new Transform(null, true);
        private ProbeInjector testee;
        
        @Before
        public void setupTestee() throws EngineLinkException{
            testee=new ProbeInjector();
        }
        
        private File deployExpected(String rscName) throws IOException{
            File expectedFile=createNtrackFile();
            normalizer.transform(createFile(rscName), expectedFile);
            return expectedFile;
        }
        
        @Test
        public void pomShouldReceiveGalaxiaInstrumentation() throws EngineLinkException, IOException{
            File injectedPom=testee.inject(createFile("initialPom.xml"));
            File expectedFile=deployExpected("expectedInjectedAnalyzedPom.xml");
            File normalizedResult=createNtrackFile();
            normalizer.transform(injectedPom, normalizedResult);
            LoggerFactory.getLogger(ProbeInjectorTest.class).debug("Expected {} and got {}.",new FileCanonicalPath(expectedFile),new FileCanonicalPath(normalizedResult));
            checkActualContentAgainstExpected(normalizedResult, expectedFile);
        }
        
        @Test
        public void shouldNotNeedDependenciesElementToInjectProbe() throws IOException, EngineLinkException{
            File injectedPom=testee.inject(createFile("initialPom_nodeps.xml"));
            File expectedFile=deployExpected("expectedInjectedAnalyzedPom_nodeps.xml");
            File normalizedResult=createNtrackFile();
            normalizer.transform(injectedPom, normalizedResult);
            LoggerFactory.getLogger(ProbeInjectorTest.class).debug("Expected {} and got {}.",new FileCanonicalPath(expectedFile),new FileCanonicalPath(normalizedResult));
            checkActualContentAgainstExpected(normalizedResult, expectedFile);
        }
        
        @Test
        public void shouldWorkWithConfigurers() throws IOException, EngineLinkException{
            File injectedPom=testee.inject(createFile("initialPom_configurers.xml"));
            File expectedFile=deployExpected("expectedInjectedAnalyzedPom_configurers.xml");
            File normalizedResult=createNtrackFile();
            normalizer.transform(injectedPom, normalizedResult);
            LoggerFactory.getLogger(ProbeInjectorTest.class).debug("Expected {} and got {}.",new FileCanonicalPath(expectedFile),new FileCanonicalPath(normalizedResult));
            checkActualContentAgainstExpected(normalizedResult, expectedFile);
        }
        
           @Test
        public void shouldWorkWithConfigurersSpecifiedAsImplementationAttribute() throws IOException, EngineLinkException{
            File injectedPom=testee.inject(createFile("initialPom_configurers_alternate.xml"));
            File expectedFile=deployExpected("expectedInjectedAnalyzedPom_configurers_alternate.xml");
            File normalizedResult=createNtrackFile();
            normalizer.transform(injectedPom, normalizedResult);
            LoggerFactory.getLogger(ProbeInjectorTest.class).debug("Expected {} and got {}.",new FileCanonicalPath(expectedFile),new FileCanonicalPath(normalizedResult));
            checkActualContentAgainstExpected(normalizedResult, expectedFile);
        }
}
