/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.metaexecution.enginelink;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.squashtest.ta.framework.components.TargetCreator;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;
import org.squashtest.ta.framework.components.Target;

@RunWith(MockitoJUnitRunner.class)
public class TargetFinderTest {
    TargetFinder testee;
    @Mock
    TestWorkspaceBrowser wsBrowser;
    @Mock
    TargetCreator creator;
    
    @Before
    public void createSlavedTestee(){
        testee=new TargetFinder(wsBrowser);
    }
    
    @Test
    public void findsTargetIfExistsAndEligible(){
        Mockito.when(wsBrowser.getTargetsDefinitions()).thenReturn(getURLlist("hsql-local.properties"));
        Mockito.when(creator.canInstantiate(Mockito.any(URL.class))).thenReturn(true);
        
        Mockito.when(creator.createTarget(Mockito.any(URL.class))).thenReturn(Mockito.mock(Target.class));
        Assert.assertNotNull(testee.findTarget("hsql-local", creator));
    }

    public List<URL> getURLlist(String... resourceNames) {
        ArrayList<URL> urlList=new ArrayList<>(resourceNames.length);
        for(String resourceName:resourceNames){
            urlList.add(getClass().getResource(resourceName));
        }
        return urlList;
    }
}
