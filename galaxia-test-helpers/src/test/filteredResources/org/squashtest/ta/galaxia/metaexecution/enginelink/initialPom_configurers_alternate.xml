<!--

        This file is part of the Squashtest platform.
        Copyright (C) 2011 - 2020 Henix

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses />.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.squashtest.ta</groupId>
        <artifactId>squash-ta-test</artifactId>
        <version>1.11.0-SNAPSHOT</version>
    </parent>
        
    <artifactId>squash-ta-self-contained-integration-tests</artifactId>
	
    <packaging>jar</packaging>
    <name>Self-continedIntegrationTests</name>
    <description>This project defines a minimal set of integration tests of the Squash TA engine that need no particular external tools</description>
    <!-- Properties definition -->
    <properties>
        <!-- Log configuration file -->
        <logConfFile>src/log4j.properties</logConfFile>
        <ta.temp.directory>${user.home}/Squash_TA_temp</ta.temp.directory>
        <ta.debug.mode>true</ta.debug.mode>
        <project.sourceEncoding>utf-8</project.sourceEncoding>
    </properties>

    <build>

        <plugins>
            <!-- Configuration of the Squash TA framework used by the project -->
            <plugin>
                <groupId>org.squashtest.ta</groupId>
                <artifactId>squash-ta-maven-plugin</artifactId>
                <version>1.10.1-RELEASE</version>

                <dependencies>

                    <!-- Dependency for hsqldb database-->
                    <dependency> 
                        <groupId>org.hsqldb</groupId>
                        <artifactId>hsqldb</artifactId>
                        <version>2.4.0</version>
                    </dependency>
                </dependencies>

                <!-- Under here is the Squash TA framework default configuration -->
                <configuration>

                    <!-- Uncomment the line below in order to the build finish in success 
                    even if a test failed -->
                    <!-- <alwaysSuccess>true</alwaysSuccess> -->

                    <!-- Define a log configuration file (at log4j format) to override the 
                    one defined internally -->
                    <!-- If the given file can't be found the engine switch to the internal 
                    configuration -->
                    <logConfiguration>${logConfFile}</logConfiguration>

                    <!-- Define exporters -->
                    <exporters>
                        <surefire>
                            <jenkinsAttachmentMode>${ta.jenkins.attachment.mode}</jenkinsAttachmentMode>
                        </surefire>
                        <html />
                    </exporters>

                    <!-- Define configurers -->
                    <configurers>
                        <configurer implementation="org.squashtest.ta.link.SquashTMCallbackEventConfigurer">
                            <endpointURL>${status.update.events.url}</endpointURL>
                            <executionExternalId>${squash.ta.external.id}</executionExternalId>
                            <jobName>${jobname}</jobName>
                            <hostName>${hostname}</hostName>
                            <endpointLoginConfFile>${squash.ta.conf.file}</endpointLoginConfFile>
                            <reportBaseUrl>${ta.tmcallback.reportbaseurl}</reportBaseUrl>
                            <jobExecutionId>${ta.tmcallback.jobexecutionid}</jobExecutionId>
                            <reportName>${ta.tmcallback.reportname}</reportName>
                        </configurer>
                    </configurers>
                </configuration>

                <!-- Bind the Squash TA "run" goal to the maven integration-test phase 
                and reuse the default configuration -->
                <executions>
                    <execution>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <!-- Squash TA maven repository -->
    <repositories>
        <repository>
            <id>org.squashtest.ta.release</id>
            <name>squashtest test automation - releases</name>
            <url>http://repo.squashtest.org/maven2/releases</url>
        </repository>
    </repositories>

    <!-- Squash TA maven plugin repository -->
    <pluginRepositories>
        <pluginRepository>
            <id>org.squashtest.plugins.release</id>
            <name>squashtest.org</name>
            <url>http://repo.squashtest.org/maven2/releases</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
        </pluginRepository>
    </pluginRepositories>

</project>
