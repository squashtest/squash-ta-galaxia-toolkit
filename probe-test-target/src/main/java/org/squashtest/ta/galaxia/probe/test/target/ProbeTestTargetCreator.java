/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.probe.test.target;

import java.net.URL;
import org.squashtest.ta.framework.annotations.TATargetCreator;
import org.squashtest.ta.framework.components.TargetCreator;

/**
 *
 * @author edegenetais
 */
@TATargetCreator("probe.test.target.creator")
public class ProbeTestTargetCreator implements TargetCreator<TerranDesignatedTarget>{

    @Override
    public boolean canInstantiate(URL propertiesFile) {
        return propertiesFile.sameFile(getClass().getResource("testTarget.properties"));
    }

    @Override
    public TerranDesignatedTarget createTarget(URL propertiesFile) {
        if(canInstantiate(propertiesFile)){
            return new TerranDesignatedTarget();
        }else{
            throw new UnsupportedOperationException("Not my fucking job (tm)");
        }
    }

}
