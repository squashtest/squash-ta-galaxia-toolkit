/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.probe.test.target;

import java.util.Collection;
import org.squashtest.ta.framework.annotations.TABinaryAssertion;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.Resource;
import org.squashtest.ta.framework.exception.BinaryAssertionFailedException;

/**
 *
 * @author edegenetais
 */
@TABinaryAssertion("grOOves")
public class BinaryAssertion implements org.squashtest.ta.framework.components.BinaryAssertion<FileResource, FileResource>{
    
    private static final String NOT_SUPPORTED_YET = "Not supported yet.";

    @Override
    public void setActualResult(FileResource actual) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setExpectedResult(FileResource expctd) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addConfiguration(Collection<Resource<?>> clctn) {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void test() throws BinaryAssertionFailedException {
        throw new UnsupportedOperationException(NOT_SUPPORTED_YET); //To change body of generated methods, choose Tools | Templates.
    }
    
}
