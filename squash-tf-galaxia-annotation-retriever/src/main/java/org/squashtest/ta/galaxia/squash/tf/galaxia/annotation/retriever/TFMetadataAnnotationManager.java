/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.galaxia.annotation.retriever;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author qtran
 */public class TFMetadataAnnotationManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(TFMetadataAnnotationManager.class);
    
    private ClassLoader bundleClassloader;
    private static final String DISPLAY_NAME = "org.junit.jupiter.api.DisplayName";
    private static final String ANNOTATION_KEY_METHOD = "key";
    private static final String ANNOTATION_VALUE_METHOD = "value";
    private static final String TF_METADATA = "org.squashtest.ta.galaxia.squash.tf.galaxia.annotations.TFMetadata";
    private static final String TF_METADATA_LIST = "org.squashtest.ta.galaxia.squash.tf.galaxia.annotations.TFMetadataList";

    public TFMetadataAnnotationManager(ClassLoader bundleClassloader) {
        this.bundleClassloader = bundleClassloader;
    }

    public ClassLoader getBundleClassloader() {
        return bundleClassloader;
    }

    public void setBundleClassloader(ClassLoader bundleClassloader) {
        this.bundleClassloader = bundleClassloader;
    }

    public JunitTestMethodController getMethodTFMetadataAnnotationContents(String className, String methodDisplayName) {
        JunitTestMethodController result = new JunitTestMethodController();
        result.setNeedToControll(false);
            
        if (bundleClassloader != null) {
            String newMethodDisplayName = removeMethodNameParentheses(methodDisplayName);
            result = chargeJunitTestBundleClass(className, newMethodDisplayName);            
        }        
        return result;        
    }

    private JunitTestMethodController chargeJunitTestBundleClass(String className, String newMethodDisplayName) {
        JunitTestMethodController result = new JunitTestMethodController();
        result.setNeedToControll(false);
        
        Class<?> bundleCls = null;
        try {
            bundleCls = bundleClassloader.loadClass(className);
            Method[] methodsInBundle = bundleCls.getMethods();
            for (Method m : methodsInBundle){
                String methodName = m.getName();
                String name = getMethodDisplayNameInBundle(bundleClassloader, m);
                methodName = (name != null)? name : methodName;
                if (methodName.equals(newMethodDisplayName)){
                    result.setNeedToControll(true);
                    result.setMethodName(newMethodDisplayName);
                    List<MetadataAnnotationController> metadataAnnotationController = new LinkedList<>();
                    //get the TFMetadata content
                    getTFAnnotationContents(m, metadataAnnotationController);
                    result.setMetadataAnnotationControllerList(metadataAnnotationController);
                    return result;
                }
            }
        } catch (ClassNotFoundException ex) {
            LOGGER.debug("Cannot find Junit test bundle class: {}.", className, ex);
        }
        return result;
    }

    private String removeMethodNameParentheses(String methodDisplayName) {
        String newMethodDisplayName = methodDisplayName;
        if (methodDisplayName.contains("()")){
            int parenthesesIndex = methodDisplayName.indexOf("()");
            newMethodDisplayName = methodDisplayName.substring(0, parenthesesIndex);
        }
        return newMethodDisplayName;
    }

    private String getMethodDisplayNameInBundle(ClassLoader bundleClassloader, Method m) {
        Class<? extends Annotation> bundleJUnitDisplayNameClass = null;
        String name = null;
        try {
            bundleJUnitDisplayNameClass = (Class<? extends Annotation>) bundleClassloader.loadClass(DISPLAY_NAME);
            if (m.isAnnotationPresent(bundleJUnitDisplayNameClass)){
                name = getDisplayName(bundleJUnitDisplayNameClass, m, name);
            }
        } catch (ClassNotFoundException ex) {
            LOGGER.debug("Cannot find class: {} in bundle.", DISPLAY_NAME, ex);
        } 
        return name;
    }

    private String getDisplayName(Class<? extends Annotation> bundleJUnitDisplayNameClass, Method m, String name) {
        try {
            Method annotationValueMethod = bundleJUnitDisplayNameClass.getMethod(ANNOTATION_VALUE_METHOD, new Class[0]);
            Annotation anno = m.getAnnotation(bundleJUnitDisplayNameClass);
            name = (String) annotationValueMethod.invoke(anno, new Object[0]);
        } catch (NoSuchMethodException | SecurityException ex) {
            LOGGER.debug("Cannot find or access to method: {} of JUnit Annotation class: {} in bundle.", ANNOTATION_VALUE_METHOD, DISPLAY_NAME, ex);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOGGER.debug("Cannot invoke method: {} of JUnit Annotation class: {} in bundle.", ANNOTATION_VALUE_METHOD, DISPLAY_NAME, ex);
        }
        return name;
    }
      
    private List<String> getStringArrayValueInAnnotation(Annotation anno, Class<? extends Annotation> bundleTFMetadataAnnoClass, String methodName) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Method annotationValueMethod = bundleTFMetadataAnnoClass.getMethod(methodName, new Class[0]);
        String[] valueArray = null;
        valueArray = (String[]) annotationValueMethod.invoke(anno, new Object[0]);
        
        List<String> result = new ArrayList<>();
        
        for (String str : valueArray){
            result.add(str.trim());
        }
        
        return result;
    }
    
    private void getTFAnnotationContents(Method m, List<MetadataAnnotationController> metadataAnnotationList) {
        Class<? extends Annotation> bundleTFMetadataAnnoClass = null;
        Class<? extends Annotation> bundleTFMetadataListAnnoClass = null;
        try {
            bundleTFMetadataAnnoClass = (Class<? extends Annotation>) bundleClassloader.loadClass(TF_METADATA);
            bundleTFMetadataListAnnoClass = (Class<? extends Annotation>) bundleClassloader.loadClass(TF_METADATA_LIST);
        } catch (ClassNotFoundException ex) {
            LOGGER.debug("Cannot find Annotation class in bundle.", ex);
        }
        
        if (bundleTFMetadataAnnoClass != null ){
            if (m.isAnnotationPresent(bundleTFMetadataAnnoClass)) {
                getTFMetadataAnnotationContent(bundleTFMetadataAnnoClass, m, metadataAnnotationList);
            } else if (bundleTFMetadataListAnnoClass != null && m.isAnnotationPresent(bundleTFMetadataListAnnoClass)) {
                getTFMetadataListAnnotationContent(bundleTFMetadataAnnoClass, m, metadataAnnotationList);
            }
        } 
    
    }

    private void getTFMetadataListAnnotationContent(Class<? extends Annotation> bundleTFMetadataAnnoClass, Method m, List<MetadataAnnotationController> metadataAnnotationList) {
        try {
            Method annoMethod = bundleTFMetadataAnnoClass.getMethod(ANNOTATION_KEY_METHOD, new Class[0]);
            Annotation[] annos = m.getAnnotationsByType(bundleTFMetadataAnnoClass);
            
            for (Annotation anno : annos) {
                getMetadataKeyValues(annoMethod, anno, bundleTFMetadataAnnoClass, metadataAnnotationList);
            }
        } catch (NoSuchMethodException | SecurityException ex) {
            LOGGER.debug("Cannot find or access to method: {} of TFMetadataList Annotation class in bundle.", ANNOTATION_VALUE_METHOD, ex);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOGGER.debug("Cannot invoke method: {} of TFMetadataList Annotation class in bundle.", ANNOTATION_VALUE_METHOD, ex);
        }
    }

    private void getTFMetadataAnnotationContent(Class<? extends Annotation> bundleTFMetadataAnnoClass, Method m, List<MetadataAnnotationController> metadataAnnotationList) {
        try {
            Method annoMethod = bundleTFMetadataAnnoClass.getMethod(ANNOTATION_KEY_METHOD, new Class[0]);
            Annotation anno = m.getAnnotation(bundleTFMetadataAnnoClass);
            
            getMetadataKeyValues(annoMethod, anno, bundleTFMetadataAnnoClass, metadataAnnotationList);
        } catch (NoSuchMethodException | SecurityException ex) {
            LOGGER.debug("Cannot find or access to method: {} of TFMetadata Annotation class in bundle.", ANNOTATION_VALUE_METHOD, ex);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOGGER.debug("Cannot invoke method: {} of TFMetadata Annotation class in bundle.", ANNOTATION_VALUE_METHOD, ex);
        }
    }

    private void getMetadataKeyValues(Method annoMethod, Annotation anno, Class<? extends Annotation> bundleTFMetadataAnnoClass, List<MetadataAnnotationController> metadataAnnotationList) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String annoKey = null;
        List<String> annoValues = new ArrayList<>();
        annoKey = (String) annoMethod.invoke(anno, new Object[0]);
        annoValues = getStringArrayValueInAnnotation(anno, bundleTFMetadataAnnoClass, ANNOTATION_VALUE_METHOD);
        if (annoKey != null){
            metadataAnnotationList.add(new MetadataAnnotationController(annoKey.trim(), annoValues));
        }
    }
    
}
