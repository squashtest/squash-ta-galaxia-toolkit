/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.squash.tf.galaxia.annotation.retriever;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author qtran
 */
public class JunitTestMethodController {
    private boolean needToControll;
    private String methodName;
    private List<MetadataAnnotationController> metadataAnnotationControllerList = new LinkedList<>();

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<MetadataAnnotationController> getMetadataAnnotationControllerList() {
        return metadataAnnotationControllerList;
    }

    public void setMetadataAnnotationControllerList(List<MetadataAnnotationController> metadataAnnotationControllerList) {
        this.metadataAnnotationControllerList = metadataAnnotationControllerList;
    }

    public boolean isNeedToControll() {
        return needToControll;
    }

    public void setNeedToControll(boolean needToControll) {
        this.needToControll = needToControll;
    }    

}
