/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.tf.param.service;


import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author dclaerhout
 */
public class TFParamServiceImpl extends TFParamService{

    private static final Logger LOGGER = LoggerFactory.getLogger(TFParamService.class);
   
    
    /*Attributes*/
    
    private static Parameters parameters;
      
    /*Constructor*/
    
    private TFParamServiceImpl(ParametersFactory paramFactory) {
        this.parameters = paramFactory.getParameters();
    }
  
    
    /*Methods*/
    
    
    //This method is used by Squash TF Junit runner to instanciate a new TFParamService with parameters received in JSON. It must be used only once or the previous parameters will be overwritten.
    public static void registerTFParamServiceImpl(ParametersFactory paramFactory){
        new TFParamServiceImpl(paramFactory);
    }
  
    private static Properties getWantedTestParamList(String testName){
            
        if (parameters!=null) {
            LOGGER.debug("TFParamServiceImpl: Method getWantedTestParamList(String testName) : Retrieving the  params list of the wanted test. Testname: {}", testName);
            return parameters.getTestsParamsList().get(testName); 
        } else {
            LOGGER.debug("TFParamServiceImpl: Method getWantedTestParamList(String testName) : The parameters are null. Returning an empty new Properties()");
            return new Properties();
        }       
    }

    @Override
    public String getTestParam(String paramName) {
        
        String testName;
        if (getTestInfo().getTestId()== null || "".equals(getTestInfo().getTestId())){
            testName = processEcosystemName(getTestInfo().getEcosystemName()) +"/"+ getTestInfo().getTestName()+"{}";
        } else {
            testName = processEcosystemName(getTestInfo().getEcosystemName()) +"/"+ getTestInfo().getTestName()+"{"+getTestInfo().getTestId()+"}";
        }
        
        LOGGER.debug("TFParamServiceImpl: Method getTestParamList(String testName) : Retrieving param value of wanted param. ParamName: {}", paramName);
        return getWantedTestParamList(testName).getProperty(paramName, null);
    }

    @Override
    public String getTestParam(String paramName, String defaultValue){

        String testName;
        if (getTestInfo().getTestId()== null || "".equals(getTestInfo().getTestId())){
            testName = processEcosystemName(getTestInfo().getEcosystemName()) +"/"+ getTestInfo().getTestName()+"{}";
        } else {
            testName = processEcosystemName(getTestInfo().getEcosystemName()) +"/"+ getTestInfo().getTestName()+"{"+getTestInfo().getTestId()+"}";
        }

        LOGGER.debug("TFParamServiceImpl: Method getTestParamList(String testName, String defaultValue)) : Retrieving param value of wanted param. ParamName: {} , DefaultValue: {}", paramName, defaultValue);
        return getWantedTestParamList(testName).getProperty(paramName, defaultValue);

    }
    
    @Override  
    public String getParam(String paramName){     
        String wantedParam=getTestParam(paramName);
                
        if (wantedParam!=null){
            LOGGER.debug("TFParamServiceImpl: Method getParam(String paramName): Retrieving param value of wanted param. ParamName: {}", paramName);
            return wantedParam;
        } else {
            LOGGER.debug("TFParamServiceImpl: Method getParam(String paramName): The param value has not been found in test params. Now checking global params. ParamName: {}", paramName);
            return getGlobalParam(paramName);
        }  
    }      
    
    @Override 
    public String getParam(String paramName, String defaultValue){
            
        String wantedParam=getTestParam(paramName, defaultValue);
                
        if (wantedParam!=null){
            LOGGER.debug("TFParamServiceImpl: Method getParam(String paramName, String defaultValue): Retrieving param value of wanted param. ParamName: {} , DefaultValue: {}", paramName, defaultValue);
            return wantedParam;
        } else {
            LOGGER.debug("TFParamServiceImpl: Method getParam(String paramName, String defaultValue): The param value has not been found in test params. Now checking global params. ParamName: {} , DefaultValue: {}", paramName, defaultValue);           
            return getGlobalParam(paramName,defaultValue);
        }
    }
    
    @Override  
    public String getGlobalParam(String paramName) {
        
        if (parameters!=null) {
            LOGGER.debug("TFParamServiceImpl: Method getGlobalParam(String paramName): Retrieving the wanted parameter from global params list. ParamName: {}", paramName);
            return parameters.getGlobalParamsList().getProperty(paramName, null);
        } else {
            LOGGER.debug("TFParamServiceImpl: Method getGlobalParam(String paramName): The parameters are null. Returning null.");
            return null;
        }
    }

    @Override
    public String getGlobalParam(String paramName, String defaultValue) {
        
        if (parameters!=null) {
             LOGGER.debug("TFParamServiceImpl: Method getGlobalParam(String paramName, String defaultValue): Retrieving the wanted parameter from global params list. ParamName: {} , DefaultValue: {}", paramName, defaultValue);       
            return parameters.getGlobalParamsList().getProperty(paramName, defaultValue);
            
        } else {
            LOGGER.debug("TFParamServiceImpl: Method getGlobalParam(String paramName, String defaultValue) : The parameters are null. Returning default value. DefaultValue: {}", defaultValue);
            return defaultValue;
        }
    }

    //TODO This a temporary fix because the ecosystem name has "." instead of ":" after the bundle name.
    private String processEcosystemName (String ecosystemName) {
        String target = "maven.test.bundle.";
        String replacement = "maven.test.bundle:";
        String otherTarget = "maven.main.bundle.";
        String otherReplacement = "maven.main.bundle:";
        
        if (ecosystemName.startsWith(target)){
            return ecosystemName.replace(target, replacement);
        } else if (ecosystemName.startsWith(otherTarget)){
            return ecosystemName.replace(otherTarget, otherReplacement);
        } else {
            return ecosystemName;
        }           
    }

 }
