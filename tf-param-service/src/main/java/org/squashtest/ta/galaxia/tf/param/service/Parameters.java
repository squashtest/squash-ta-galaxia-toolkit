/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.tf.param.service;

import java.util.Map;
import java.util.Properties;

/**
 *
 * @author dclaerhout
 */
public class Parameters {
    
    /*Attributes*/
    
    private Map <String , Properties> testsParamsList;
    private Properties globalParams;
    
    
    /*Constructor*/
    
    public Parameters(Map<String, Properties> testsParamsList, Properties globalParams ) {
        this.testsParamsList = testsParamsList;
        this.globalParams = globalParams;
    }
    
    
    /*Getters*/
    
    public Map<String, Properties> getTestsParamsList() {
        
        return testsParamsList;
    }
    
    public Properties getGlobalParamsList() {
        return globalParams;
    }
    
}
