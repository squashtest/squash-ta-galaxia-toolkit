/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.tf.param.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.commons.factories.specification.SuiteSpecification;
import org.squashtest.ta.commons.factories.specification.TestSpecification;

/**
 *
 * @author dclaerhout
 */
public class ParametersFactory {

    
    /*Attributes*/
    
    private static Logger LOGGER = LoggerFactory.getLogger(ParametersFactory.class);
    private SuiteSpecification suiteSpecs;
    private Parameters param;
    
    /*Constructor*/
    
    public ParametersFactory(SuiteSpecification suiteSpecs) {
        this.suiteSpecs = suiteSpecs;
    }
    
    /*Methods*/
    
    public SuiteSpecification getSuiteSpecs() {
        return suiteSpecs;
    }


    public Parameters getParameters(){
                 
        if (suiteSpecs!=null) {
            
            Properties globalParams = suiteSpecs.getGlobalParams();
            List<TestSpecification> testSpecs = suiteSpecs.getTests();     

            Map<String, Properties> loadParam = new HashMap<>(); 
            Properties testParam;
            LOGGER.debug("ParametersFactory: Retrieving parameters from SuiteSpecification.");
                for (TestSpecification testSpec: testSpecs) {
                    testParam=testSpec.getParam();
                    loadParam.put(testSpec.getScript()+"{"+testSpec.getId()+"}", testParam);
                }

            param = new Parameters(loadParam, globalParams);

            return param;     
    
        } else {
            LOGGER.debug("ParametersFactory: The given SuiteSpecification was null. There aren't any paramaters to retrieve. Returning Parameters=null.");
            return null;
        }
   }
}
