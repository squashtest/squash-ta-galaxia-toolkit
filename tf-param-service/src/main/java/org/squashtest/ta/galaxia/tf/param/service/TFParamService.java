/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.tf.param.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author dclaerhout
 */
public class TFParamService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TFParamService.class);
    
    private static TFParamService paramService = new TFParamService();
    private static String TF_PARAM_SERVICE_LOGGER_INTRO_MESSAGE = "TFParamService: TF JUnit Runner is not used. ";
    private CurrentTestInfo testInfo;
   

    protected TFParamService() {
        paramService=this;
    }

    public static TFParamService getInstance() {
        return paramService;
    }

    public String getTestParam(String paramName) {
        LOGGER.debug(TF_PARAM_SERVICE_LOGGER_INTRO_MESSAGE + "Returning null for method getTestParam(String paramName). ParamName: {}", paramName);
        return null;    
    }
    
    public String getTestParam(String paramName, String defaultValue){
        LOGGER.debug(TF_PARAM_SERVICE_LOGGER_INTRO_MESSAGE + "Returning defaultValue for method getTestParam(String paramName, String defaultValue). ParamName: {} , DefaultValue: {}", paramName, defaultValue);
        return defaultValue;
    }
    
    public String getParam(String paramName){
        LOGGER.debug(TF_PARAM_SERVICE_LOGGER_INTRO_MESSAGE + "Returning null for method getParam(paramName). ParamName: {}", paramName);
        return null;    
    }        
    
    public String getParam(String paramName, String defaultValue){
        LOGGER.debug(TF_PARAM_SERVICE_LOGGER_INTRO_MESSAGE + "Returning defaultValue for method getParam(String paramName, String defaultValue). ParamName: {} , DefaultValue: {}", paramName, defaultValue);
        return defaultValue;
    }
    public String getGlobalParam(String paramName) {
        LOGGER.debug(TF_PARAM_SERVICE_LOGGER_INTRO_MESSAGE + "Returning null for method getGlobalParam(String paramName). ParamName: {}", paramName);
        return null;   
    }

    public String getGlobalParam(String paramName, String defaultValue) {
        LOGGER.debug(TF_PARAM_SERVICE_LOGGER_INTRO_MESSAGE + "Returning defaultValue for method getGlobalParam(String paramName, String defaultValue). ParamName: {} , DefaultValue: {}", paramName, defaultValue);
        return defaultValue;
    }

    public CurrentTestInfo getTestInfo() {
        return testInfo;
    }
    
    public void updateTestInfo (String ecosystemName, String testName, String testId) {
        testInfo= new CurrentTestInfo(ecosystemName, testName, testId);
    }
 
 }
