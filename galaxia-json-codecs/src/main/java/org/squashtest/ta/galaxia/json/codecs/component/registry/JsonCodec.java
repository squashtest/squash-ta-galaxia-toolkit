/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.json.codecs.component.registry;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignature;

/**
 * This codec loads a component model from a json file.
 * @author edegenetais
 */
public class JsonCodec {
    private ObjectMapper jacksonCodec;
    public JsonCodec() {
        jacksonCodec=new ObjectMapper();
        jacksonCodec.addMixIn(SquashDSLMacroSignature.class, SquashDSLMacroSignatureMixin.class);
    }
    
    public SquashDSLComponentRegistry unmarshall(File f) throws IOException{
        return jacksonCodec.readValue(f, SquashDSLComponentRegistry.class);
    }
    
    public void marshall(SquashDSLComponentRegistry registry,File destination) throws IOException{
        jacksonCodec.writeValue(destination, registry);
    }
            
}
