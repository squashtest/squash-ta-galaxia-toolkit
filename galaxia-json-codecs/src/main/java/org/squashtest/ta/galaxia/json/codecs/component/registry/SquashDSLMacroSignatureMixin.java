/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.json.codecs.component.registry;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroFixedPart;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroParam;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = SquashDSLMacroFixedPart.class, name="fixedPart"),
        @JsonSubTypes.Type(value = SquashDSLMacroParam.class, name="param")
})
/**
 * This class is part of the jackson-based json codec for the 
 * {@link org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignature} class.
 * @author edegenetais
 */
public class SquashDSLMacroSignatureMixin {

}
