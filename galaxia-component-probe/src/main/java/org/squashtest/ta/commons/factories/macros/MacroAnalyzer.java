/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.commons.factories.macros;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import org.squashtest.ta.commons.factories.SerialGenerator;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroFixedPart;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroParam;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroParamDefinition;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroParamType;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignature;

/**
 * This class instruments the MAcro tolling package to get some internals exposed in a controlled way.
 * This allows us to support ANY version of the Squash DSL Framework out of the box.
 * When the Framework evolves Macro tolling, this class may break. At that time we should give ourselves two (additional) goals :
 * <ol>
 *  <li>Manage the breaking (be it in the form of a linkage error or incomplete data extracted from here) gracefully, so that backward compatibility
 *      is retained.</li>
 *  <li>Create a suitable API in the new macro tooling so that further hacks like this one are avoided.</li>
 * </ol>
 * @author edegenetais
 */
public class MacroAnalyzer extends Macro {
    private String firstLine;
    public MacroAnalyzer(Macro m) {
        super(m.getOriginalFile(),new SerialGenerator(){
            @Override
            public Integer nextInt() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        try(BufferedReader f=new BufferedReader(new FileReader(m.getOriginalFile()))){
            firstLine=f.readLine();
        }catch(FileNotFoundException fnfe){
            throw new Error("Something's rotten in the realm of Denmark : how on earth could the definition of an already loaded macro be aunavailable ?", fnfe);
        }catch(IOException e){
            throw new RuntimeException("O_o juste read, why can't we reread it now ?",e);
        }
    }
    
    public List<SquashDSLMacroSignature> getSignature(){
        Matcher m=getMatcher();
        Map<String,String> parmMapping=m.readParameters(firstLine);
        SortedSet<SquashDSLMacroSignature> sorter=new TreeSet<SquashDSLMacroSignature>(new Comparator<SquashDSLMacroSignature>(){
            @Override
            public int compare(SquashDSLMacroSignature t, SquashDSLMacroSignature t1) {
                if(firstLine.indexOf(t.definitionPart())<firstLine.indexOf(t1.definitionPart())){
                    return -1;
                }else if(firstLine.indexOf(t.definitionPart())==firstLine.indexOf(t1.definitionPart())){
                    return 0;
                }else{
                    return 1;
                }
            }
            
        });
        
        for(String parmName:parmMapping.keySet()){
            final String strippedParmName = parmName.substring(1, parmName.length()-1);
            SquashDSLMacroParamDefinition des=new SquashDSLMacroParamDefinition(strippedParmName, new SquashDSLMacroParamType("Raw", "raw"));
            sorter.add(new SquashDSLMacroParam(des));
        }
        int fixedPartBegin=2; //we remove the macro marker from the signature defintion.
        List<SquashDSLMacroFixedPart> fixedParts=new ArrayList<>();
        for(SquashDSLMacroSignature parm:sorter){
            String parmCode=parm.definitionPart();
            int fixedPartEnd=firstLine.indexOf(parmCode);
            String fixedPartCandiate=firstLine.substring(fixedPartBegin,fixedPartEnd);
            if(fixedPartCandiate.length()>0){
                //This supports the - admitedly odd, but never explicitely forbidden - case of a macro definition begining by a parameter.
                fixedParts.add(new SquashDSLMacroFixedPart(fixedPartCandiate));
            }
            fixedPartBegin=fixedPartEnd+parmCode.length();
        }
        String noParamFixedPart = firstLine.substring(fixedPartBegin);
        if (!"".equals(noParamFixedPart.trim())){
            fixedParts.add(new SquashDSLMacroFixedPart(noParamFixedPart));
        }
        sorter.addAll(fixedParts);
        return new ArrayList<>(sorter);
    }
}
