/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.component.exporter;

import java.io.File;
import java.io.IOException;
import org.squashtest.ta.commons.exporter.ReportType;
import org.squashtest.ta.commons.exporter.ResultExporter;
import org.squashtest.ta.core.tools.io.FileTree;
import org.squashtest.ta.framework.test.result.SuiteResult;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.galaxia.component.probe.ComponentRegistryBuilder;
import org.squashtest.ta.galaxia.component.probe.GalaxiaProbeRepositoryCreator;
import org.squashtest.ta.galaxia.component.probe.MacroDefinitionExtractor;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.json.codecs.component.registry.JsonCodec;

/**
 *
 * @author edegenetais
 */
public class GalaxiaComponentRegistryExporter implements ResultExporter{
    
    private String outputDirectoryName="probe";
    private String registryName="componentRegistry.json";
    
    @Override
    public void write(File targetDirectory, SuiteResult sr) throws IOException {
        File destinationDir=new File(targetDirectory,outputDirectoryName);
        if(destinationDir.mkdirs() || destinationDir.exists()){
            /*
            * We cannot afford to collide with the previously created macro extraction directory, 
            * as these collisions are not properly managed in all versions of the commons exploitation layer.
            */
            File macroTempDirectory=null;
            try{
                macroTempDirectory = FileTree.FILE_TREE.createTempDirectory();
                TempDir.setMacroTempDir(macroTempDirectory);
                ComponentRegistryBuilder builder=new ComponentRegistryBuilder(new GalaxiaProbeRepositoryCreator(), new MacroDefinitionExtractor());
                SquashDSLComponentRegistry registry=builder.buildRegistry();
                new JsonCodec().marshall(registry,new File(destinationDir,registryName));
            }finally{
                if(macroTempDirectory!=null && macroTempDirectory.exists()){
                    FileTree.FILE_TREE.clean(macroTempDirectory);
                }
            }
        }else{
            throw new IOException("Failed to create registry data destination directory "+destinationDir.getAbsolutePath());
        }
    }

    @Override
    public String getOutputDirectoryName() {
        return outputDirectoryName;
    }

    @Override
    public void setOutputDirectoryName(String directoryName) {
        this.outputDirectoryName=directoryName;
    }

    @Override
    public void setReportType(ReportType rt) {
        //noop : I don't care !
    }

    public String getRegistryName() {
        return registryName;
    }

    public void setRegistryName(String registryName) {
        this.registryName = registryName;
    }

}
