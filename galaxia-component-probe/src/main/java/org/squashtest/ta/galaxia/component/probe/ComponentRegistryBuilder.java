/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.component.probe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.init.EngineComponentDefinitionManager;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLAssertion;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLCommand;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLConverter;

/**
 * This component compiles all component and macro data extracted by probe components and builds
 * the {@link SquashDSLComponentRegistry} instance to publish that data.
 * @author edegenetais
 */
public class ComponentRegistryBuilder {
    private final ComponentFinder componentFinder;
    private final MacroFinder macroFinder;

    public ComponentRegistryBuilder(ComponentFinder componentFinder, MacroFinder macroFinder) {
        this.componentFinder = componentFinder;
        this.macroFinder = macroFinder;
    }

    public SquashDSLComponentRegistry buildRegistry(){
        
        Set<String> targetTypes=findsTargetTypes();
        
        HashSet<String> resourceTypes = findResourceTypes();
        
        List<SquashDSLAssertion> assertions=findAssertions();
        
        List<SquashDSLCommand> commands = findCommands();
        
        List<SquashDSLConverter> converters = findConverters();
        
        return new SquashDSLComponentRegistry(assertions, commands, converters, macroFinder.getMacros(), resourceTypes,targetTypes);
    }

    private List<SquashDSLConverter> findConverters() {
        final List<SquashDSLConverter> converters = new ArrayList<>();
        for(ConverterDefinition def:componentFinder.getComponentManager().getConverterDefinitions()){
            converters.add(new SquashDSLConverter(def.getCategory().getName(), def.getFirstNature().getName(), def.getSecondNature().getName()));
        }
        return converters;
    }

    private ArrayList<SquashDSLCommand> findCommands() {
        final ArrayList<SquashDSLCommand> commands = new ArrayList<>();
        for(CommandDefinition def:componentFinder.getComponentManager().getCommandDefinitions()){
            commands.add(new SquashDSLCommand(def.getCategory().getName(), def.getFirstNature().getName(), def.getSecondNature().getName(), def.getResultNature().getName()));
        }
        return commands;
    }

    private List<SquashDSLAssertion> findAssertions() {
        List<SquashDSLAssertion> assertions=new ArrayList<>();
        for(BinaryAssertionDefinition def:componentFinder.getComponentManager().getBinaryAssertionDefinitions()){
            assertions.add(new SquashDSLAssertion(def.getCategory().getName(), def.getFirstNature().getName(), def.getSecondNature().getName()));
        }
        for(UnaryAssertionDefinition def:componentFinder.getComponentManager().getUnaryAssertionDefinitions()){
            assertions.add(new SquashDSLAssertion(def.getCategory().getName(),def.getFirstNature().getName(),null));
        }
        return assertions;
    }

    private HashSet<String> findResourceTypes() {
        final HashSet<String> resourceTypes = new HashSet<>();
        final EngineComponentDefinitionManager componentManager = componentFinder.getComponentManager();
        
        for(BinaryAssertionDefinition def:componentManager.getBinaryAssertionDefinitions()){
            resourceTypes.add(def.getFirstNature().getName());
            resourceTypes.add(def.getSecondNature().getName());
        }
        
        for(CommandDefinition def:componentManager.getCommandDefinitions()){
            resourceTypes.add(def.getFirstNature().getName());
            resourceTypes.add(def.getResultNature().getName());
        }
        
        for(ConverterDefinition def:componentManager.getConverterDefinitions()){
            resourceTypes.add(def.getFirstNature().getName());
            resourceTypes.add(def.getSecondNature().getName());
        }
        
        for(UnaryAssertionDefinition def:componentManager.getUnaryAssertionDefinitions()){
            resourceTypes.add(def.getFirstNature().getName());
        }
        
        return resourceTypes;
    }
    
    private Set<String> findsTargetTypes(){
        
        Set<String> types=new HashSet<>();
        final EngineComponentDefinitionManager componentManager = componentFinder.getComponentManager();
        for(CommandDefinition def:componentManager.getCommandDefinitions()){
            types.add(def.getSecondNature().getName());
        }
        return types;
    }
}
