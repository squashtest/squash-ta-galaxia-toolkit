/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.component.probe;

import java.util.ArrayList;
import java.util.List;
import org.squashtest.ta.commons.factories.macros.Macro;
import org.squashtest.ta.commons.factories.macros.MacroAnalyzer;
import org.squashtest.ta.commons.factories.macros.TestSuiteMacro;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacro;

/**
 *
 * @author edegenetais
 */
public class MacroDefinitionExtractor implements MacroFinder{
    public List<SquashDSLMacro> getMacros(){
        TestSuiteMacro macroRegistry=extract();
        
        List<Macro> macros=macroRegistry.getMacros();
        List<SquashDSLMacro> macroDefinitions=new ArrayList<>(macros.size());
        int i=1;
        for(Macro m:macros){
            
            MacroAnalyzer analyzer=new MacroAnalyzer(m);
            
            SquashDSLMacro definition=new SquashDSLMacro("macro"+(i++), analyzer.getSignature());
            
            macroDefinitions.add(definition);
        }
        return macroDefinitions;
    }

    private TestSuiteMacro extract() {
        TestSuiteMacro macroRegistry=new TestSuiteMacro();
        macroRegistry.addMacrosFromClasspath("builtin/macros/");
        return macroRegistry;
    }
}
