/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.component.probe;

import java.net.URL;
import java.util.Properties;
import javax.inject.Inject;
import org.squashtest.ta.backbone.init.EngineComponentDefinitionManager;
import org.squashtest.ta.framework.annotations.TARepositoryCreator;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.RepositoryCreator;
import org.squashtest.ta.framework.components.ResourceRepository;

/**
 * This dummy component is used to instrument the engine context to get 
 * the component list.
 * @author edegenetais
 */
@TARepositoryCreator("galaxia.probe")
public class GalaxiaProbeRepositoryCreator implements RepositoryCreator<ResourceRepository>,ComponentFinder{
    
    /** 
     * This resource repository will NEVER find anything : it is here only to complete interface implementation with minimal disturbance
     * to the project configuration.
     */
    public static class ProbeResourceRepository implements ResourceRepository{

        @Override
        public void init() {/*noop : */}

        @Override
        public void reset() {/*noop : */}

        @Override
        public void cleanup() {/*noop : */}

        @Override
        public Properties getConfiguration() {
                return new Properties();
        }

        @Override
        public FileResource findResources(String string) {
            return null;
        }
        
    }
    /** This static variable is used as a handle on the instance that will be created by the engine context. */
    private static GalaxiaProbeRepositoryCreator instance=null;

    @Inject
    private EngineComponentDefinitionManager componentManager;
    
    public GalaxiaProbeRepositoryCreator(){
        // Leak the instance so that the probe will be able to inspect the component registry.
        if(instance==null || instance.componentManager==null){
            instance=this;
        }
    }

    @Override
    public EngineComponentDefinitionManager getComponentManager(){
        if(instance==null || instance.componentManager==null){
            throw new IllegalStateException("The probe class has NOT been loaded into the engine. No data available.");
        }
        return instance.componentManager;
    }
    
   @Override
    public boolean canInstantiate(URL url) {return false;}
    
   @Override
    public ResourceRepository createRepository(URL url) {
        return new ProbeResourceRepository();
    }    
}
