/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.component.probe;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;
import org.squashtest.ta.framework.tools.TempDir;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacro;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroFixedPart;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroParam;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignature;

/**
 *
 * @author edegenetais
 */
public class MacroDefinitionExtractorTest extends ResourceExtractorTestBase{
    
    @Before
    public void prepareTestDirectory() throws IOException{
        /*
        * We need to make sure temporary files don't previously exist, because this test uses the engine's
        * temporary storage subsystem in an unusual way, and the beast does have a rather volatile temper...
        */
        File macroTempDir=new File(createNtrackDir(),"macros");
        TempDir.setMacroTempDir(macroTempDir);
    }
    
    @Test
    public void findMacros(){
        List<SquashDSLMacro> macros=new MacroDefinitionExtractor().getMacros();
        Assert.assertEquals(2, macros.size());
    }
    
    @Test
    public void getMostFrequentMacroStyleParmNames(){
        List<SquashDSLMacro> macros=new MacroDefinitionExtractor().getMacros();
        List<String> exectedNames=Arrays.asList(new String[]{"firstParam","secondParam"});
        List<String> actualNames=new ArrayList<>();
        for(SquashDSLMacroSignature signPart:macros.get(0).getSignatures()){
            if(SquashDSLMacroParam.TYPE.equals(signPart.getType())){
                actualNames.add(((SquashDSLMacroParam)signPart).getDefinition().getName());
            }
        }
        Assert.assertEquals("Expected macro parameter names not found", exectedNames, actualNames);
    }
    
    @Test
    public void getMostFrequentMacroStyleFixedParts(){
        List<SquashDSLMacro> macros=new MacroDefinitionExtractor().getMacros();
        List<String> exectedNames=Arrays.asList(new String[]{"TEST MACRO "," FOR "," AND TO CAP IT"});
        List<String> actualNames=new ArrayList<>();
        for(SquashDSLMacroSignature signPart:macros.get(0).getSignatures()){
            if(SquashDSLMacroFixedPart.TYPE.equals(signPart.getType())){
                actualNames.add(((SquashDSLMacroFixedPart)signPart).getContent());
            }
        }
        Assert.assertEquals("Expected fixed parts not found", exectedNames, actualNames);
    }
    
    //but let's not ignore the minority report :p
    @Test
    public void getMinorityMacroStyleParmNames(){
        List<SquashDSLMacro> macros=new MacroDefinitionExtractor().getMacros();
        List<String> exectedNames=Arrays.asList(new String[]{"conernercase","custom"});
        List<String> actualNames=new ArrayList<>();
        for(SquashDSLMacroSignature signPart:macros.get(1).getSignatures()){
            if(SquashDSLMacroParam.TYPE.equals(signPart.getType())){
                actualNames.add(((SquashDSLMacroParam)signPart).getDefinition().getName());
            }
        }
        Assert.assertEquals("Expected macro parameter names not found", exectedNames, actualNames);
    }
    
    @Test
    public void getMinorityMacroStyleFixedParts(){
        List<SquashDSLMacro> macros=new MacroDefinitionExtractor().getMacros();
        List<String> exectedNames=Arrays.asList(new String[]{"THIS IS $(",") MACRO OF "});
        List<String> actualNames=new ArrayList<>();
        for(SquashDSLMacroSignature signPart:macros.get(1).getSignatures()){
            if(SquashDSLMacroFixedPart.TYPE.equals(signPart.getType())){
                actualNames.add(((SquashDSLMacroFixedPart)signPart).getContent());
            }
        }
        Assert.assertEquals("Expected fixed parts not found", exectedNames, actualNames);
    }
}
