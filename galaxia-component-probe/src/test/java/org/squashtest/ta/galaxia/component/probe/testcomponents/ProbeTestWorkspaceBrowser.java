/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.component.probe.testcomponents;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import org.squashtest.ta.framework.annotations.TARepository;
import org.squashtest.ta.framework.components.FileResource;
import org.squashtest.ta.framework.components.ResourceRepository;
import org.squashtest.ta.framework.facade.TestWorkspaceBrowser;

/**
 *
 * @author edegenetais
 */
public class ProbeTestWorkspaceBrowser implements TestWorkspaceBrowser {

    public ProbeTestWorkspaceBrowser() {
    }

    @Override
    public List<URL> getTargetsDefinitions() {
        return Arrays.asList(new URL[]{getClass().getResource("/org/squashtest/ta/galaxia/probe/test/target/testTarget.properties")});
    }

    @Override
    public List<URL> getRepositoriesDefinitions() {
        return Collections.emptyList();
    }

    @Override
    public ResourceRepository getDefaultResourceRepository() {
        return new ResourceRepositoryImpl();
    }

    @Override
    public File getDefaultResourceFile() {
        try {
            File f = File.createTempFile("dummy", ".properties");
            f.deleteOnExit();
            return f;
        } catch (IOException ex) {
            throw new Error("There's something rotten in the realm of Denmark. Shlouda been able to create an empty temp file.", ex);
        }
    }

    @TARepository(value = "dummy.probe.repository")
    class ResourceRepositoryImpl implements ResourceRepository {

        public ResourceRepositoryImpl() {
        }

        @Override
        public void init() {
            //too lazy :p
        }

        @Override
        public void reset() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void cleanup() {
            //why should I care
        }

        @Override
        public Properties getConfiguration() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public FileResource findResources(String string) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

}
