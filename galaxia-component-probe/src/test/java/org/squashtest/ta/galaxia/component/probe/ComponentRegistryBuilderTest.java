/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.component.probe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.engine.wrapper.Nature;
import org.squashtest.ta.backbone.init.EngineComponentDefinitionManager;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLAssertion;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLCommand;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLConverter;

/**
 *
 * @author edegenetais
 */
@RunWith(MockitoJUnitRunner.class)
public class ComponentRegistryBuilderTest {
    @Mock
    ComponentFinder finderMock;
    
    @Mock
    MacroFinder macroFinder;
    
    @Mock
    EngineComponentDefinitionManager componentManager;
    
    private Set<String> expectedSet(final String[] expected) {
        Set<String> expectedTargetTypes=new HashSet<>(Arrays.asList(expected));
        return expectedTargetTypes;
    }
    
    @Before
    public void setupCommonMockStructure(){
        Mockito.when(finderMock.getComponentManager()).thenReturn(componentManager);
        
        Mockito.when(componentManager.getBinaryAssertionDefinitions()).thenReturn(Arrays.asList(
                new BinaryAssertionDefinition(new Nature("xml"), new Nature("result.sql"), new Nature("equals"), null)
        ));
        
        Mockito.when(componentManager.getCommandDefinitions()).thenReturn(Arrays.asList(
                        new CommandDefinition(Nature.FILE_RESOURCE_NATURE, new Nature("void"), new Nature("chocapic"), Nature.FILE_RESOURCE_NATURE, null),
                        new CommandDefinition(new Nature("ethics"), new Nature("gafa.boss"), new Nature("try.to.infuse"), new Nature("void.resource"), null)
        ));
        
        Mockito.when(componentManager.getConverterDefinitions()).thenReturn(Arrays.asList(
                new ConverterDefinition(new Nature("computer"), new Nature("useless.lump"), new Nature("windows"), null)
        ));
        
        Mockito.when(componentManager.getUnaryAssertionDefinitions()).thenReturn(Arrays.asList(
                new UnaryAssertionDefinition(new Nature("O/S"), new Nature("rocks"), null)
        ));
        
        testee = new ComponentRegistryBuilder(finderMock, macroFinder);
    }
    
    @Test
    public void findTargetTypes(){
        
        Set<String> actualTargetTypes=testee.buildRegistry().getTargetTypes();
        
        Assert.assertEquals("Target types not found", expectedSet(new String[]{"void","gafa.boss"}), actualTargetTypes);
    }
    public ComponentRegistryBuilder testee;

    
    
    @Test
    public void findResourceTypes(){
        Set<String> actualResourceTypes=testee.buildRegistry().getResourceTypes();
        
        Assert.assertEquals("Resource types not found.",
                expectedSet(new String[]{"xml","result.sql","file","void.resource","computer","useless.lump","computer","O/S","ethics"}),
                        actualResourceTypes
        );
    }
    
    @Test
    public void findAssertions(){
        List<SquashDSLAssertion> actualAssertions=normalizeAssertions(testee.buildRegistry().getAssertions());
        
        List<SquashDSLAssertion> expectedAssertions=normalizeAssertions(Arrays.asList(
                new SquashDSLAssertion("equals", "xml", "result.sql"),
                new SquashDSLAssertion("rocks", "O/S", null)
        ));
        
        Assert.assertEquals("Assertions not found", expectedAssertions,actualAssertions);
    }
    
    @Test
    public void findCommands(){
        List<SquashDSLCommand> actualCommands=normalizeCommands(testee.buildRegistry().getCommands());
        List<SquashDSLCommand> expectedCommands=normalizeCommands(Arrays.asList(
                new SquashDSLCommand("chocapic", "file", "void", "file"),
                new SquashDSLCommand("try.to.infuse", "ethics", "gafa.boss", "void.resource")
        ));
        
        Assert.assertEquals("Commands not found",expectedCommands,actualCommands);
    }
    
    @Test
    public void findConverters(){
        List<SquashDSLConverter> actualConverters=normalizeConverters(testee.buildRegistry().getConverters());
        List<SquashDSLConverter> expectedConverters=normalizeConverters(Arrays.asList(
                new SquashDSLConverter("windows", "computer", "useless.lump")
        ));
        
        Assert.assertEquals("Converter not found", expectedConverters,actualConverters);
    }

    private List<SquashDSLAssertion> normalizeAssertions(final List<SquashDSLAssertion> assertions) {
        List<SquashDSLAssertion> actualAssertions=new ArrayList<>(assertions);
        Collections.sort(actualAssertions, new AssertionComparator());
        return actualAssertions;
    }
    
    private List<SquashDSLCommand> normalizeCommands(final List<SquashDSLCommand> commands) {
        List<SquashDSLCommand> actualCommands=new ArrayList<>(commands);
        Collections.sort(actualCommands, new CommandComparator());
        return actualCommands;
    }
    
    private List<SquashDSLConverter> normalizeConverters(final List<SquashDSLConverter> converters) {
        List<SquashDSLConverter> actualCommands=new ArrayList<>(converters);
        Collections.sort(actualCommands, new ConverterComparator());
        return actualCommands;
    }

    private class AssertionComparator implements Comparator<SquashDSLAssertion> {

        public AssertionComparator() {
        }

        @Override
        public int compare(SquashDSLAssertion t, SquashDSLAssertion t1) {
            String tdescription=buildDescriptor(t);
            String t1descriptor=buildDescriptor(t1);
            return tdescription.compareTo(t1descriptor);
        }

        public String buildDescriptor(SquashDSLAssertion t) {
            return t.getActualResource()+t.getNature()+t.getExpectedResource();
        }
    }

    private class CommandComparator implements Comparator<SquashDSLCommand> {

        public CommandComparator() {
        }

        @Override
        public int compare(SquashDSLCommand t, SquashDSLCommand t1) {
            String descriptorT=descriptor(t);
            String descriptorT1=descriptor(t1);
            return descriptorT.compareTo(descriptorT1);
        }

        private String descriptor(SquashDSLCommand t) {
            String descriptorT=t.getInputResource()+t.getNature()+t.getTarget()+t.getOutputResource();
            return descriptorT;
        }
    }

    private class ConverterComparator implements Comparator<SquashDSLConverter> {

        public ConverterComparator() {
        }

        @Override
        public int compare(SquashDSLConverter t, SquashDSLConverter t1) {
            String decriptorT=descriptor(t);
            String descriptorT1=descriptor(t1);
            return decriptorT.compareTo(descriptorT1);
        }

        private String descriptor(SquashDSLConverter t) {
            String decriptorT=t.getInputResource()+t.getNature()+t.getOutputResource();
            return decriptorT;
        }
    }
}
