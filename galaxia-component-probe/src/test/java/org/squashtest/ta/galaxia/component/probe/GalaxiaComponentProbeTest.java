/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.component.probe;

import java.io.IOException;
import java.util.function.Consumer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.backbone.definition.BinaryAssertionDefinition;
import org.squashtest.ta.backbone.definition.CommandDefinition;
import org.squashtest.ta.backbone.definition.ConverterDefinition;
import org.squashtest.ta.backbone.definition.UnaryAssertionDefinition;
import org.squashtest.ta.backbone.init.EngineComponentDefinitionManager;
import org.squashtest.ta.backbone.init.EngineLoader;
import org.squashtest.ta.core.tools.io.TempFileUtils;
import org.squashtest.ta.framework.facade.Engine;
import org.squashtest.ta.framework.test.definition.TestSuite;
import org.squashtest.ta.galaxia.component.probe.testcomponents.ProbeTestWorkspaceBrowser;

/**
 * This is a component test of the engine probe.
 * @author edegenetais
 */
public class GalaxiaComponentProbeTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(GalaxiaComponentProbeTest.class);
    
    private static boolean illegalStateOK;
    
    @BeforeClass
    public static void testPutBeforeLoadingEngineByThatHackShouldYieldIllegalState(){
        /*
         * This is a bit uncanny : by putting this here, 
         * we make sure that it is done once per classloading context,
         * and above all before the tests that load the probe (since we wanna test pre-loading error handling ;) ).
         */
        try{
            EngineComponentDefinitionManager registry=new GalaxiaProbeRepositoryCreator().getComponentManager();
            illegalStateOK=false;
        }catch(IllegalStateException e){
            illegalStateOK=true;
            LOGGER.debug("OK : unloaded probe threw IllegalStateException.",e);
        }catch(Exception e){
            LOGGER.error("Improperly caught illegal state for the probe",e);
            illegalStateOK=false;
        }
    }
    
    @Test
    public void usingProbeBeforeEngineLoadShouldThrowIllegalState(){
        Assert.assertTrue("IllegalState status was not properly reported.", illegalStateOK);
    }
    
    @Before
    public void loadProbe() throws IOException{
        Engine engine=new EngineLoader().load();
        TempFileUtils.init(System.getProperty("java.io.tmpdir"), "true");
        final TestSuite testSuite = new TestSuite();
        testSuite.setName("neverland");
        engine.execute(testSuite,new ProbeTestWorkspaceBrowser());
    }

    @Test
    public void loadedProbeHasRegistry(){
        EngineComponentDefinitionManager registry=new GalaxiaProbeRepositoryCreator().getComponentManager();
        Assert.assertNotNull("When the probe is loaded into the engine, the registry should exist.",registry);
    }
            
    @Test
    public void findsCommand(){
        
        EngineComponentDefinitionManager registry=new GalaxiaProbeRepositoryCreator().getComponentManager();
        
        Assert.assertEquals(1,registry.getCommandDefinitions().size()); 
        
        registry.getCommandDefinitions().forEach(new CommandChecker());
        
    }
    
    @Test
    public void findsBinaryAssertion(){
        EngineComponentDefinitionManager registry=new GalaxiaProbeRepositoryCreator().getComponentManager();
        
        Assert.assertEquals(1,registry.getBinaryAssertionDefinitions().size()); 
        
        registry.getBinaryAssertionDefinitions().forEach(new BinaryAssertionChecker());
    }
    
    @Test
    public void findsConverter(){
    
        EngineComponentDefinitionManager registry=new GalaxiaProbeRepositoryCreator().getComponentManager();
        
        Assert.assertEquals(1,registry.getConverterDefinitions().size()); 
                
        registry.getConverterDefinitions().forEach(new ConverterPrinter());
    }
    
    @Test
    public void findsUnaryAssertions(){
    
        EngineComponentDefinitionManager registry=new GalaxiaProbeRepositoryCreator().getComponentManager();
        
        Assert.assertEquals(1,registry.getUnaryAssertionDefinitions().size()); 
    
        registry.getUnaryAssertionDefinitions().forEach(new UnaryAssertionPrinter());
        
    }
    
    private static class CommandChecker implements Consumer<CommandDefinition> {
        @Override
        public void accept(CommandDefinition t) {
            LOGGER.info("Command : EXECUTE {} WITH {} ON {} AS {}",
                    t.getCategory().getName(),      // self explanatory, innit ?
                    t.getFirstNature().getName(),   // command first nature ~ INPUT resource type
                    t.getSecondNature().getName(),  // command second nature ~ target type
                    t.getResultNature().getName()   // self explanatory, innit ?
            );
            Assert.assertEquals("Bad category", "testcommand", t.getCategory().getName());
            Assert.assertEquals("Bad INPUT type", "file", t.getFirstNature().getName());
            Assert.assertEquals("Bad TARGET type","void.target",t.getSecondNature().getName());
            Assert.assertEquals("Bad OUTPUT type", "void.resource", t.getResultNature().getName());
        }
    }

    private static class BinaryAssertionChecker implements Consumer<BinaryAssertionDefinition> {

        @Override
        public void accept(BinaryAssertionDefinition t) {
            
            LOGGER.info("BinaryAssertion : ASSERT THAT {} {} {}",
                    t.getFirstNature().getName(),
                    t.getCategory().getName(),
                    t.getSecondNature().getName()
            );
            Assert.assertEquals("Bad category", "grOOves", t.getCategory().getName());
            Assert.assertEquals("Bad INPUT type", "file", t.getFirstNature().getName());
            Assert.assertEquals("Bad OUTPUT type", "file", t.getSecondNature().getName());
        }
    }

    private static class UnaryAssertionPrinter implements Consumer<UnaryAssertionDefinition> {

        @Override
        public void accept(UnaryAssertionDefinition t) {
            
            LOGGER.info("UnaryAssertion : ASSERT THAT {} {}",
                    t.getFirstNature().getName(),
                    t.getCategory().getName()
            );
            Assert.assertEquals("Bad category", "rocks", t.getCategory().getName());
            Assert.assertEquals("Bad INPUT type", "O/S", t.getFirstNature().getName());
        }
    }

    private static class ConverterPrinter implements Consumer<ConverterDefinition> {

        @Override
        public void accept(ConverterDefinition t) {
            
            LOGGER.info("Converter {} changes {} into {}",
                    t.getCategory().getName(),
                    t.getFirstNature().getName(),
                    t.getSecondNature().getName()
            );
            Assert.assertEquals("Bad category", "windows", t.getCategory().getName());
            Assert.assertEquals("Bad INPUT type", "computer", t.getFirstNature().getName());
            Assert.assertEquals("Bad OUTPUT type", "useless.lump", t.getSecondNature().getName());
        }
    }

}
