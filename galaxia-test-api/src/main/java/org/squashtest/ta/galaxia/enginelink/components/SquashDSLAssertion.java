/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.enginelink.components;

import java.util.Objects;

public class SquashDSLAssertion {
    private String nature;
    private String actualResource;
    private String expectedResource;

    public SquashDSLAssertion(String nature, String actualResource, String expectedResource) {
        this.nature = nature;
        this.actualResource = actualResource;
        this.expectedResource = expectedResource;
    }

    public SquashDSLAssertion() {
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getActualResource() {
        return actualResource;
    }

    public void setActualResource(String actualResource) {
        this.actualResource = actualResource;
    }

    public String getExpectedResource() {
        return expectedResource;
    }

    public void setExpectedResource(String expectedResource) {
        this.expectedResource = expectedResource;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.nature);
        hash = 89 * hash + Objects.hashCode(this.actualResource);
        hash = 89 * hash + Objects.hashCode(this.expectedResource);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SquashDSLAssertion other = (SquashDSLAssertion) obj;
        if (!Objects.equals(this.nature, other.nature)) {
            return false;
        }
        if (!Objects.equals(this.actualResource, other.actualResource)) {
            return false;
        }
        if (!Objects.equals(this.expectedResource, other.expectedResource)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SquashDSLAssertion{" +
                "nature='" + nature + '\'' +
                ", actualResource='" + actualResource + '\'' +
                ", expectedResource='" + expectedResource + '\'' +
                '}';
    }
}
