/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.enginelink.components;

import java.util.Objects;

public class SquashDSLMacroParamDefinition {
    private String name;
    private SquashDSLMacroParamType type;

    /**
     * Convenience constructor for the legacy 'raw type'.
     * @param name 
     */
    public SquashDSLMacroParamDefinition(String name){
        this(name, SquashDSLMacroParamType.RAW_TYPE);
    }
    
    public SquashDSLMacroParamDefinition(String name, SquashDSLMacroParamType type) {
        this.name = name;
        this.type = type;
    }

    public SquashDSLMacroParamDefinition() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SquashDSLMacroParamType getType() {
        return type==null?SquashDSLMacroParamType.RAW_TYPE:type;
    }

    public void setType(SquashDSLMacroParamType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SquashDSLMacroParamDefinition other = (SquashDSLMacroParamDefinition) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }
    
    
}
