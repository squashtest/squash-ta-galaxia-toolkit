/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.enginelink.components;

import java.util.Objects;

public class SquashDSLCommand {
    private String nature;
    private String inputResource;
    private String target;
    private String outputResource;

    public SquashDSLCommand(String nature, String inputResource, String target, String outputResource) {
        this.nature = nature;
        this.inputResource = inputResource;
        this.target = target;
        this.outputResource = outputResource;
    }

    public SquashDSLCommand() {
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getInputResource() {
        return inputResource;
    }

    public void setInputResource(String inputResource) {
        this.inputResource = inputResource;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getOutputResource() {
        return outputResource;
    }

    public void setOutputResource(String outputResource) {
        this.outputResource = outputResource;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.nature);
        hash = 59 * hash + Objects.hashCode(this.inputResource);
        hash = 59 * hash + Objects.hashCode(this.target);
        hash = 59 * hash + Objects.hashCode(this.outputResource);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SquashDSLCommand other = (SquashDSLCommand) obj;
        if (!Objects.equals(this.nature, other.nature)) {
            return false;
        }
        if (!Objects.equals(this.inputResource, other.inputResource)) {
            return false;
        }
        if (!Objects.equals(this.target, other.target)) {
            return false;
        }
        if (!Objects.equals(this.outputResource, other.outputResource)) {
            return false;
        }
        return true;
    }

    
    
    @Override
    public String toString() {
        return "SquashDSLCommand{" +
                "nature='" + nature + '\'' +
                ", inputResource='" + inputResource + '\'' +
                ", target='" + target + '\'' +
                ", outputResource='" + outputResource + '\'' +
                '}';
    }
}
