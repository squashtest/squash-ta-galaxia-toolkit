/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.enginelink.components;

import java.util.Objects;

public class SquashDSLMacroParam extends SquashDSLMacroSignature {
    public static final String TYPE = "param";
    
    private SquashDSLMacroParamDefinition definition;
    
    public SquashDSLMacroParam(SquashDSLMacroParamDefinition definition) {
        super(TYPE);
        this.definition = definition;
    }
    
    //TODO : patch the json codec to be able to remove this, because the ability to create and use invlid object is SHIT.
    public SquashDSLMacroParam() {
        super(TYPE);
    }

    public SquashDSLMacroParamDefinition getDefinition() {
        return definition;
    }

    public void setDefinition(SquashDSLMacroParamDefinition definition) {
        this.definition = definition;
    }
    
    @Override
    public String getType(){
        return TYPE;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.definition);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SquashDSLMacroParam other = (SquashDSLMacroParam) obj;
        if (!Objects.equals(this.definition, other.definition)) {
            return false;
        }
        return true;
    }

    @Override
    public String definitionPart() {//TODO : repackage this ! It doesn't belong in this model because it ties keyword definition to DSL code syntax...
        if(definition==null){
            throw new IllegalStateException("You've just tried to use a partially created object. You should forget the default constructor (to be removed later on anyway) and use one of the other full initialization constructors.");
        }
        if("Raw".equals(definition.getType().getCategory())){
            return "{"+definition.getName()+"}";
        }else{
            throw new UnsupportedOperationException("Typed Macro parameters are not yet supported by the galaxia connector.");
        }
    }

    @Override
    public <RESULT> RESULT visit(SquashDSLMacroSignatureVisitor<RESULT> visitor) {
        return visitor.visit(this);
    }
    
    
}
