/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.enginelink.components;

import java.util.List;
import java.util.Objects;

public class SquashDSLMacro {
    private String name;
    private List<SquashDSLMacroSignature> signatures;
    private boolean customMacro;

    public SquashDSLMacro() {
    }

    public SquashDSLMacro(String name, List<SquashDSLMacroSignature> signatures) {
        this.name = name;
        this.signatures = signatures;
    }

    public SquashDSLMacro(String name, List<SquashDSLMacroSignature> signatures, boolean customMacro) {
        this.name = name;
        this.signatures = signatures;
        this.customMacro = customMacro;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SquashDSLMacroSignature> getSignatures() {
        return signatures;
    }

    public void setSignatures(List<SquashDSLMacroSignature> signatures) {
        this.signatures = signatures;
    }

    public boolean isCustomMacro() {
        return customMacro;
    }

    public void setCustomMacro(boolean customMacro) {
        this.customMacro = customMacro;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + Objects.hashCode(this.signatures);
        hash = 37 * hash + (this.customMacro ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SquashDSLMacro other = (SquashDSLMacro) obj;
        if (this.customMacro != other.customMacro) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return Objects.equals(this.signatures, other.signatures);
    }

    @Override
    public String toString() {
        return "SquashDSLMacro{" +
                "name=" + name +
                ", signatures=" + signatures +
                ", customMacro=" + customMacro +
                '}';
    }

}
