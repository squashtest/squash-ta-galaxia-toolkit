/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.enginelink.components;

import java.util.Objects;

public class SquashDSLMacroFixedPart extends SquashDSLMacroSignature {
    public static final String TYPE = "fixedPart";
    
    private String content;

    public SquashDSLMacroFixedPart(String content) {
        super(TYPE);
        this.content = content;
    }

    public SquashDSLMacroFixedPart() {
        super(TYPE);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.content);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SquashDSLMacroFixedPart other = (SquashDSLMacroFixedPart) obj;
        if (!Objects.equals(this.content, other.content)) {
            return false;
        }
        return true;
    }

    
    
    @Override
    public String definitionPart() {
        return content;
    }

    @Override
    public <RESULT> RESULT visit(SquashDSLMacroSignatureVisitor<RESULT> visitor) {
        return visitor.visit(this);
    }
}
