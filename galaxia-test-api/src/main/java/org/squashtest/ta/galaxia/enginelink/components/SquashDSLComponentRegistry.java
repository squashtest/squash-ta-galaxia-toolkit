/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.enginelink.components;

import java.util.List;
import java.util.Set;

public class SquashDSLComponentRegistry {
    private List<SquashDSLAssertion>assertions;
    private List<SquashDSLCommand>commands;
    private List<SquashDSLConverter>converters;
    private List<SquashDSLMacro>macros;
    private Set<String> targetTypes;
    private Set<String>resourceTypes;
    
    public SquashDSLComponentRegistry(List<SquashDSLAssertion> assertions, List<SquashDSLCommand> commands, List<SquashDSLConverter> converters, List<SquashDSLMacro> macros, Set<String> resourceTypes, Set<String> targetTypes) {
        this.assertions = assertions;
        this.commands = commands;
        this.converters = converters;
        this.macros = macros;
        this.resourceTypes = resourceTypes;
        this.targetTypes = targetTypes;
    }

    public SquashDSLComponentRegistry() {
    }

    public List<SquashDSLAssertion> getAssertions() {
        return assertions;
    }

    public void setAssertions(List<SquashDSLAssertion> assertions) {
        this.assertions = assertions;
    }

    public List<SquashDSLCommand> getCommands() {
        return commands;
    }

    public void setCommands(List<SquashDSLCommand> commands) {
        this.commands = commands;
    }

    public List<SquashDSLConverter> getConverters() {
        return converters;
    }

    public void setConverters(List<SquashDSLConverter> converters) {
        this.converters = converters;
    }

    public List<SquashDSLMacro> getMacros() {
        return macros;
    }

    public void setMacros(List<SquashDSLMacro> macros) {
        this.macros = macros;
    }

    public Set<String> getResourceTypes() {
        return resourceTypes;
    }

    public void setResourceTypes(Set<String> resources) {
        this.resourceTypes = resources;
    }

    public Set<String> getTargetTypes() {
        return targetTypes;
    }

    public void setTargetTypes(Set<String> targetTypes) {
        this.targetTypes = targetTypes;
    }
    
    @Override
    public String toString() {
        return "SquashDSLComponentRegistry{" +
                "assertions=" + assertions +
                ", commands=" + commands +
                ", converters=" + converters +
                ", macros=" + macros +
                ", resourceTypes=" + resourceTypes +
                ", targetTypes=" + targetTypes +
                '}';
    }
}
