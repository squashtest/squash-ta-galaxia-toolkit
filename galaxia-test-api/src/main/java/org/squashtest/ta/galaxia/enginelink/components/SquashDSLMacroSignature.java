/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2016 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.galaxia.enginelink.components;

import java.util.Objects;

public abstract class SquashDSLMacroSignature {
    private String type;

    public SquashDSLMacroSignature(String type) {
        this.type = type;
    }

    public SquashDSLMacroSignature() {
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SquashDSLMacroSignature other = (SquashDSLMacroSignature) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }

    public abstract String getType();
    
    public abstract <RESULT> RESULT visit(SquashDSLMacroSignatureVisitor<RESULT> visitor);
    
    /** This gives tools access to the code defining that signature element. */
    public abstract String definitionPart(); 
}
