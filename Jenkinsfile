// call the library : https://bitbucket.org/nx/std-builds-library

@Library('std-builds-library') _


pipeline {
    agent {
        kubernetes {
            inheritFrom 'maven-builder-jdk8'
            defaultContainer 'maven'
        }
    }

    environment {
            JAVA_TOOL_OPTIONS = "-Duser.timezone=Europe/Paris -Duser.language=FR -Duser.country=FR -Duser.variant=FR"
            HOME = "${WORKSPACE}"         
    }
    parameters{
        choice(
            name : 'build_type',
            description : 'Choose between regular or release build',
            choices: ["regular", "release"]
        )

        string(
            name: "release_version",
            description: "the release version (if release)",
            defaultValue : ""
        )

        string(
            name: "next_version",
            description: "the next development version (if release)",
            defaultValue : ""
        )
    }    

    stages {
           
        stage('Build') {
            when  {
                expression { params.build_type == 'regular' }
                  }         
            
            steps {           
                    
                sh 'mvn -B -ntp org.jacoco:jacoco-maven-plugin:prepare-agent deploy -Pci,public'

            }
        }
        stage('QA'){
            when  {
                expression { params.build_type == 'regular' }
                  }  
            steps {
                withSonarQubeEnv('SonarQube'){
                    sh 'mvn -Dsonar.scm.disabled=true sonar:sonar'
                }
                timeout(time: 10, unit: 'MINUTES'){
                    waitForQualityGate abortPipeline: true
                }
            }
            post {
                always{
                    script {
                        notifications.pushToGChat(['gchat_credentials' : 'tf-legacy-build-status'])
                    }
                }
            } 
        }
        // release : when the build is a release (serious game) build
        stage('release'){
            when{
                expression { params.build_type == 'release' }
            }
            steps {
                container('maven') {
                    script {

                        def valid = release.isConfValid()
                        if (!valid) {
                            error('Cannot release : either the release version or next dev version is wrong')
                        }

                        echo "releasing : "
                        sh """
                            mvn -B -ntp org.codehaus.gmaven:gmaven-plugin:execute@release-logic -Pci \
                                -DreleaseVersion=${params.release_version} \
                                -DdevelopmentVersion=${params.next_version}"""   
                        
                        sh """
                            mvn -B -ntp release:prepare -Dpush=false release:perform \
                            -DuseReleaseProfile=false \
                            -Dmaven.source.skip \
                            -Pci,public"""                   
                        echo "release complete !"

                    }
                }

            }
            post{
                always{
                    script {
                        notifications.pushToGChat(['gchat_credentials' : 'tf-legacy-build-status'])
                    }
                }
                success {
                    script {
                        release.addReleaseInfo(this)
                        release.push()
                    }
                }
            }
        }        
    }
}
